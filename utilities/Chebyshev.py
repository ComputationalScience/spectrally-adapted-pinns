# -*- coding: utf-8 -*-
"""
Created on Mon Jan 17 03:07:57 2022

@author: 15000
"""
from math import sin, sqrt, acos, cos, pi

# the definition of Chebyshev polynomials of order n
def Chebyshev(x, n, beta):
    if n == 0:
        y = 1
    elif n == 1:
        y = x
    else:
        y1 = 1
        y2 = x
        for i in range(n-1):
            y3 = 2 * x * y2 - y1
            y1 = y2
            y2 = y3
    
        y = y2
        
    return y

# the collocation points, weights, and norms for the Chebyshev polynomials
def Init(n, beta):
    scale = 1
    X = [0 for i in range(n)]
    Wx = [0 for i in range(n)]
    for i in range(n):
        X[i] = scale * -cos(pi*i / (n-1)) 
        if i == 0 or i == n-1:
            Wx[i] = pi / (n-1) / 2 * scale
        else:
            Wx[i] = pi / (n-1) * scale
    
    Gamma_x = [pi/2 for i in range(n)]
    Gamma_x[0] = Gamma_x[0] * 2
    return X, Wx, Gamma_x
    
# calculate the coefficients u_i in the spectral expansion given the original function u
def discrete_beta(u, X, Wx, n, Gamma_x, beta):
    U = [0 for i in range(n)]
    X_val = [[0 for i in range(n)] for i in range(n)]
    for i in range(n):
        for j in range(n):
            X_val[i][j] = Chebyshev(X[i], j, beta)
    
    for i in range(n):
        for j in range(n):
            U[i] += u[j] * Wx[j] * X_val[j][i] / Gamma_x[i]
    
    return U

# given the coefficients in the spectral expansion U_{N-1} = \sum u_i T_i, calculate the 
# function U_{N-1} at x
def inverse(x, y, beta):
    n = len(y)
    result = 0
    for i in range(n):
        result += Chebyshev(x, i, beta) * y[i]
    
    return result

# the definition of the frequency indicator, which measures the ratio of high frequency
# components in a spectral expansion
def frequency_indicator(u_inv, n, Gamma_x):
    total = 0
    residual = 0
    for i in range(n):
        total += u_inv[i]**2 * Gamma_x[i]
        if i > (2*n+2) // 3:
            residual += u_inv[i]**2 * Gamma_x[i]
    
    return sqrt(residual / total)