# -*- coding: utf-8 -*-
"""
Created on Mon Jan 17 03:26:28 2022

@author: 15000
"""
from scipy import exp, sin, sqrt, cos, pi
import copy
import torch

# the collocation points, weights, and norms for the generalized Hermite polynomials
def Init(n, beta):
    A = [[0 for i in range(n)] for j in range(n)]
    Wx = [0 for i in range(n)]
    for i in range(n):
        if i == 0:
            A[i][i] = 0
            A[i][i+1] = sqrt((1+i)/2)
        elif i == n-1:
            A[i][i] = 0
            A[i][i-1] = sqrt(i/2)
        else:
            A[i][i] = 0
            A[i][i-1] = sqrt(i/2)
            A[i][i+1] = sqrt((i+1) / 2)
    
    A = torch.tensor(A, dtype=torch.float64)
    (evals, evecs) = torch.eig(A, eigenvectors=True)
    X = [0 for i in range(n)]
    for i in range(n):
        X[i] = float(evals[i][0])
    
    X.sort()
    for i in range(n):
        Wx[i] = 1 / n / Hermite(X[i], n-1, 1)**2
    
    Gamma_x = [1 for i in range(n)]
    Wx = [Wx[i] / beta for i in range(n)]
    X = [X[i] / beta for i in range(n)]
    
    return X, Wx, Gamma_x

# calculate the coefficients u_i in the spectral expansion given the original function u
def discrete_beta(u, X, Wx, n, Gamma_x, beta):
    U = [0 for i in range(n)]
    X_val = [[0 for i in range(n)] for i in range(n)]
    for i in range(n):
        for j in range(n):
            X_val[i][j] = Hermite(X[i], j, beta)
    
    for i in range(n):
        for j in range(n):
            U[i] += u[j] * Wx[j] * X_val[j][i] / Gamma_x[i]
    
    U2 = [0 for i in range(n)]
    X_val = [[0 for i in range(n)] for i in range(n)]
    for i in range(n):
        for j in range(n):
            X_val[i][j] = Hermite(X[i], j, beta)
    
    for i in range(n):
        for j in range(n):
            U2[i] += u[j+n] * Wx[j] * X_val[j][i] / Gamma_x[i]
    
    return copy.deepcopy(U) + copy.deepcopy(U2)

# the definition of the generalized Hermite functions of order n with a scaling factor \beta
def Hermite(x, n, beta):
    x = x * beta
    if n == 0:
        y = pi**(-1/4) * exp(-x**2/2)
    elif n == 1:
        y = sqrt(2) * pi**(-1/4) * x * exp(-x**2/2)
    else:
        y0 = pi**(-1/4) * exp(-x**2/2)
        y1 = sqrt(2) * pi**(-1/4) * x * exp(-x**2/2)
        for i in range(n-1):
            y2 = x * sqrt(2/(i+2)) * y1 - sqrt((i+1)/(i+2))*y0
            y0 = y1
            y1 = y2
        y = y2
    
    y = y * sqrt(beta)
    return y

# given the coefficients in the spectral expansion U_{N-1} = \sum u_i T_i, calculate the 
# function U_{N-1} at x
def inverse(x, y, beta):
    n = len(y)
    result = 0
    for i in range(n):
        result += Hermite(x, i, beta) * y[i]
    
    return result

# the definition of the frequency indicator in both dimensions, which measures the ratio of high frequency
# components in a spectral expansion
def frequency_indicator(u_inv, n, Gamma_x):
    total = 0
    residual = 0
    for i in range(n):
        total += u_inv[i]**2 * Gamma_x[i]
        if i > (2*n+2) // 3:
            residual += u_inv[i]**2 * Gamma_x[i]
    
    for i in range(n):
        total += u_inv[i+n]**2 * Gamma_x[i]
        if i > (2*n+2) // 3:
            residual += u_inv[i+n]**2 * Gamma_x[i]
    
    return sqrt(residual / total)