# -*- coding: utf-8 -*-
"""
Created on Mon Jan 17 03:19:12 2022

@author: 15000
"""
from math import sin, sqrt, acos, cos, pi
from scipy import exp
import torch

# the collocation points, weights, and norms for the generalized Hermite polynomials
def Init(n, beta):
    A = [[0 for i in range(n)] for j in range(n)]
    Wx = [0 for i in range(n)]
    for i in range(n):
        if i == 0:
            A[i][i] = 0
            A[i][i+1] = sqrt((1+i)/2)
        elif i == n-1:
            A[i][i] = 0
            A[i][i-1] = sqrt(i/2)
        else:
            A[i][i] = 0
            A[i][i-1] = sqrt(i/2)
            A[i][i+1] = sqrt((i+1) / 2)
    
    A = torch.tensor(A, dtype=torch.float64)
    (evals, evecs) = torch.eig(A, eigenvectors=True)
    X = [0 for i in range(n)]
    for i in range(n):
        X[i] = float(evals[i][0])
    
    X.sort()
    for i in range(n):
        Wx[i] = 1 / n / Hermite(X[i], n-1, 1)**2
    
    Gamma_x = [1 for i in range(n)]
    Wx = [Wx[i] / beta for i in range(n)]
    X = [X[i] / beta for i in range(n)]
    
    return X, Wx, Gamma_x

# calculate the coefficients u_i in the spectral expansion given the original function u
def discrete_beta(u, X, Wx, n, Gamma_x, Y, Wy, ny, Gamma_y, beta, betay):
    U = [0 for i in range(n*ny)]
    X_val = [[0 for i in range(n)] for j in range(n)]
    Y_val = [[0 for i in range(ny)] for j in range(ny)]
    for i in range(n):
        for j in range(n):
            X_val[i][j] = Hermite(X[i], j, beta)
    
    for i in range(ny):
        for j in range(ny):
            Y_val[i][j] = Hermite(Y[i], j, betay)
    
    for i in range(n):
        for j in range(n):
            for i2 in range(ny):
                for j2 in range(ny):        
                    U[i2*n + i] += u[j2*n+j] * Wx[j] * X_val[j][i] / Gamma_x[i] * Wy[j2] * Y_val[j2][i2] / Gamma_y[i2]
    
    return U

# the definition of the generalized Hermite functions of order n with a scaling factor \beta
def Hermite(x, n, beta):
    x = x * beta
    if n == 0:
        y = pi**(-1/4) * exp(-x**2/2)
    elif n == 1:
        y = sqrt(2) * pi**(-1/4) * x * exp(-x**2/2)
    else:
        y0 = pi**(-1/4) * exp(-x**2/2)
        y1 = sqrt(2) * pi**(-1/4) * x * exp(-x**2/2)
        for i in range(n-1):
            y2 = x * sqrt(2/(i+2)) * y1 - sqrt((i+1)/(i+2))*y0
            y0 = y1
            y1 = y2
        y = y2
    
    y = y * sqrt(beta)
    return y

# given the coefficients in the spectral expansion U_{N-1} = \sum u_i T_i, calculate the 
# function U_{N-1} at x
def inverse(x, y, n, ny, beta, beta_y, u_inv):
    result = 0
    for i in range(n):
        for j in range(ny):
            result += Hermite(x, i, beta) * Hermite(y, j, beta_y) * u_inv[i+j*n]
    
    return result

# the definition of the frequency indicator in both dimensions, which measures the ratio of high frequency
# components in a spectral expansion
def frequency_indicator_x(u_inv, n, Gamma_x, ny, Gamma_y):
    total = 0
    residual = 0
    for i in range(n):
        for j in range(ny):
            total += u_inv[i+j*n]**2 * Gamma_x[i]
            if i > (2*n+2) // 3:
                residual += u_inv[i+j*n]**2 * Gamma_x[i]
    
    return sqrt(residual / total)

def frequency_indicator_y(u_inv, n, Gamma_x, ny, Gamma_y):
    total = 0
    residual = 0
    for i in range(n):
        for j in range(ny):
            total += u_inv[i+j*n]**2 * Gamma_x[i]
            if j > (2*ny+2) // 3:
                residual += u_inv[i+j*n]**2 * Gamma_x[i]
    
    return sqrt(residual / total)