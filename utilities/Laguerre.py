# -*- coding: utf-8 -*-
"""
Created on Mon Jan 17 03:13:42 2022

@author: 15000
"""
from math import sin, sqrt, acos, cos, pi, exp
import torch

# the collocation points, weights, and norms for the Laguerre polynomials
def Init(n, beta):
    A = [[0 for i in range(n)] for j in range(n)]
    Wx = [0 for i in range(n)]
    for i in range(n):
        if i == 0:
            A[i][i] = 2*i + 1 
            A[i][i+1] = -sqrt((i+1)*(i+1))
        elif i == n-1:
            A[i][i] = 2*(i) + 1
            A[i][i-1] = -sqrt((i)*((i)))
        else:
            A[i][i] = 2*(i) + 1
            A[i][i-1] = -sqrt((i)*((i)))
            A[i][i+1] = -sqrt((i+1)*(i+1))
    
    A = torch.tensor(A, dtype=torch.float64)
    (evals, evecs) = torch.eig(A, eigenvectors=True)
    X = [0 for i in range(n)]
    for i in range(n):
        X[i] = float(evals[i][0])
    
    X.sort()
    for i in range(n):
        Wx[i] = X[i] / n**2 / (Laguerre(X[i], n-1, 1))**2
    
    Gamma_x = [1 / beta for i in range(n)]
    Wx = [Wx[i] / beta for i in range(n)]
    X = [X[i] / beta for i in range(n)]
    
    return X, Wx, Gamma_x
    
# calculate the coefficients u_i in the spectral expansion given the original function u
def discrete_beta(u, X, Wx, n, Gamma_x, beta):
    U = [0 for i in range(n)]
    X_val = [[0 for i in range(n)] for i in range(n)]
    for i in range(n):
        for j in range(n):
            X_val[i][j] = Laguerre(X[i], j, beta)
    
    for i in range(n):
        for j in range(n):
            U[i] += u[j] * Wx[j] * X_val[j][i] / Gamma_x[i]
    
    return U

# the definition of Laguerre polynomials of order n
def Laguerre(x, n, beta):
    x = x * beta
    if n == 0:
        y = exp(-x/2)
    elif n == 1:
        y = -x+1
        y = y * exp(-x/2)
    else:
        y1 = 1
        y2 = -x + 1
        for i in range(1, n):
            y3 = ((2*i + 1 - x) * y2 - (i) * y1) / (i+1)
            y1 = y2
            y2 = y3
    
        y = y2 *exp(-x/2)
        
    return y

# given the coefficients in the spectral expansion U_{N-1} = \sum u_i T_i, calculate the 
# function U_{N-1} at x
def inverse(x, y, beta):
    n = len(y)
    result = 0
    for i in range(n):
        result += Laguerre(x, i, beta) * y[i]
    
    return result

# the definition of the frequency indicator, which measures the ratio of high frequency
# components in a spectral expansion
def frequency_indicator(u_inv, n, Gamma_x):
    total = 0
    residual = 0
    for i in range(n):
        total += u_inv[i]**2 * Gamma_x[i]
        if i > (2*n+2) // 3:
            residual += u_inv[i]**2 * Gamma_x[i]
    
    return sqrt(residual / total)