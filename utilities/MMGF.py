# -*- coding: utf-8 -*-
"""
Created on Mon Jan 17 03:02:59 2022

@author: 15000
"""
from math import sqrt, acos
from scipy import exp

# definition of scaled MMGF of order n
def MMGF(x, n, beta):
    x = x * beta
    x = x / sqrt(x**2 + 1)
    if n == 0:
        y = 1 / sqrt(2)
    else:
        y = (exp(1j*n * acos(x)) + exp(-1j*n*acos(x)))/2
        y = y * sqrt(1 - x**2)
    
    return y.real