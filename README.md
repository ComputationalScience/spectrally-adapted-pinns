# Spectrally adapted PINNs

This project provides several example implementations of spectrally adapted physics-informed neural networks (PINNs). We show that spectrally adaptbed PINNs are useful to solve analytically intractable partial differential equations (PDEs) that involve at least one variable that is defined in an unbounded domain. Numerically solving unbounded domain problems requires efficient numerical methods that accurately resolve the dependence of the PDE on that variable over several orders of
magnitude. Unbounded domain problems arise in various application areas and solving such problems is thus important for understanding multi-scale biological dynamics, resolving physical processes at long time scales and distances, and performing parameter inference in engineering problems.

The directory `utilities/` contains implementations of several basis functions (e.g., Chebyshev, Hermite, Laguerre, and Gegenbauer functions). Implementations of all examples that we discuss in our paper are provided in the directory `examples/`.

## Method overview

<div align="center">
<img width="600" src="spectral_pinn.svg" alt="spectrally adapted PINNs">
</div>

Panel (a) in the above figure shows an example of a function u(x, t) for three different time points. Panel (b) shows the decaying behavior of a corresponding basis function element. Panel (c) shows that PDEs in unbounded domains can be solved by combining spectral decomposition and physics-informed neural networks. The spatial derivatives of the basis functions can be easily and explicitly obtained with high accuracy.

## Reference
* M. Xia, L. Böttcher, T. Chou, [Spectrally adapted physics-informed neural networks for solving unbounded domain problems](https://iopscience.iop.org/article/10.1088/2632-2153/acd0a1), Machine Learning: Science and Technology 4, 025024 (2023)

Please cite our work if you find the above implementations helpful for your own research projects.

```
@article{xiaspectral2023,
  title={Spectrally adapted physics-informed neural networks for solving unbounded domain problems},
  author={Xia, Mingtao and B{\"o}ttcher, Lucas and Chou, Tom},
  journal={Machine Learning: Science and Technology},
  volume={4},
  pages={025024},
  year={2023}
}
```
