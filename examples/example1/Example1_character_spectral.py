# -*- coding: utf-8 -*-
"""
Created on Fri Sep  3 01:31:55 2021

@author: 15000
"""

# -*- coding: utf-8 -*-
"""
Created on Tue Aug 31 05:04:52 2021

@author: 15000
"""

# -*- coding: utf-8 -*-
"""
Created on Tue Aug 31 01:27:15 2021

@author: 15000
"""

# this is the spectral-PINN model for approximation a function u = u(x, c)
import sys
sys.path.insert(0, '../../utilities/')

from Network_Model import TwoLayerNet3
from math import sin, sqrt, acos, cos, pi
from scipy import exp
import random
import numpy as np
import torch
from torch.autograd import Variable
import pdb
import csv
import pandas as pd
import copy
import random
from MMGF import MMGF
torch.set_default_tensor_type(torch.DoubleTensor)

# scaling factor
beta = 1/12 #1/2
n = 3
k = 1
        

def target_fun_new(lst):
    global beta
    x = lst[-1]
    y = 0
    y += lst[0] * sin((3*(x))) * x/2 / (1+(x/2)**2)**2
        
    return y

# import the training data
X_record = csv.reader(open('X0.csv'))
X = []
for i in X_record:
    X.append(float(i[0])*12)
    
X_ori = X
Labels = [[random.random() for j in range(1)] for i in range(200)]
labels_reader = csv.reader(open('Labels.csv'))
start = 0
for i in labels_reader:
    for j in range(1):
        Labels[start][j] = float(i[j])
        
    start += 1
    
# import the testing data
X_record2 = csv.reader(open('X20.csv'))
X2 = []
for i in X_record2:
    X2.append(float(i[0])*12)
    
X_ori2 = X2
Labels2 = [[random.random() for j in range(1)] for i in range(200)]

labels_reader2 = csv.reader(open('Labels2.csv'))
start = 0
for i in labels_reader2:
    for j in range(1):
        Labels2[start][j] = float(i[j])
        
    start += 1



Labels_x = copy.deepcopy(Labels)
for i in range(200):
    Labels_x[i].append(X_ori[i])
    
Labels_x2 = copy.deepcopy(Labels2)
for i in range(200):
    Labels_x2[i].append(X_ori2[i])

# calculate the target function 
X_ref = torch.tensor(Labels_x)
Y_ref = [[target_fun_new(Labels_x[i])] for i in range(200)]
Labels = torch.tensor(Labels)
Y = torch.tensor(Y_ref)

Y_ref2 = [[target_fun_new(Labels_x2[i])] for i in range(200)]
Labels2 = torch.tensor(Labels2)
Y2 = torch.tensor(Y_ref2)

# construct the spectral-PINN model, and set the optimizer
model = TwoLayerNet3(1, 10, 10)
criterion = torch.nn.MSELoss(reduction='sum')
optimizer = torch.optim.SGD(model.parameters(), lr=0.0005)
Y_pred = model(Labels)
optimizer.zero_grad()
Matrix = [[MMGF(X_ori[j], i, beta) for j in range(200)] for i in range(10)]
Matrix = torch.tensor(Matrix).to(torch.float64)
Matrix2 = [[MMGF(X_ori2[j], i, beta) for j in range(200)] for i in range(10)]
Matrix2 = torch.tensor(Matrix2).to(torch.float64)
loss = criterion(Y.transpose(0, 1)[0], torch.diag(torch.matmul(Y_pred, Matrix)))
loss.backward()
optimizer.step()
optimizer.zero_grad()
error = []
error2 = []

# perform the training
while loss.item() > 1e-10 and k < 50000:
    k += 1
    Y_pred = model(Labels)
    loss = criterion(Y.transpose(0, 1)[0], torch.diag(torch.matmul(Y_pred, Matrix)))
    optimizer.zero_grad()
    loss.backward()
    optimizer.step()
    if k % 10 == 0:
        Y_pred2 = model(Labels)
        loss2 = criterion(Y.transpose(0, 1)[0], torch.diag(torch.matmul(Y_pred2, Matrix)))
        result = loss2.item()
        Y_pred3 = model(Labels2)
        loss3 = criterion(Y2.transpose(0, 1)[0], torch.diag(torch.matmul(Y_pred3, Matrix2)))
        result3 = loss3.item()
        print(result, result3, k)
        error.append(result)
        error2.append(result3)
    
testing = pd.DataFrame(data = error)
testing.to_csv('error_spectral_approximate_train2.csv', header=False, index = False)
testing = pd.DataFrame(data = error2)
testing.to_csv('error_spectral_approximate_test2.csv', header=False, index = False)
print(k)
