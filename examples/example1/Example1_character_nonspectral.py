# -*- coding: utf-8 -*-
"""
Created on Fri Sep  3 01:44:26 2021

@author: 15000
"""

# -*- coding: utf-8 -*-
"""
Created on Fri Sep  3 01:31:55 2021

@author: 15000
"""

# -*- coding: utf-8 -*-
"""
Created on Tue Aug 31 05:04:52 2021

@author: 15000
"""

# -*- coding: utf-8 -*-
"""
Created on Tue Aug 31 01:27:15 2021

@author: 15000
"""

# this is using a non-spectral neural network for approximating u=u(x, c)
from Network_Model import TwoLayerNet
from math import sin, sqrt, acos, cos, pi, tan
from scipy import exp
import random
import numpy as np
import torch
from torch.autograd import Variable
import pdb
import csv
import pandas as pd
import copy
import random
torch.set_default_tensor_type(torch.DoubleTensor)
k = 1
beta = 1
n = 3

# the target function 
def target_fun_new(lst):
    global beta
    x = lst[-1] 
    y = 0
    y += lst[0] * sin((x*3)) * (x/2) / (1+(x/2)**2)**2
        
    return y

# import the training data and the testing data, X stands X and Labels stands for C
#X = [tan(pi * (random.random()-1/2))  for i in range(200)]
#testing = pd.DataFrame(data = X)
#testing.to_csv('X0.csv', header=False, index = False)

#X2 = [tan(pi * (random.random()-1/2))  for i in range(200)]
#testing = pd.DataFrame(data = X2)
#testing.to_csv('X20.csv', header=False, index = False)
#pdb.set_trace()

X_record = csv.reader(open('X0.csv'))
X = []
for i in X_record:
    X.append(float(i[0])*12)
    
X_ori = X
X_record2 = csv.reader(open('X20.csv'))
X2 = []
for i in X_record2:
    X2.append(float(i[0])*12)
    
X_ori2 = X2

Labels = [[random.random() for j in range(1)] for i in range(200)]
Labels2 = [[random.random() for j in range(1)] for i in range(200)]
labels_reader = csv.reader(open('Labels.csv'))
start = 0
for i in labels_reader:
    for j in range(1):
        Labels[start][j] = float(i[j])
        
    start += 1
    
Labels_x = copy.deepcopy(Labels)
for i in range(200):
    Labels_x[i].append(X_ori[i])

labels_reader2 = csv.reader(open('Labels.csv'))
start = 0
for i in labels_reader2:
    for j in range(1):
        Labels2[start][j] = float(i[j])
        
    start += 1
    
Labels_x2 = copy.deepcopy(Labels2)
for i in range(200):
    Labels_x2[i].append(X_ori2[i])

Labels_x2 = torch.tensor(Labels_x2)
X_ref = torch.tensor(Labels_x)
Y_ref = [[target_fun_new(Labels_x[i])] for i in range(200)]
Labels = torch.tensor(Labels)
Labels_x = torch.tensor(Labels_x)
Y = torch.tensor(Y_ref)
Y_ref2 = [[target_fun_new(Labels_x2[i])] for i in range(200)]
Labels2 = torch.tensor(Labels2)
Y2 = torch.tensor(Y_ref2)

# construct the neural network model as well as the optimizer. Here the inputs are both x and c
model = TwoLayerNet(1+1, 10, 1)
criterion = torch.nn.MSELoss(reduction='sum')
optimizer = torch.optim.SGD(model.parameters(), lr=0.0005)
Y_pred = model(Labels_x)
k = 1
optimizer.zero_grad()
loss = criterion(Y,Y_pred)
loss.backward()
print(loss.item())
optimizer.step()
optimizer.zero_grad()
error = []
error2 = []
# perform the training for the non-spectral neural network
while loss.item() > 1e-10 and k < 50000:
    k += 1
    Y_pred = model(Labels_x)
    loss = criterion(Y,Y_pred)
    optimizer.zero_grad()
    loss.backward()
    optimizer.step()
    if k % 10 == 0:
        Y_pred2 = model(Labels_x)
        loss2 = criterion(Y,Y_pred2)
        result = loss2.item()
        Y_pred3 = model(Labels_x2)
        loss3 = criterion(Y2,Y_pred3)
        result3 = loss3.item()
        print(result, result3, k)
        error.append(result)
        error2.append(result3)
    
testing = pd.DataFrame(data = error)
testing.to_csv('error_nonspectral_approximate_train2.csv', header=False, index = False)
testing = pd.DataFrame(data = error2)
testing.to_csv('error_nonspectral_approximate_test2.csv', header=False, index = False)
print(k)