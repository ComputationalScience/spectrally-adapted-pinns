function plot_error_approximate
subplot('Position', [0.10 0.2 0.37 0.7])

error1 = csvread('error_nonspectral_approximate_train2.csv')/200;
error2 = csvread('error_spectral_approximate_train2.csv')/200;
[x1, ~] = size(error1);
k1 = 1:1:x1;
k1 = k1 * 500;
[x2, ~] = size(error2);
k2 = 1:1:x2;
k2 = k2 * 500;
h1 = semilogy(k1, error1, 'color', 'k', 'Linewidth', 1.5);
hold on;
h2 = semilogy(k2, error2, 'color', 'r', 'Linewidth', 1.5);
hold on;
%semilogy(k1(1:10:x2), error1(1:10:x2), '*', 'color', 'k', 'Linewidth', 1.5);
%hold on;
%semilogy(k2(1:10:x2), error2(1:10:x2), 's', 'color', 'r', 'Linewidth', 1.5);
%hold on;
set(gca, 'Fontsize', 20)
[H, h] = legend([h1, h2], ' ~non-spectral', '~spectral', 'interp', 'latex');
H.Box = 'off';
%set(h(4), 'marker', '*')
%set(h(6), 'marker', 's')
set(h(1:2), 'fontsize', 20, 'interp', 'latex')
%set(gca, 'xtick', [0 10000 20000 30000 40000 50000])
%set(gca, 'xticklabel', {0 ,'10^4' ,'2*10^4','3*10^4','4*10^4', '5*10^4'})
set(gca, 'ytick', [1e-5, 1e-4, 1e-3, 1e-2, 1e-1])
set(gca, 'yticklabel', {'10^{-5}', '10^{-4}', '10^{-3}', '10^{-2}', '10^{-1}'})
set(gca, 'yminortick', 'off')
axis([0, 50000, 1e-4, 1e-1])
set(H, 'Position', [0.19, 0.65, 0.2, 0.2])
xlabel('epochs', 'interp', 'latex')
ylabel('MSE', 'interp', 'latex')
title('(a) training error', 'interp', 'latex')

subplot('Position', [0.6 0.2 0.37 0.7])

error1 = csvread('error_nonspectral_approximate_test2.csv')/200;
error2 = csvread('error_spectral_approximate_test2.csv')/200;
[x1, ~] = size(error1);
k1 = 1:1:x1;
k1 = k1 * 500;
[x2, ~] = size(error2);
k2 = 1:1:x2;
k2 = k2 * 500;
h1 = semilogy(k1, error1, 'color', 'k', 'Linewidth', 1.5);
hold on;
h2 = semilogy(k2, error2, 'color', 'r', 'Linewidth', 1.5);
hold on;
%semilogy(k1(1:10:x2), error1(1:10:x2), '*', 'color', 'b', 'Linewidth', 1.5);
%hold on;
%semilogy(k2(1:10:x2), error2(1:10:x2), 's', 'color', 'r', 'Linewidth', 1.5);
%hold on;
set(gca, 'Fontsize', 20)
[H, h] = legend([h1, h2], ' ~non-spectral', '~spectral', 'interp', 'latex');
H.Box = 'off';
%set(h(4), 'marker', '*')
%set(h(6), 'marker', 's')
set(h(1:2), 'fontsize', 20, 'interp', 'latex')
%set(gca, 'xtick', [0 10000 20000 30000 40000 50000])
set(H, 'Position', [0.7, 0.65, 0.2, 0.2])
%set(gca, 'xticklabel', {0 ,'10^4' ,'2*10^4','3*10^4','4*10^4', '5*10^4'})
set(gca, 'ytick', [1e-3, 1e-2, 1e-1, 1])
set(gca, 'yticklabel', {'10^{-3}', '10^{-2}', '10^{-1}', '1'})
set(gca, 'yminortick', 'off')
xlabel('epochs', 'interp', 'latex')
ylabel('MSE', 'interp', 'latex')
axis([0, 50000, 1e-3, 1])
title('(b) testing error', 'interp', 'latex')
end