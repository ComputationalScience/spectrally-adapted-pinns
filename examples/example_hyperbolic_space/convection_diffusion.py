# -*- coding: utf-8 -*-
"""
Created on Fri Oct 29 02:15:09 2021

@author: 15000
"""


"""
Created on Sun Sep  5 08:48:05 2021

@author: 15000
"""

# -*- coding: utf-8 -*-
"""
Created on Thu Sep  2 01:26:17 2021

@author: 15000
"""

# -*- coding: utf-8 -*-
"""
Created on Wed Sep  1 13:55:34 2021

@author: 15000
"""

# -*- coding: utf-8 -*-
"""
Created on Wed Sep  1 05:50:31 2021

@author: 15000
"""

from Network_Model import TwoLayerNet
from math import sin, sqrt, acos, cos, pi
from scipy import exp
import random
import numpy as np
import torch
import time as timeing
from torch.autograd import Variable
import pdb
import csv
import pandas as pd
import copy

xi = 0.3
k = 1
beta = 0.4
beta_y = 0.5
beta_z = 0.7
m = 200
n = 10
ny = 10
nz = 10
torch.set_default_tensor_type(torch.DoubleTensor)

C_record = csv.reader(open('C.csv'))
B_record = csv.reader(open('B.csv'))
A_record = csv.reader(open('A.csv'))
C = []
B = []

for i in C_record:
    C.append(float(i[0]))

for i in B_record:
    B.append(float(i[0]))

A = [[] for i in range(len(C)+1)]
interm = len(C) + 1
ss = 0
for i in A_record:
    for j in i:
        #print(A[ss], ss)
        #print(j, A[i])
        A[ss].append(float(j))
    ss += 1

#print(A, B, C)
#pdb.set_trace()

def Init(n, beta):
    A = [[0 for i in range(n)] for j in range(n)]
    Wx = [0 for i in range(n)]
    for i in range(n):
        if i == 0:
            A[i][i] = 0
            A[i][i+1] = sqrt((1+i)/2)
        elif i == n-1:
            A[i][i] = 0
            A[i][i-1] = sqrt(i/2)
        else:
            A[i][i] = 0
            A[i][i-1] = sqrt(i/2)
            A[i][i+1] = sqrt((i+1) / 2)
    
    A = torch.tensor(A, dtype=torch.float64)
    #print(A)
    #pdb.set_trace()
    #A = A.to(torch.float64)
    (evals, evecs) = torch.eig(A, eigenvectors=True)
    X = [0 for i in range(n)]
    for i in range(n):
        X[i] = float(evals[i][0])
    
    X.sort()
    for i in range(n):
        Wx[i] = 1 / n / Hermite(X[i], n-1, 1)**2
    
    Gamma_x = [1 for i in range(n)]
    Wx = [Wx[i] / beta for i in range(n)]
    X = [X[i] / beta for i in range(n)]
    
    return X, Wx, Gamma_x

def judge_in(ind, gamma, N, K):
    prod = 1
    for i in range(N):
        prod *= max(ind[i], 1)
    if gamma is None:
        if max(ind) <= K:
            return prod, True
        else:
            return prod, False
    else:
        
        
        prod *= max(max(ind), 1)**(-gamma)
        if prod <= K**(1-gamma):
            return prod, True
        else:
            return prod, False

def judge_in_ori(ind, gamma, N, K):    
    prod = 1
    #print(gamma)
    #pdb.set_trace()
    if max(ind) > K:
        return prod, False
    
    for i in range(N):
        prod *= max(ind[i], 1)
    if gamma is None:
        if max(ind) <= K:
            return prod, True
        else:
            return prod, False
    else:
        
        
        prod *= max(max(ind), 1)**(-gamma)
        if prod <= K**(1-gamma):
            return prod, True
        else:
            return prod, False
            
def generate_next_new(ind, N, K, eta):
    ind_next = copy.deepcopy(ind)
    for i in range(N):
        ind_next[i] += 1
        prod, judger = judge_in(ind_next, eta, N, K-1)
        if judger:
            return ind_next
        else:
            ind_next[i] = 0
            if i == N-1:
                return None





def discrete_beta(u, X, Wx, n, Gamma_x, Y, Wy, ny, Gamma_y, Z, Wz, nz, Gamma_z, beta, betay, betaz, key = 0):
    global n_total_ori
    global n_ori, eta
    eta_new = None
    n_new = n 
    U = []
    X_val = [[0 for i in range(n)] for j in range(n)]
    Y_val = [[0 for i in range(ny)] for j in range(ny)]
    Z_val = [[0 for i in range(nz)] for j in range(nz)]
    for i in range(n):
        for j in range(n):
            X_val[i][j] = Hermite(X[i], j, beta)
    
    for i in range(ny):
        for j in range(ny):
            Y_val[i][j] = Hermite(Y[i], j, betay)
        
    for i in range(nz):
        for j in range(nz):
            Z_val[i][j] = Hermite(Z[i], j, betaz)
    
    ind = [0, 0, 0]
    U_tail = []
    K = n
    N = 3
    #print(u)
    #pdb.set_trace()
    #print(u, X_val, Y_val, Z_val)
    #print(torch.tensor(X_val) - torch.tensor(Y_val))
    #pdb.set_trace()
    #eta = 0
    #uu = 0
    #while generate_next_new(ind, N, n_new, eta_new) is not None:
    while ind is not None:
        _, judger = judge_in_ori(ind, eta, N, n-1)
        temp = 0
        for j in range(n):
            for j2 in range(ny):      
                for j3 in range(nz):
                        
                #print(U[uu], K)
                #print(len(Y_val), len(X_val))
                    temp += u[j3*n*ny + j2*n + j] * Wx[j] * X_val[j][ind[0]] / Gamma_x[ind[0]] * Wy[j2] * Y_val[j2][ind[1]] / Gamma_y[ind[1]] * Wz[j3] * Z_val[j3][ind[2]] / Gamma_z[ind[2]]

            
        if judger:
            U.append(temp)
        else:
            U_tail.append(temp)
        #uu += 1
        #print(ind)
        ind = generate_next_new(ind, N, n_new, eta_new)
        
    
    #print(len(U))
    #pdb.set_trace()
    
    #if key == 0:
        
    #    return copy.deepcopy(U + U_tail)
    #else:
    return copy.deepcopy(U)




def Hermite(x, n, beta):
    x = x * beta
    if n == 0:
        y = pi**(-1/4) * exp(-x**2/2)
    elif n == 1:
        y = sqrt(2) * pi**(-1/4) * x * exp(-x**2/2)
    else:
        y0 = pi**(-1/4) * exp(-x**2/2)
        y1 = sqrt(2) * pi**(-1/4) * x * exp(-x**2/2)
        for i in range(n-1):
            y2 = x * sqrt(2/(i+2)) * y1 - sqrt((i+1)/(i+2))*y0
            y0 = y1
            y1 = y2
        y = y2
    
    y = y * sqrt(beta)
    return y
    
    
def target_fun(x, y, z, t=0):
    global xi, k
    result = 1 / sqrt(t+1) *  exp(-z**2/4/(t+1)) * 1 / sqrt(t+3) * 1 / sqrt(t+2) *  exp(-x**2/4/(t+3)) * exp(-y**2/4/(t+2))
    return result


X_x, Wx, Gamma_x = Init(n, beta)
Y_x, Wy, Gamma_y = Init(ny, beta_y)
Z_x, Wz, Gamma_z = Init(nz, beta_z)

Weight_mat = [[0 for i in range(n*ny)] for j in range(n*ny)]
for i in range(n):
    for j in range(ny):    
        Weight_mat[i+j*n][i+j*n] = sqrt(Wx[i]) * sqrt(Wy[j])

Weight_mat = torch.tensor(Weight_mat)


def prediction2(X, y_pred):
    samples = []
    x = X
    for j in range(len(x)):
        samples.append(inverse(x[j], y_pred))
    

    return torch.tensor(samples)

def inverse(x, y, z, n, ny, nz, beta, beta_y, beta_z, u_inv):
    K = n
    N = 3
    #eta = 0
    global eta
    result = [0. for i in range(n*ny*nz)]
    for i in range(ny):
        for j in range(n):
            for r in range(nz):
                
                ind = [0, 0, 0]
                result0 = 0
                #print(len(x), x0)
                x_pos = x[j] 
                y_pos = y[i] 
                z_pos = z[r]
                uu = 0
                while ind is not None:
                    result0 += Hermite(x_pos, ind[0], beta) * Hermite(y_pos, ind[1], beta_y) * Hermite(z_pos, ind[2], beta_z) * u_inv[uu]
                    uu += 1
                    ind = generate_next_new(ind, N, K, eta)
                
                result[r*n*ny + i*n + j] = result0
            #for i in range(n):
            #    for j in range(ny):
            #        result += Hermite(x, i, beta) * Hermite(y, j, beta_y) * u_inv[i+j*n]
    
    return result

def reconstruct(coefs, nx, ny, nz):
    global eta
    N = 3
    K = nx
    ind = [0, 0, 0]
    coef_rec = [0 for i in range(nx*ny*nz)]
    uu = 0
    while ind is not None:
        coef_rec[ind[0] + ind[1]*nx+ind[2]*nx*ny] = coefs[uu]
        uu += 1
        ind = generate_next_new(ind, N, K, None)
    
    return torch.tensor(coef_rec)

def mat_shrink(matrix, nx, ny, nz, n_total):
    global eta
    N = 3
    K = nx
    mat = torch.tensor([[0. for j in range(n_total)] for i in range(n_total)])
    uu = 0
    
    ind0 = [0, 0, 0]
    
    while ind0 is not None:
        ind1 = [0, 0, 0]
        vv = 0
        while ind1 is not None:
            mat[uu, vv] = matrix[ind0[0] + ind0[1] * ny + ind0[2]*ny*nx, ind1[0] + ind1[1] * ny + ind1[2]*ny*nx] 
            vv += 1
            ind1 = generate_next_new(ind1, N, K, eta)
        
        ind0 = generate_next_new(ind0, N, K, eta)
        uu += 1
        
    return mat

def mat_shrink1(matrix, nx, ny, nz, n_total):
    global eta
    N = 3
    K = nx
    mat = torch.tensor([[0. for j in range(n_total)] for i in range(nx*ny*nz)])
    uu = 0
    
    ind0 = [0, 0, 0]
    
    while ind0 is not None:
        ind1 = [0, 0, 0]
        vv = 0
        while ind1 is not None:
            mat[uu, vv] = matrix[ind0[0] + ind0[1] * ny + ind0[2]*ny*nx, ind1[0] + ind1[1] * ny + ind1[2]*ny*nx] 
            vv += 1
            ind1 = generate_next_new(ind1, N, K, eta)
        
        ind0 = generate_next_new(ind0, N, K, None)
        uu += 1
        
    return mat
            
    
    

def f(x, y, z, t, nx, ny, nz):
    y = [0 for i in range(nx*ny*nz)]
    for i in range(nx):
        for j in range(ny):    
            for r in range(nz):
                
                y[i+j*nx+r*nx*ny] = 0#(x[i] * cos(x[i]) + (t+1) *sin(x[i])) * (t+1)**(-3/2) * exp(-x[i]**2/4/(t+1))
    
    return y

def F(n, ny, nz, t, dt, time, beta, betay, betaz):
    global X_x, Wx, Gamma_x, Y_x, Wy, Gamma_y, Z_x, Wz, Gamma_z
    result = []
    for i in range(len(time)):
        y = f(X_x, Y_x, Z_x, time[i], n, ny, nz)
        y = discrete_beta(y, X_x, Wx, n, Gamma_x, Y_x, Wy, ny, Gamma_y, Z_x, Wz, nz, Gamma_z, beta, betay, betaz)
        #y = discrete_beta(y, X_x, Wx, n, Gamma_x, beta)
        result.append(y)
    
    result = torch.tensor(result, dtype=torch.float64)
    result = result.transpose(0, 1)
    return result


B_matrix = torch.tensor(A, dtype=torch.float64)
B_matrix = B_matrix.transpose(0, 1)
Dif_mat = [[0 for i in range(n*ny*nz)] for j in range(n*ny*nz)]


for i in range(n):
    for j in range(ny):
        for r in range(nz):
            
            if i - 1 > 0:
                Dif_mat[r*n*ny + j*n+i][r*n*ny + j*n+i-2] = Dif_mat[r*n*ny +j*n+i][r*n*ny +j*n+i-2]-beta**2 *sqrt((i) * (i-1)) / 2
    
            Dif_mat[r*n*ny +j*n+i][r*n*ny +j*n+i] = Dif_mat[r*n*ny +j*n+i][r*n*ny +j*n+i]+(i+1/2) * beta**2
            if i + 2 < n:
                Dif_mat[r*n*ny +j*n+i][r*n*ny +j*n+i+2] = Dif_mat[r*n*ny +j*n+i][r*n*ny +j*n+i+2]-beta**2 *sqrt((i+1) * (i+2)) / 2
    
for i in range(n):
    for j in range(ny):
        for r in range(nz):
            
            if j - 1 > 0:
                Dif_mat[r*n*ny +j*n+i][r*n*ny +(j-2)*n+i] = Dif_mat[r*n*ny +j*n+i][r*n*ny +(j-2)*n+i]-beta_y**2 *sqrt((j) * (j-1)) / 2
    
            Dif_mat[r*n*ny +j*n+i][r*n*ny +j*n+i] = Dif_mat[r*n*ny +j*n+i][r*n*ny +j*n+i]+(j+1/2) * beta_y**2
            if j + 2 < ny:
                Dif_mat[r*n*ny +j*n+i][r*n*ny +(j+2)*n+i] = Dif_mat[r*n*ny +j*n+i][r*n*ny +(j+2)*n+i]-beta_y**2 *sqrt((j+1) * (j+2)) / 2
           
for i in range(n):
    for j in range(ny):
        for r in range(nz):
            
            if r - 1 > 0:
                Dif_mat[r*n*ny +j*n+i][(r-2)*n*ny +(j)*n+i] = Dif_mat[r*n*ny +j*n+i][(r-2)*n*ny +(j)*n+i]-beta_z**2 *sqrt((r) * (r-1)) / 2
    
            Dif_mat[r*n*ny +j*n+i][r*n*ny +j*n+i] = Dif_mat[r*n*ny +j*n+i][r*n*ny +j*n+i]+(r+1/2) * beta_z**2
            if r + 2 < nz:
                Dif_mat[r*n*ny +j*n+i][(r+2)*n*ny +(j)*n+i] = Dif_mat[r*n*ny +j*n+i][(r+2)*n*ny +(j)*n+i]-beta_z**2 *sqrt((r+1) * (r+2)) / 2
                
                
Dif_mat = torch.tensor(Dif_mat, dtype=torch.float64)

#Matrix = Dif_mat
X_record = csv.reader(open('X.csv'))
Y_record = csv.reader(open('Y.csv'))
X = []
Y = []
for i in X_record:
    X.append(float(i[0]))

for i in Y_record:
    Y.append(float(i[0]))

t = 0
dt = 0.2
time = [(t + dt * C[i]) for i in range(interm-1)]
time.append(t + dt)

#X_ori = X
X = torch.tensor([[time[i]] for i in range(interm)], dtype=torch.float64)
#X = X.transpose(0, 1)

k = 1

Matrix = [[0 for i in range(n*ny*nz)] for j in range(n*ny*nz)]
for i in range(n):
    for j in range(n):
            
        for i2 in range(ny):
            for j2 in range(ny):
                for i3 in range(nz):
                        
                    for j3 in range(nz):
                            
                        Matrix[i3*n*ny+i2*n+i][j3*n*ny+j2*n+j] = Hermite(X_x[i], j, beta) * Hermite(Y_x[i2], j2, beta_y) * Hermite(Z_x[i3], j3, beta_z)

Matrix1 = torch.tensor(Matrix, dtype=torch.float64)

error = []


t_target = 2

def frequency_indicator_x(u_inv, n, Gamma_x, ny, Gamma_y, nz, Gamma_z):
    total = 0
    global eta
    K = n
    N = 3
    residual = 0
    ind = [0, 0, 0]
    uu = 0
    while ind is not None:
        total += u_inv[uu]**2 * Gamma_x[ind[0]]
        if ind[0] > (2*n+2) // 3:
            residual += u_inv[uu]**2 * Gamma_x[ind[0]]
        
        uu += 1
        ind = generate_next_new(ind, N, K, eta)
    
    return sqrt(residual / total)

def frequency_indicator_y(u_inv, n, Gamma_x, ny, Gamma_y, nz, Gamma_z):
    total = 0
    global eta
    K = n
    N = 3
    residual = 0
    ind = [0, 0, 0]
    uu = 0
    while ind is not None:
        total += u_inv[uu]**2 * Gamma_y[ind[1]]
        if ind[1] > (2*ny+2) // 3:
            residual += u_inv[uu]**2 * Gamma_y[ind[1]]
        
        uu += 1
        ind = generate_next_new(ind, N, K, eta)
    
    return sqrt(residual / total)

def frequency_indicator_z(u_inv, n, Gamma_x, ny, Gamma_y, nz, Gamma_z):
    total = 0
    global eta
    K = n
    N = 3
    residual = 0
    ind = [0, 0, 0]
    uu = 0
    while ind is not None:
        total += u_inv[uu]**2 * Gamma_z[ind[2]]
        if ind[2] > (2*nz+2) // 3:
            residual += u_inv[uu]**2 * Gamma_z[ind[2]]
        
        uu += 1
        ind = generate_next_new(ind, N, K, eta)
    
    return sqrt(residual / total)


u = [0 for i in range(n*ny*nz)]
for i in range(n):
    for j in range(ny):      
        for r in range(nz):
            
            u[r*n*ny+j*n+i] = target_fun(X_x[i], Y_x[j], Z_x[r], t)

Y_real = u
#print(target_fun(X_x[1], Y_x[1], Z_x[0], t), u)
#pdb.set_trace()
Weight_mat = [[0 for i in range(n*ny*nz)] for j in range(n*ny*nz)]
for i in range(n):
    for j in range(ny):    
        for r in range(nz):
            Weight_mat[i+j*n+r*n*nz][i+j*n+r*n*nz] = sqrt(Wz[r]) * sqrt(Wx[i]) * sqrt(Wy[j])

Weight_mat = torch.tensor(Weight_mat)
u = torch.tensor([u for i in range(interm)])
freq_ind_x = None
freq_ind_y = None
freq_ind_z = None
q = 0.95
nu = 1 / q
beta_under = 0.2
beta_upper = 5
beta_over = 5
error = []
freq_x_list = []
freq_y_list = []
freq_z_list = []
beta_x_list = []
beta_y_list = []
beta_z_list = []
#print(X_x)
eta = None
#pdb.set_trace()
sk = 0
ttime = timeing.time()

while sk < 10:
    if freq_ind_x is None:
        Y_real_inv = discrete_beta(Y_real, X_x, Wx, n, Gamma_x, Y_x, Wy, ny, Gamma_y, Z_x, Wz, nz, Gamma_z, beta, beta_y, beta_z)
        n_total = len(Y_real_inv)
        zeros = [0 for i in range(n_total)]
        zeros = torch.tensor([zeros for i in range(interm)], dtype = torch.float64)
        #print(Y_real_inv)
        #print(n_total)
        #print(Y_real_inv)
        #pdb.set_trace()
        freq_ind_x = frequency_indicator_x(Y_real_inv, n, Gamma_x, ny, Gamma_y, nz, Gamma_z)
        freq_ind_y = frequency_indicator_y(Y_real_inv, n, Gamma_x, ny, Gamma_y, nz, Gamma_z)
        freq_ind_z = frequency_indicator_z(Y_real_inv, n, Gamma_x, ny, Gamma_y, nz, Gamma_z)
        #print(freq_ind_x, freq_ind_y, freq_ind_z)
        #pdb.set_trace()
        freq_thres_x = freq_ind_x
        freq_thres_y = freq_ind_y
        freq_thres_z = freq_ind_z
        Dif_mat = mat_shrink(Dif_mat, n, ny, nz, n_total)
    else:
        u = []
        lst = [0 for i2 in range(n*ny*nz)]
        coeffs = [float(Y_pred[interm-1, i2]) for i2 in range(n_total)]
        lst = inverse(X_x, Y_x, Z_x, n, ny, nz, beta, beta_y, beta_z, coeffs)
        
        coeffs_ori = copy.deepcopy(coeffs)
        for i1 in range(interm):
            u.append(copy.deepcopy(lst))
            
        u = torch.tensor(u)
        
        Y_real_inv = [float(Y_pred[interm-1, i2]) for i2 in range(n_total)]
        u_inv = copy.deepcopy(Y_real_inv)
        u_inv_ori = copy.deepcopy(u_inv)
        X_x_ori = copy.deepcopy(X_x)
        Wx_ori = copy.deepcopy(Wx)
        Gamma_x_ori = copy.deepcopy(Gamma_x)
        Y_x_ori = copy.deepcopy(Y_x)
        Wy_ori = copy.deepcopy(Wy)
        Gamma_y_ori = copy.deepcopy(Gamma_y)
        beta_ori = beta
        beta_y_ori = beta_y
        beta_z_ori = beta_z
        freq_ind_x = frequency_indicator_x(Y_real_inv, n, Gamma_x, ny, Gamma_y, nz, Gamma_z)
        #print(frequency_indicator_x(Y_real_inv, n, Gamma_x, ny, Gamma_y, nz, Gamma_z), frequency_indicator_y(Y_real_inv, n, Gamma_x, ny, Gamma_y, nz, Gamma_z), frequency_indicator_z(Y_real_inv, n, Gamma_x, ny, Gamma_y, nz, Gamma_z))
    
        
        
        if freq_ind_x > nu *freq_thres_x:
            beta_hat = beta *q
            X_x1, Wx1, Gamma_x1 = Init(n, beta_hat)
            
                        
            lst = inverse(X_x1, Y_x, Z_x, n, ny, nz, beta, beta_y, beta_z, copy.deepcopy(coeffs_ori))
                                                                                                
                    
            #lst = [inverse(X_x1[i2], Y_real_inv, beta) for i2 in range(n)]
            u_inv_new = discrete_beta(lst, X_x1, Wx1, n, Gamma_x1, Y_x, Wy, ny, Gamma_y, Z_x, Wz, nz, Gamma_z, beta_hat, beta_y, beta_z)
            fred_ind_xnew = frequency_indicator_x(u_inv_new, n, Gamma_x1, ny, Gamma_y, nz, Gamma_z)
            
            while fred_ind_xnew < freq_ind_x and beta_hat > beta_under:
                if q == 1:
                    break
                #print(fred_ind_xnew, freq_ind_x)
                #pdb.set_trace()
                
                #print('*3')
                freq_thres_x = fred_ind_xnew
                freq_ind_ori = freq_ind_x
                freq_ind_x = fred_ind_xnew
              

                U_new = lst#######star [inverse(X_x1[i2], u_inv_new, beta_hat) for i2 in range(n)]
                u = torch.tensor([U_new for i in range(interm)])
                beta = beta_hat
                #if beta < 0.45:
                #    print(freq_ind_ori, fred_ind_xnew)
                #    pdb.set_trace()
                X_x = X_x1
                Wx = Wx1
                Gamma_x = Gamma_x1
                u_inv = u_inv_new
                beta_hat *= q
                X_x1, Wx1, Gamma_x1 = Init(n, beta_hat)
                            
                lst = inverse(X_x1, Y_x, Z_x, n, ny, nz, beta, beta_y, beta_z, u_inv)
                #lst = [inverse(X_x1[i2], u_inv_new, beta) for i2 in range(n)]
                u_inv_new = discrete_beta(lst, X_x1, Wx1, n, Gamma_x1, Y_x, Wy, ny, Gamma_y, Z_x, Wz, nz, Gamma_z, beta_hat, beta_y, beta_z)#discrete_beta(lst, X_x1, Wx1, n, Gamma_x1, beta_hat)
                fred_ind_xnew = frequency_indicator_x(u_inv_new, n, Gamma_x1, ny, Gamma_y, nz, Gamma_z)#frequency_indicator(u_inv_new, n, Gamma_x1)
        elif freq_ind_x < freq_thres_x:
            beta_hat = beta /q
            X_x1, Wx1, Gamma_x1 = Init(n, beta_hat)
            lst = inverse(X_x1, Y_x, Z_x, n, ny, nz, beta, beta_y, beta_z, copy.deepcopy(coeffs_ori))
                    
            #lst = [inverse(X_x1[i2], Y_real_inv, beta) for i2 in range(n)]
            u_inv_new = discrete_beta(lst, X_x1, Wx1, n, Gamma_x1, Y_x, Wy, ny, Gamma_y, Z_x, Wz, nz, Gamma_z, beta_hat, beta_y, beta_z)
            fred_ind_xnew = frequency_indicator_x(u_inv_new, n, Gamma_x1, ny, Gamma_y, nz, Gamma_z)
            while fred_ind_xnew < freq_ind_x and beta_hat < beta_upper:
                if q == 1:
                    break
                #print('*3')
                freq_thres_x = fred_ind_xnew
                freq_ind_x = fred_ind_xnew
                U_new = lst#######star [inverse(X_x1[i2], u_inv_new, beta_hat) for i2 in range(n)]
                u = torch.tensor([U_new for i in range(interm)])
                beta = beta_hat
                X_x = X_x1
                Wx = Wx1
                Gamma_x = Gamma_x1
                u_inv = u_inv_new
                beta_hat /= q
                X_x1, Wx1, Gamma_x1 = Init(n, beta_hat)
                lst = inverse(X_x1, Y_x, Z_x, n, ny, nz, beta, beta_y, beta_z, u_inv)
                #lst = [inverse(X_x1[i2], u_inv_new, beta) for i2 in range(n)]
                u_inv_new = discrete_beta(lst, X_x1, Wx1, n, Gamma_x1, Y_x, Wy, ny, Gamma_y, Z_x, Wz, nz, Gamma_z, beta_hat, beta_y, beta_z)#discrete_beta(lst, X_x1, Wx1, n, Gamma_x1, beta_hat)
                fred_ind_xnew = frequency_indicator_x(u_inv_new, n, Gamma_x1, ny, Gamma_y, nz, Gamma_z)#frequency_indicator(u_inv_new, n, Gamma_x1)
        
        freq_ind_y = frequency_indicator_y(u_inv_ori, n, Gamma_x_ori, ny, Gamma_y, nz, Gamma_z)
        #print(freq_ind_y)
        if freq_ind_y > nu *freq_thres_y:
            beta_y_hat = beta_y *q
            Y_x1, Wy1, Gamma_y1 = Init(ny, beta_y_hat)
            lst = inverse(X_x_ori, Y_x1, Z_x, n, ny, nz, beta_ori, beta_y, beta_z, copy.deepcopy(coeffs_ori))
            #print(nz)
            #pdb.set_trace()
            #lst = [inverse(X_x1[i2], Y_real_inv, beta) for i2 in range(n)]
            u_inv_new = discrete_beta(lst, X_x_ori, Wx_ori, n, Gamma_x_ori, Y_x1, Wy1, ny, Gamma_y1, Z_x, Wz, nz, Gamma_z, beta_ori, beta_y_hat, beta_z)
            fred_ind_ynew = frequency_indicator_y(u_inv_new, n, Gamma_x_ori, ny, Gamma_y1, nz, Gamma_z)
            
            #pdb.set_trace()
            while fred_ind_ynew < freq_ind_y and beta_y_hat > beta_under:
                if q == 1:
                    break
                
                #print(fred_ind_ynew, freq_ind_y)
                #print('*2')
                freq_thres_y = fred_ind_ynew
                freq_ind_y = fred_ind_ynew
                U_new = lst#######star [inverse(X_x1[i2], u_inv_new, beta_hat) for i2 in range(n)]
                u = torch.tensor([U_new for i in range(interm)])
                beta_y = beta_y_hat
                Y_x = Y_x1
                Wy = Wy1
                Gamma_y = Gamma_y1
                u_inv = u_inv_new
                beta_y_hat *= q
                Y_x1, Wy1, Gamma_y1 = Init(ny, beta_y_hat)
                lst = inverse(X_x_ori, Y_x1, Z_x, n, ny, nz, beta_ori, beta_y, beta_z, u_inv)
                #lst = [inverse(X_x1[i2], u_inv_new, beta) for i2 in range(n)]
                u_inv_new = discrete_beta(lst, X_x_ori, Wx_ori, n, Gamma_x_ori, Y_x1, Wy1, ny, Gamma_y1, Z_x, Wz, nz, Gamma_z, beta_ori, beta_y_hat, beta_z)#discrete_beta(lst, X_x1, Wx1, n, Gamma_x1, beta_hat)
                fred_ind_ynew = frequency_indicator_y(u_inv_new, n, Gamma_x_ori, ny, Gamma_y1, nz, Gamma_z)#frequency_indicator(u_inv_new, n, Gamma_x1)
        elif freq_ind_y < freq_thres_y:
            beta_y_hat = beta_y /q
            Y_x1, Wy1, Gamma_y1 = Init(ny, beta_y_hat)
            
                        
            lst = inverse(X_x_ori, Y_x1, Z_x, n, ny, nz, beta_ori, beta_y, beta_z, copy.deepcopy(coeffs_ori))
                    
            #lst = [inverse(X_x1[i2], Y_real_inv, beta) for i2 in range(n)]
            u_inv_new = discrete_beta(lst, X_x_ori, Wx_ori, n, Gamma_x_ori, Y_x1, Wy1, ny, Gamma_y1, Z_x, Wz, nz, Gamma_z, beta_ori, beta_y_hat, beta_z)
            fred_ind_ynew = frequency_indicator_y(u_inv_new, n, Gamma_x_ori, ny, Gamma_y1, nz, Gamma_z)
            while fred_ind_ynew < freq_ind_y and beta_y_hat < beta_upper:
                if q == 1:
                    break
                #print('*2')
                freq_thres_y = fred_ind_ynew
                freq_ind_y = fred_ind_ynew
                U_new = lst#######star [inverse(X_x1[i2], u_inv_new, beta_hat) for i2 in range(n)]
                u = torch.tensor([U_new for i in range(interm)])
                beta_y = beta_y_hat
                Y_x = Y_x1
                Wy = Wy1
                Gamma_y = Gamma_y1
                u_inv = u_inv_new
                beta_y_hat /= q
                Y_x1, Wy1, Gamma_y1 = Init(ny, beta_y_hat)
                            
                lst = inverse(X_x, Y_x1, Z_x, n, ny, nz, beta_ori, beta_y, beta_z, u_inv)
                #lst = [inverse(X_x1[i2], u_inv_new, beta) for i2 in range(n)]
                u_inv_new = discrete_beta(lst, X_x_ori, Wx_ori, n, Gamma_x_ori, Y_x1, Wy1, ny, Gamma_y1, Z_x, Wz, nz, Gamma_z, beta_ori, beta_y_hat, beta_z)#discrete_beta(lst, X_x1, Wx1, n, Gamma_x1, beta_hat)
                fred_ind_ynew = frequency_indicator_y(u_inv_new, n, Gamma_x_ori, ny, Gamma_y1, nz, Gamma_z)#frequency_indicator(u_inv_new, n, Gamma_x1)
                
        freq_ind_z = frequency_indicator_z(u_inv_ori, n, Gamma_x_ori, ny, Gamma_y_ori, nz, Gamma_z)
        #print(freq_ind_y)
        if freq_ind_z > nu *freq_thres_z:
            beta_z_hat = beta_z *q
            Z_x1, Wz1, Gamma_z1 = Init(nz, beta_z_hat)
                        
            lst = inverse(X_x_ori, Y_x_ori, Z_x1, n, ny, nz, beta_ori, beta_y_ori, beta_z, copy.deepcopy(coeffs_ori))
                    
            #lst = [inverse(X_x1[i2], Y_real_inv, beta) for i2 in range(n)]
            u_inv_new = discrete_beta(lst, X_x_ori, Wx_ori, n, Gamma_x_ori,Y_x_ori, Wy_ori, ny, Gamma_y_ori, Z_x1, Wz1, nz, Gamma_z1, beta_ori, beta_y_ori, beta_z_hat)
            fred_ind_znew = frequency_indicator_z(u_inv_new, n, Gamma_x_ori, ny, Gamma_y_ori, nz, Gamma_z1)
            #print(fred_ind_znew, freq_ind_z)
            #pdb.set_trace()
            while fred_ind_znew < freq_ind_z and beta_z_hat > beta_under:
                if q == 1:
                    break
                #print('*11', beta_z_hat, freq_ind_z, fred_ind_znew)
                #print(freq_ind_z, fred_ind_znew)
                #pdb.set_trace()
                freq_thres_z = fred_ind_znew
                freq_ind_z = fred_ind_znew
                U_new = lst#######star [inverse(X_x1[i2], u_inv_new, beta_hat) for i2 in range(n)]
                u = torch.tensor([U_new for i in range(interm)])
                beta_z = beta_z_hat
                Z_x = Z_x1
                Wz = Wz1
                Gamma_z = Gamma_z1
                u_inv = u_inv_new
                beta_z_hat *= q
                Z_x1, Wz1, Gamma_z1 = Init(nz, beta_z_hat)
                lst = inverse(X_x_ori, Y_x_ori, Z_x1, n, ny, nz, beta_ori, beta_y_ori, beta_z, u_inv)
                #lst = [inverse(X_x1[i2], u_inv_new, beta) for i2 in range(n)]
                u_inv_new = discrete_beta(lst, X_x_ori, Wx_ori, n, Gamma_x_ori, Y_x_ori, Wy_ori, ny, Gamma_y_ori, Z_x1, Wz1, nz, Gamma_z1, beta_ori, beta_y_ori, beta_z_hat)#discrete_beta(lst, X_x1, Wx1, n, Gamma_x1, beta_hat)
                fred_ind_znew = frequency_indicator_z(u_inv_new, n, Gamma_x_ori, ny, Gamma_y_ori, nz, Gamma_z1)#frequency_indicator(u_inv_new, n, Gamma_x1)
        elif freq_ind_z < freq_thres_z:
            beta_z_hat = beta_z /q
            Z_x1, Wz1, Gamma_z1 = Init(nz, beta_z_hat)
            lst = inverse(X_x_ori, Y_x_ori, Z_x1, n, ny, nz, beta_ori, beta_y_ori, beta_z, copy.deepcopy(coeffs_ori))
                    
            #lst = [inverse(X_x1[i2], Y_real_inv, beta) for i2 in range(n)]
            u_inv_new = discrete_beta(lst, X_x_ori, Wx_ori, n, Gamma_x_ori, Y_x_ori, Wy_ori, ny, Gamma_y_ori, Z_x1, Wz1, nz, Gamma_z1, beta_ori, beta_y_ori, beta_z_hat)
            fred_ind_znew = frequency_indicator_z(u_inv_new, n, Gamma_x_ori, ny, Gamma_y_ori, nz, Gamma_z1)
            while fred_ind_znew < freq_ind_z and beta_z_hat < beta_upper:
                if q == 1:
                    break
                #print('*12', beta_z_hat)
                freq_thres_z = fred_ind_znew
                freq_ind_z = fred_ind_znew
                U_new = lst#######star [inverse(X_x1[i2], u_inv_new, beta_hat) for i2 in range(n)]
                u = torch.tensor([U_new for i in range(interm)])
                beta_z = beta_z_hat
                Z_x = Z_x1
                Wz = Wz1
                Gamma_z = Gamma_z1
                u_inv = u_inv_new
                beta_z_hat /= q
                Z_x1, Wz1, Gamma_z1 = Init(nz, beta_z_hat)

                lst = inverse(X_x_ori, Y_x_ori, Z_x1, n, ny, nz, beta_ori, beta_y_ori, beta_z, u_inv)
                #lst = [inverse(X_x1[i2], u_inv_new, beta) for i2 in range(n)]
                u_inv_new = discrete_beta(lst, X_x_ori, Wx_ori, n, Gamma_x_ori, Y_x_ori, Wy_ori, ny, Gamma_y_ori, Z_x1, Wz1, nz, Gamma_z1, beta_ori, beta_y_ori, beta_z_hat)#discrete_beta(lst, X_x1, Wx1, n, Gamma_x1, beta_hat)
                fred_ind_znew = frequency_indicator_z(u_inv_new, n, Gamma_x_ori, ny, Gamma_y_ori, nz, Gamma_z1)#frequency_indicator(u_inv_new, n, Gamma_x1)
                
            
                
                
                
    #if t > 0:
        
        #print(beta, beta_y, beta_z, t)
        #pdb.set_trace()
        
    X_x, Wx, Gamma_x = Init(n, beta)
    Y_x, Wy, Gamma_y = Init(ny, beta_y)
    Z_x, Wz, Gamma_z = Init(nz, beta_z)
    if t == 0:
        u = [0 for i in range(n*ny*nz)]
        for i in range(n):
            for j in range(ny):
                for r in range(nz):
                    
                    u[r*n*ny+j*n+i] = target_fun(X_x[i], Y_x[j], Z_x[r], t)
        #u = [target_fun(X_x[i], t) for i in range(n)]
        #Y_real = discrete_beta(u, X_x, Wx, n, Gamma_x, beta)
        u = torch.tensor([u for i in range(interm)])
    else:        
        lst = inverse(X_x, Y_x, Z_x, n, ny, nz, beta_ori, beta_y_ori, beta_z_ori, coeffs_ori) 
    
        u_inv = discrete_beta(lst, X_x, Wx, n, Gamma_x, Y_x, Wy, ny, Gamma_y, Z_x, Wz, nz, Gamma_z, beta, beta_y, beta_z)
        u = torch.tensor([lst for i in range(interm)])
   
    Matrix = [[0 for i in range(n*ny*nz)] for j in range(n*ny*nz)]
    for i in range(n):
        for j in range(n):
            for i2 in range(ny):
                for j2 in range(ny):
                    for i3 in range(nz):
                        for j3 in range(nz):
                            
                            Matrix[i3*n*ny+i2*n+i][j3*n*ny+j2*n+j] = Hermite(X_x[i], j, beta) * Hermite(Y_x[i2], j2, beta_y) * Hermite(Z_x[i3], j3, beta_z)

    Matrix1 = torch.tensor(Matrix, dtype=torch.float64)
    Matrix1 = mat_shrink1(Matrix1, n, ny, nz, n_total)
    Weight_mat = [[0 for i in range(n*ny*nz)] for j in range(n*ny*nz)]
    for i in range(n):
        for j in range(ny):    
            for r in range(nz):
                
                Weight_mat[i+j*n+r*n*ny][i+j*n+r*n*ny] = sqrt(Wx[i]) * sqrt(Wy[j]) * sqrt(Wz[r])

    Weight_mat = torch.tensor(Weight_mat)
    #Weight_mat = mat_shrink(Weight_mat, n, ny, nz, n_total)
    
    
    Y_real = [0 for i in range(n*ny*nz)]
    for i in range(n):
        for j in range(ny):
            for r in range(nz):
                
                Y_real[r*n*nz+j*n+i] = target_fun(X_x[i], Y_x[j], Z_x[r], t+dt)
            
    Y_real_inv = discrete_beta(Y_real, X_x, Wx, n, Gamma_x, Y_x, Wy, ny, Gamma_y, Z_x, Wz, nz, Gamma_z, beta, beta_y, beta_z)#(Y_real, X_x, Wx, n, Gamma_x, beta)
    Y_real_inv1 = copy.deepcopy(Y_real_inv)
    Y_real_inv2 = torch.tensor([Y_real_inv], dtype=torch.float64)
    #if t > 0:
    #    Y_real = [0 for i in range(n*ny)]
    #    for i in range(n):
    #        for j in range(ny):
    #            Y_real[j*n+i] = target_fun(X_x[i], Y_x[j], t)
    #        
    #    Y_real_inv = discrete_beta(Y_real, X_x, Wx, n, Gamma_x, Y_x, Wy, ny, Gamma_y, beta, beta_y)#(Y_real, X_x, Wx, n, Gamma_x, beta)
    #    Y_real_inv1 = copy.deepcopy(Y_real_inv)
    #    Y_real_inv2 = torch.tensor([Y_real_inv], dtype=torch.float64)
    #    print(sqrt(sum((torch.matmul(torch.matmul(Matrix1, Y_real_inv2.transpose(0, 1)).transpose(0, 1)[0] - torch.matmul(Matrix1, torch.tensor([u_inv]).transpose(0, 1)).transpose(0, 1)[0], Weight_mat))**2)))
    #    pdb.set_trace()
        
    Dif_mat = [[0 for i in range(n*ny*nz)] for j in range(n*ny*nz)]


    for i in range(n):
        for j in range(ny):
            for r in range(nz):
                
                if i - 1 > 0:
                    Dif_mat[r*n*ny + j*n+i][r*n*ny + j*n+i-2] = Dif_mat[r*n*ny +j*n+i][r*n*ny +j*n+i-2]-beta**2 *sqrt((i) * (i-1)) / 2
        
                Dif_mat[r*n*ny +j*n+i][r*n*ny +j*n+i] = Dif_mat[r*n*ny +j*n+i][r*n*ny +j*n+i]+(i+1/2) * beta**2
                if i + 2 < n:
                    Dif_mat[r*n*ny +j*n+i][r*n*ny +j*n+i+2] = Dif_mat[r*n*ny +j*n+i][r*n*ny +j*n+i+2]-beta**2 *sqrt((i+1) * (i+2)) / 2
        
    for i in range(n):
        for j in range(ny):
            for r in range(nz):
                
                if j - 1 > 0:
                    Dif_mat[r*n*ny +j*n+i][r*n*ny +(j-2)*n+i] = Dif_mat[r*n*ny +j*n+i][r*n*ny +(j-2)*n+i]-beta_y**2 *sqrt((j) * (j-1)) / 2
        
                Dif_mat[r*n*ny +j*n+i][r*n*ny +j*n+i] = Dif_mat[r*n*ny +j*n+i][r*n*ny +j*n+i]+(j+1/2) * beta_y**2
                if j + 2 < ny:
                    Dif_mat[r*n*ny +j*n+i][r*n*ny +(j+2)*n+i] = Dif_mat[r*n*ny +j*n+i][r*n*ny +(j+2)*n+i]-beta_y**2 *sqrt((j+1) * (j+2)) / 2
               
    for i in range(n):
        for j in range(ny):
            for r in range(nz):
                
                if r - 1 > 0:
                    Dif_mat[r*n*ny +j*n+i][(r-2)*n*ny +(j)*n+i] = Dif_mat[r*n*ny +j*n+i][(r-2)*n*ny +(j)*n+i]-beta_z**2 *sqrt((r) * (r-1)) / 2
        
                Dif_mat[r*n*ny +j*n+i][r*n*ny +j*n+i] = Dif_mat[r*n*ny +j*n+i][r*n*ny +j*n+i]+(r+1/2) * beta_z**2
                if r + 2 < nz:
                    Dif_mat[r*n*ny +j*n+i][(r+2)*n*ny +(j)*n+i] = Dif_mat[r*n*ny +j*n+i][(r+2)*n*ny +(j)*n+i]-beta_z**2 *sqrt((r+1) * (r+2)) / 2
                    
                    
    Dif_mat = torch.tensor(Dif_mat, dtype=torch.float64)
        
    Dif_mat = mat_shrink(Dif_mat, n, ny, nz, n_total)
    

    
    time = [(t + dt * C[i]) for i in range(interm-1)]
    time.append(t + dt)
    F_inter = F(n, ny, nz, t, dt, time, beta, beta_y, beta_z)
    model = TwoLayerNet(1, 700, n_total)
    Y_pred = model(X)
    #print(torch.matmul(Matrix1, Y_pred.transpose(0, 1) + dt * torch.matmul(torch.matmul(Dif_mat, Y_pred.transpose(0, 1)), B_matrix)).transpose(0, 1) - u)
    criterion = torch.nn.MSELoss(reduction='sum')
    optimizer = torch.optim.SGD(model.parameters(), lr=0.0002)
    optimizer.zero_grad()
    u_inv = discrete_beta([float(u[0, ii]) for ii in range(n*ny*nz)], X_x, Wx, n, Gamma_x, Y_x, Wy, ny, Gamma_y, Z_x, Wz, nz, Gamma_z, beta, beta_y, beta_z) #torch.tensor()
    u_inv = torch.tensor([u_inv for ii in range(interm)])
   # print(Y_pred + dt * torch.matmul(torch.matmul(Dif_mat, Y_pred.transpose(0, 1)), B_matrix).transpose(0, 1) - u_inv)
   #print(zeros.shape)
    #aaa = Y_pred + dt * torch.matmul(torch.matmul(Dif_mat, Y_pred.transpose(0, 1)), B_matrix).transpose(0, 1) - u_inv
    #print(aaa.shape, zeros.shape)
    #pdb.set_trace()
    #u_inv = discrete_beta(u, X_x, Wx, n, Gamma_x, Y_x, Wy, ny, Gamma_y, Z_x, Wz, nz, Gamma_z, beta, beta_y, beta_z)
    #loss = criterion(zeros,torch.matmul(torch.matmul(Matrix1, Y_pred.transpose(0, 1) + dt * torch.matmul(torch.matmul(Dif_mat, Y_pred.transpose(0, 1)) - F_inter, B_matrix)).transpose(0, 1) - u, Weight_mat))
    loss = criterion(zeros, Y_pred + dt * torch.matmul(torch.matmul(Dif_mat, Y_pred.transpose(0, 1)), B_matrix).transpose(0, 1) - u_inv)
    loss.backward()
    #print(loss.item())
    optimizer.step()
    optimizer.zero_grad()
    while loss.item() > 1e-20 and k < 400000:
        if k % 10000 == 0:
            print(loss.item())
            
        k += 1
        Y_pred = model(X)
        loss = criterion(zeros, Y_pred + dt * torch.matmul(torch.matmul(Dif_mat, Y_pred.transpose(0, 1)), B_matrix).transpose(0, 1) - u_inv)#loss = criterion(zeros,torch.matmul(torch.matmul(Matrix1, Y_pred.transpose(0, 1) + dt * torch.matmul(torch.matmul(Dif_mat, Y_pred.transpose(0, 1)) - F_inter, B_matrix)).transpose(0, 1) - u, Weight_mat))
        
        loss.backward()
        optimizer.step()
        optimizer.zero_grad()
        
    result = sqrt(sum((torch.matmul(torch.matmul(Matrix1, Y_real_inv2.transpose(0, 1)).transpose(0, 1)[0] - torch.matmul(Matrix1, Y_pred.transpose(0, 1)).transpose(0, 1)[interm-1], Weight_mat))**2))
    error.append(result)
    freq_x_list.append(freq_ind_x)
    freq_y_list.append(freq_ind_y)
    freq_z_list.append(freq_ind_z)
    beta_x_list.append(beta)
    beta_y_list.append(beta_y)
    beta_z_list.append(beta_z)
    print(beta, beta_y, beta_z, freq_ind_x, freq_ind_y, freq_ind_z, result, t)
    k = 0
    t += dt
    sk += 1

print(n_total, timeing.time() - ttime)
testing = pd.DataFrame(data = error)
testing.to_csv('error_spectral_parabolic_nonadpative_3D_none_700.csv', header=False, index = False)
testing = pd.DataFrame(data = beta_x_list)
testing.to_csv('betax_spectral_parabolic_nonadpative_3D_none_700.csv', header=False, index = False)
testing = pd.DataFrame(data = beta_y_list)
testing.to_csv('betay_spectral_parabolic_nonadpative_3D_none_700.csv', header=False, index = False)
testing = pd.DataFrame(data = beta_z_list)
testing.to_csv('betaz_spectral_parabolic_nonadpative_3D_none_700.csv', header=False, index = False)
testing = pd.DataFrame(data = freq_x_list)
testing.to_csv('freq_x_spectral_parabolic_nonadpative_3D_none_700.csv', header=False, index = False)
testing = pd.DataFrame(data = freq_y_list)
testing.to_csv('freq_y_spectral_parabolic_nonadpative_3D_none_700.csv', header=False, index = False)
testing = pd.DataFrame(data = freq_z_list)
testing.to_csv('freq_z_spectral_parabolic_nonadpative_3D_none_700.csv', header=False, index = False)