"""
@author: Maziar Raissi
"""

import sys
sys.path.insert(0, '../../Utilities/')

import tensorflow.compat.v1 as tf
import numpy as np
import matplotlib.pyplot as plt
import time
import scipy.io
#from plotting import newfig, savefig
import matplotlib.gridspec as gridspec
from mpl_toolkits.axes_grid1 import make_axes_locatable
import pdb
from math import sqrt, pi, exp, cos
import csv
import pandas as pd

tf.disable_v2_behavior()
np.random.seed(1234)

tf.keras.backend.set_floatx('float64')
class PhysicsInformedNN:
    # Initialize the class
    def __init__(self, x0, u0, x1, layers, dt, lb, ub, q, t, deltat):
        
        self.lb = lb
        self.ub = ub
        self.t = t
        self.deltat = deltat
        self.x0 = x0
        self.x1 = x1
        
        self.u0 = u0
        
        self.layers = layers
        self.dt = dt
        self.q = max(q,1)
        t_steps = csv.reader(open('C.csv'))
        self.t_steps = []
        for i in t_steps:
            self.t_steps.append(float(i[0]))
        
        # Initialize NN
        self.weights, self.biases = self.initialize_NN(layers)
        
        # Load IRK weights
        tmp = np.float64(np.loadtxt('Butcher_IRK%d.txt' % (q), ndmin = 2))
        self.IRK_weights = np.reshape(tmp[0:q**2+q], (q+1,q))
        self.IRK_times = tmp[q**2+q:]
        
        # tf placeholders and graph
        self.sess = tf.Session(config=tf.ConfigProto(allow_soft_placement=True,
                                                     log_device_placement=True))
        
        self.x0_tf = tf.placeholder(tf.float64, shape=(None, self.x0.shape[1]))
        self.x1_tf = tf.placeholder(tf.float64, shape=(None, self.x1.shape[1]))
        self.u0_tf = tf.placeholder(tf.float64, shape=(None, self.u0.shape[1]))
        self.dummy_x0_tf = tf.placeholder(tf.float64, shape=(None, self.q)) # dummy variable for fwd_gradients
        self.dummy_x1_tf = tf.placeholder(tf.float64, shape=(None, self.q+1)) # dummy variable for fwd_gradients
        
        self.U0_pred = self.net_U0(self.x0_tf) # N x (q+1)
        self.U1_pred, self.U1_x_pred= self.net_U1(self.x1_tf) # N1 x (q+1)
        #print(self.U1_pred)
        #pdb.set_trace()
        self.loss = tf.reduce_sum(tf.square(self.u0_tf - self.U0_pred)) + \
                    tf.reduce_sum(tf.square(self.U1_pred[0,:] - 0)) #+ \
                    #tf.reduce_sum(tf.square(self.U1_x_pred[0,:] - self.U1_x_pred[1,:]))                     
        
        self.optimizer = tf.keras.optimizers.Adagrad(self.loss)
        
        self.optimizer_Adam = tf.train.AdamOptimizer(learning_rate=0.001, beta1=0.9, beta2=0.999, epsilon=1e-08, use_locking=False,
    name='Adam')
        self.train_op_Adam = self.optimizer_Adam.minimize(self.loss)
        
        init = tf.global_variables_initializer()
        self.sess.run(init)
        
    def initialize_NN(self, layers):        
        weights = []
        biases = []
        num_layers = len(layers) 
        for l in range(0,num_layers-1):
            W = self.xavier_init(size=[layers[l], layers[l+1]])
            b = tf.Variable(tf.zeros([1,layers[l+1]], dtype=tf.float64), dtype=tf.float64)
            weights.append(W)
            biases.append(b) 
        
        return weights, biases
        
    def xavier_init(self, size):
        in_dim = size[0]
        out_dim = size[1]
              
        xavier_stddev = np.sqrt(2/(in_dim + out_dim)) 
        return tf.Variable(tf.random.truncated_normal([in_dim, out_dim], stddev=tf.cast(xavier_stddev, tf.float64), dtype=tf.float64), dtype=tf.float64)
    
    def neural_net(self, X, weights, biases):
        num_layers = len(weights) + 1
        H = 2.0*(X - self.lb)/(self.ub - self.lb) - 1.0
        for l in range(0,num_layers-2):
            W = weights[l]
            b = biases[l]
            H = tf.tanh(tf.add(tf.matmul(H, W), b))
        W = weights[-1]
        b = biases[-1]
        Y = tf.add(tf.matmul(H, W), b)
        return Y
    
    def neural_net1(self, X, weights, biases):
        num_layers = len(weights) + 1
        H = 2.0*(X - self.lb)/(self.ub - self.lb) - 1.0
        for l in range(0,num_layers-2):
            W = weights[l]
            b = biases[l]
            H = tf.tanh(tf.add(tf.matmul(H, W), b))
        W = weights[-1]
        b = biases[-1]
        Y = tf.add(tf.matmul(H, W), b)
        return Y
    
    def fwd_gradients_0(self, U, x):        
        g = tf.gradients(U, x, grad_ys=self.dummy_x0_tf)[0]
        return tf.gradients(g, self.dummy_x0_tf)[0]
    
    def fwd_gradients_1(self, U, x):        
        g = tf.gradients(U, x, grad_ys=self.dummy_x1_tf)[0]
        return tf.gradients(g, self.dummy_x1_tf)[0]
    
    def net_U0(self, x):
        U1 = self.neural_net(x, self.weights, self.biases)
        U = U1[:,:-1]
        U_x = self.fwd_gradients_0(U, x)
        F = U_x
        H1 = (x + 2)/(self.t_steps[0]*dt + self.t+1)
        H2 = (x + 2)/(self.t_steps[1]*dt + self.t+1)
        H3 = (x + 2)/(self.t_steps[2]*dt + self.t+1)
        H4 = (x + 2)/(self.t_steps[3]*dt + self.t+1)
        H5 = (x + 2)/(self.t_steps[4]*dt + self.t+1)
        H6 = (x + 2)/(self.t_steps[5]*dt + self.t+1)
        matrix = tf.concat([H1, H2, H3, H4, H5, H6], 1)
        F = F *matrix
        U0 = U1 - self.dt*tf.matmul(F, self.IRK_weights.T)
        return U0

    def net_U1(self, x):
        U1 = self.neural_net(x, self.weights, self.biases)
        H1 = tf.cos((x+2) * (self.t + 1))
        U1_x = self.fwd_gradients_1(U1, x)
        return U1 - H1, U1_x # N x (q+1)
    
    def callback(self, loss):
        print('Loss:', loss)
    
    def train(self, nIter):
        tf_dict = {self.x0_tf: self.x0, self.u0_tf: self.u0, self.x1_tf: self.x1,
                   self.dummy_x0_tf: np.ones((self.x0.shape[0], self.q)),
                   self.dummy_x1_tf: np.ones((self.x1.shape[0], self.q+1))}
        
        start_time = time.time()
        for it in range(nIter):
            self.sess.run(self.train_op_Adam, tf_dict)
            
            # Print
            if it % 10000 == 0:
                elapsed = time.time() - start_time
                loss_value = self.sess.run(self.loss, tf_dict)
                print('It: %d, Loss: %.3e, Time: %.2f' % 
                      (it, loss_value, elapsed), self.t)
                start_time = time.time()
        
    
    def predict(self, x_star):
        
        U1_star = self.sess.run(self.U1_pred, {self.x1_tf: x_star})
                    
        return U1_star



def exact_solution(x, t):
    return cos((x+2)*(t+1))
    
if __name__ == "__main__": 
    tttime = time.time()  
    tttime1 = time.time()
    q = 6
    layers = [1, 200, 200, 200, 200, q+1]
    lb = np.array([-1.0])
    ub = np.array([1.0])
    
    N = 200
    data = scipy.io.loadmat('AC.mat')
    
    t = data['tt'].flatten()[:,None] # T x 1
    x = data['x'].flatten()[:,None] # N x 1
    lengthx = len(x)
    Exact = np.real(data['uu']).T # T x N
    idx_t0 = 20
    idx_t1 = 30
    dt = 0.01
    ttime = 0
    t_target = 3
    Error_list = []
    time_llist = []
    deltat = dt
    u0 = np.array([[exact_solution(float(x[i][0]), ttime)] for i in range(lengthx)])
    while ttime < t_target:
        idx_x = [i for i in range(lengthx)]
    
        x0 = x[idx_x,:]

    
        Exact = np.array([exact_solution(float(x[i][0]), ttime + deltat) for i in range(lengthx)])
    # Boudanry data
        x1 = np.vstack((lb))
    
    # Test data
        x_star = x

        model = PhysicsInformedNN(x0, u0, x1, layers, dt, lb, ub, q, ttime, deltat)
        model.train(100000)
    
        U1_pred = model.predict(x_star) 
        time_llist.append(time.time() - tttime1)
        tttime1 = time.time()
        error = np.linalg.norm(U1_pred[:,-1] - Exact, 2)/np.linalg.norm(Exact, 2)
        print('Error: %e' % (error), ttime)
        Error_list.append(error)
        ttime += deltat
        u0 = np.array([[float(U1_pred[:,-1][i])] for i in range(lengthx)])
        u0[-1, 0] = exact_solution(float(x[-1][0]), ttime)
        #pdb.set_trace()
    ######################################################################
    ############################# Plotting ###############################
    ######################################################################  
    test_data = pd.DataFrame(data = Error_list)
    test_data.to_csv('Errors_PINN_t01.csv', header=False, index = False)
    tttime = time.time() - tttime
    test_data = pd.DataFrame(data = [tttime])
    test_data.to_csv('Computational_time_PINN_t01.csv', header=False, index = False)
    test_data = pd.DataFrame(data = time_llist)
    test_data.to_csv('Computational_total_time_PINN_t01.csv', header=False, index = False)
    

    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    