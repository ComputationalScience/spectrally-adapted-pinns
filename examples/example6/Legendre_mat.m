function Legendre_mat(n)
format long;
A = zeros(n);
for i=1:n
    A(i, i) = 0;
    if i > 1
        A(i, i-1) = sqrt((i-1)^2 / (4 * (i-1)^2-1));
    end
    if i < n
        A(i, i+1) = sqrt((i)^2 / (4 * (i)^2-1));
    end
end
[U, V] = eig(A);
X = diag(V);
X = sort(X);
X = (X+1) / 2;

function y=q(t)
    y = 1;
    for j=1:n
        y = y * (t - X(j));
    end 
end

function y = qs(t, y, i)
    y = 1;
    for j=1:n
        if j ~= i
            y = y * (t - X(j));
        end
    end 
end

%ode45(@viral_dynamics,[t0 tf],x0,[],pars);
%[t, x0] = ode45(@qs, [0, X(1)], 0, [], 1);
%disp(x0)
A = zeros(n + 1);
B = zeros(n, 1);
opts = odeset('RelTol',1e-10,'AbsTol',1e-14);
for i=1:n
    for s=1:n
        [t, x0] = ode45(@qs, [0, X(s)], 0, opts, i);
        leng = length(x0);
        A(s, i) = x0(leng) / qs(X(i), 0, i);%ode45(@qs, [0, X(s)], 0, [], i) / qs(X(i), 0, i);
    end
end

for i=1:n
    [t, x0] = ode45(@qs, [0, 1], 0, opts, i);
    leng = length(x0);
    B(i) = x0(leng) / qs(X(i), 0, i);
end

for i=1:n
    A(n+1, i) = B(i);
end
disp(X)
disp(A)
disp(B)
csvwrite('C.csv', X)
csvwrite('A.csv', A)
csvwrite('B.csv', B)
end
