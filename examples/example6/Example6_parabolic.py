# -*- coding: utf-8 -*-
"""
Created on Sun Sep  5 08:48:05 2021

@author: 15000
"""

# -*- coding: utf-8 -*-
"""
Created on Thu Sep  2 01:26:17 2021

@author: 15000
"""

# -*- coding: utf-8 -*-
"""
Created on Wed Sep  1 13:55:34 2021

@author: 15000
"""

# -*- coding: utf-8 -*-
"""
Created on Wed Sep  1 05:50:31 2021

@author: 15000
"""

from Network_Model13 import TwoLayerNet20
from math import sin, sqrt, acos, cos, pi
from scipy import exp
import random
import numpy as np
import torch
from torch.autograd import Variable
import pdb
import csv
import pandas as pd
import copy
from time import time

xi = 0.3
k = 1
beta = 0.8
m = 200
n = 21
torch.set_default_tensor_type(torch.DoubleTensor)

C_record = csv.reader(open('C.csv'))
B_record = csv.reader(open('B.csv'))
A_record = csv.reader(open('A.csv'))
C = []
B = []

for i in C_record:
    C.append(float(i[0]))

for i in B_record:
    B.append(float(i[0]))

A = [[] for i in range(len(C)+1)]
interm = len(C) + 1
ss = 0
for i in A_record:
    for j in i:
        #print(A[ss], ss)
        #print(j, A[i])
        A[ss].append(float(j))
    ss += 1

#print(A, B, C)
#pdb.set_trace()

def Init(n, beta):
    A = [[0 for i in range(n)] for j in range(n)]
    Wx = [0 for i in range(n)]
    for i in range(n):
        if i == 0:
            A[i][i] = 0
            A[i][i+1] = sqrt((1+i)/2)
        elif i == n-1:
            A[i][i] = 0
            A[i][i-1] = sqrt(i/2)
        else:
            A[i][i] = 0
            A[i][i-1] = sqrt(i/2)
            A[i][i+1] = sqrt((i+1) / 2)
    
    A = torch.tensor(A, dtype=torch.float64)
    #print(A)
    #pdb.set_trace()
    #A = A.to(torch.float64)
    (evals, evecs) = torch.eig(A, eigenvectors=True)
    X = [0 for i in range(n)]
    for i in range(n):
        X[i] = float(evals[i][0])
    
    X.sort()
    for i in range(n):
        Wx[i] = 1 / n / Hermite(X[i], n-1, 1)**2
    
    Gamma_x = [1 for i in range(n)]
    Wx = [Wx[i] / beta for i in range(n)]
    X = [X[i] / beta for i in range(n)]
    
    return X, Wx, Gamma_x



def discrete(u, X, Wx, n, Gamma_x):
    global beta
    U = [0 for i in range(n)]
    X_val = [[0 for i in range(n)] for i in range(n)]
    for i in range(n):
        for j in range(n):
            X_val[i][j] = Hermite(X[i], j, beta)
    
    for i in range(n):
        for j in range(n):
            U[i] += u[j] * Wx[j] * X_val[j][i] / Gamma_x[i]
    
    return U

def discrete_beta(u, X, Wx, n, Gamma_x, beta):
    U = [0 for i in range(n)]
    X_val = [[0 for i in range(n)] for i in range(n)]
    for i in range(n):
        for j in range(n):
            X_val[i][j] = Hermite(X[i], j, beta)
    
    for i in range(n):
        for j in range(n):
            U[i] += u[j] * Wx[j] * X_val[j][i] / Gamma_x[i]
    
    return U


def Hermite(x, n, beta):
    x = x * beta
    if n == 0:
        y = pi**(-1/4) * exp(-x**2/2)
    elif n == 1:
        y = sqrt(2) * pi**(-1/4) * x * exp(-x**2/2)
    else:
        y0 = pi**(-1/4) * exp(-x**2/2)
        y1 = sqrt(2) * pi**(-1/4) * x * exp(-x**2/2)
        for i in range(n-1):
            y2 = x * sqrt(2/(i+2)) * y1 - sqrt((i+1)/(i+2))*y0
            y0 = y1
            y1 = y2
        y = y2
    
    y = y * sqrt(beta)
    return y
    
    
def target_fun(x, t=0):
    global xi, k
    y = sin(x)/sqrt(t+1) * exp(-x**2/4/(t+1))
    return y

def MSE(y_pred):
    global n, m
    length = n
    samples = []
    global X, Y_ori
    y = Y_ori
    x = X
    for j in range(m):
        samples.append(inverse(x[0][j], y_pred[0]))
    
    error = np.array([float((y[i] - float(samples[i]))**2) for i in range(m)])
    #print(sqrt(sum((error)**2) / length))
    return sqrt(sum((error)**2) / length)

X_x, Wx, Gamma_x = Init(n, beta)

Weight_mat = [[0 for i in range(n)] for j in range(n)]
for i in range(n):
    Weight_mat[i][i] = sqrt(Wx[i])

Weight_mat = torch.tensor(Weight_mat)


def prediction2(X, y_pred):
    samples = []
    x = X
    for j in range(len(x)):
        samples.append(inverse(x[j], y_pred))
    

    return torch.tensor(samples)

def inverse(x, y, beta):
    n = len(y)
    result = 0
    for i in range(n):
        result += Hermite(x, i, beta) * y[i]
    
    return result

#print(prediction2(X_x, Y_real), torch.tensor(u))
#pdb.set_trace()
def prediction(y_pred):
    samples = []
    global X, Y_ori, m
    x = X
    for j in range(m):
        samples.append(inverse(x[0][j], y_pred[0]))
    

    return Variable(torch.tensor([MSE(y_pred), MSE(y_pred)]), requires_grad=True)

def prediction1(y_pred):
    samples = []
    global X, Y_ori, m
    x = X
    for j in range(m):
        samples.append(inverse(x[0][j], y_pred))
    

    return torch.tensor(samples)

def f(x, t):
    y = [0 for i in range(n)]
    for i in range(n):
        y[i] = (x[i] * cos(x[i]) + (t+1) *sin(x[i])) * (t+1)**(-3/2) * exp(-x[i]**2/4/(t+1))
    
    return y

def F(n, t, dt, times, beta):
    global X_x, Wx, Gamma_x
    #time = [(1/2 - sqrt(15/10)) * dt+t, 1/2*dt+t, (1/2 + sqrt(15/10))*dt +t, 1*dt + t]
    result = []
    for i in range(len(times)):
        y = f(X_x, times[i])
        y = discrete_beta(y, X_x, Wx, n, Gamma_x, beta)
        result.append(y)
    
    result = torch.tensor(result, dtype=torch.float64)
    result = result.transpose(0, 1)
    return result

def F1(n, t, dt):
    global X_x, Wx, Gamma_x
    times = [t]
    result = []
    for i in range(1):
        y = f(X_x, times[i])
        y = discrete_beta(y, X_x, Wx, n, Gamma_x, beta)
        result.append(y)
    
    result = torch.tensor(result, dtype=torch.float64)
    result = result.transpose(0, 1)
    return result


#X_record = [1/2 - sqrt(15/10), 1/2, 1/2 + sqrt(15/10), 1]
B_matrix = [[5/36, 2/9 - 1/sqrt(15), 5/36 - sqrt(15)/30, 0], [5/36 + sqrt(15) / 24, 2/9, 
                                                            5/36 - sqrt(15) / 24, 0], 
            [5/36 + sqrt(15)/30, 2/9 + 1/sqrt(15), 5/36, 0], [5/18, 4/9, 5/18, 0]]

B_matrix = torch.tensor(B_matrix, dtype=torch.float64)
B_matrix = torch.tensor(A, dtype=torch.float64)
B_matrix = B_matrix.transpose(0, 1)
#B_matrix = B_matrix.transpose(0, 1)

Dif_mat = [[0 for i in range(n)] for j in range(n)]


for i in range(n):
    if i - 1 > 0:
        Dif_mat[i][i-2] = -beta**2 *sqrt((i) * (i-1)) / 2
    
    Dif_mat[i][i] = (i+1/2) * beta**2
    if i + 2 < n:
        Dif_mat[i][i+2] = -beta**2 *sqrt((i+1) * (i+2)) / 2
    
            
        
Dif_mat = torch.tensor(Dif_mat, dtype=torch.float64)
Matrix = Dif_mat
#X = [random.normalvariate(0, 1) for i in range(1000)]
#testing_X = pd.DataFrame(data = X)
#testing_X.to_csv('X_time.csv', header=False, index = False)
#pdb.set_trace()
X_record = csv.reader(open('X.csv'))
Y_record = csv.reader(open('Y.csv'))
X = []
Y = []
for i in X_record:
    X.append(float(i[0]))

for i in Y_record:
    Y.append(float(i[0]))

t = 0
dt = 0.1
t_target = 1
#time = [(1/2 - sqrt(15/10)) * dt+t, 1/2*dt+t, (1/2 + sqrt(15/10))*dt +t, 1*dt + t]
times = [(t + dt * C[i]) for i in range(interm-1)]
times.append(t + dt)


F_test = F1(n, t, dt).transpose(0, 1)[0]
X_ori = X
X = torch.tensor([[times[i]] for i in range(interm)], dtype=torch.float64)
#X = X.transpose(0, 1)

Y_ori = [target_fun(float(X_ori[i])) for i in range(m)]
Y_ori1 = [target_fun(float(X_ori[i]), t+dt) for i in range(m)]
#Y = torch.tensor([Y_ori, Y_ori])
Y = torch.tensor([Y_ori, Y_ori, Y_ori, Y_ori], dtype=torch.float64)
#Y.transpose(0, 1)
#y_target = torch.tensor([[0], [0]])
#print(prediction2(X_ori, Y_real) - torch.tensor(Y_ori))

#Y_pred = model(X)
k = 1

Matrix = [[Hermite(X_ori[j], i, beta) for j in range(m)] for i in range(n)]
Matrix = torch.tensor(Matrix, dtype=torch.float64)
Matrix = Matrix.transpose(0, 1)

Matrix1 = [[Hermite(X_x[j], i, beta) for j in range(n)] for i in range(n)]
Matrix1 = torch.tensor(Matrix1, dtype=torch.float64)
Matrix1 = Matrix1.transpose(0, 1)

error = []


Y_real_inv_prev = discrete_beta([target_fun(X_x[i], t) for i in range(n)], X_x, Wx, n, Gamma_x, beta)
Y_real_inv1_prev = copy.deepcopy(Y_real_inv_prev)
Y_real_inv2_prev = torch.tensor(Y_real_inv1_prev, dtype=torch.float64)
Y_real_inv = discrete_beta([target_fun(X_x[i], t+dt) for i in range(n)], X_x, Wx, n, Gamma_x, beta)
Y_real_inv1 = copy.deepcopy(Y_real_inv)
Y_real_inv2 = torch.tensor([Y_real_inv], dtype=torch.float64)
Y_recon = torch.tensor([inverse(X_ori[i], Y_real_inv1, beta) for i in range(m)])
Y_real1 = torch.tensor([Y_real_inv1], dtype=torch.float64).transpose(0, 1)
Y_real_inv = torch.tensor([copy.deepcopy(Y_real_inv), copy.deepcopy(Y_real_inv), copy.deepcopy(Y_real_inv), copy.deepcopy(Y_real_inv)], dtype=torch.float64)
Y_real = torch.tensor([target_fun(X_ori[i], t+dt) for i in range(m)])
#print(Y_recon - Y_real)
#print([target_fun(X_x[i], t) for i in range(n)])
#print(Y_real1, Y_real_inv1)
Y_pred1 = torch.tensor([Y_real_inv1, Y_real_inv1, Y_real_inv1, Y_real_inv1]).to(torch.float64)
#print(torch.matmul(Matrix, Y_real1)- Y_real)
#pdb.set_trace()
#print([target_fun(X_x[i], t+dt) for i in range(n)])
#print(torch.matmul(Dif_mat, Y_real_inv2_prev).transpose(0, 1)[0], F_test)
#pdb.set_trace()
#print(Y_real_inv2_prev - dt * (torch.matmul(Dif_mat, Y_real_inv2_prev) - F_test))
####print(sqrt(sum((Y_real_inv2 - Y_real_inv2_prev.transpose(0, 1)[0] + dt * (torch.matmul(Dif_mat, Y_real_inv2_prev).transpose(0, 1)[0] - F_test))**2) / m))
##print(sqrt(sum((Y_real_inv2 - ( Y_real_inv2_prev - dt * (torch.matmul(Dif_mat, Y_real_inv2_prev) - F_test))).transpose(0, 1)[0]**2) / m))
zeros = [0 for i in range(n)]
zeros = torch.tensor([zeros for i in range(interm)], dtype = torch.float64)
###loss = criterion(u,torch.matmul(Matrix1, Y_pred.transpose(0, 1) + dt * torch.matmul(torch.matmul(Dif_mat, Y_pred.transpose(0, 1)) - F_inter, B_matrix)).transpose(0, 1))

#pdb.set_trace()


def frequency_indicator(u_inv, n, Gamma_x):
    total = 0
    residual = 0
    for i in range(n):
        total += u_inv[i]**2 * Gamma_x[i]
        if i > (2*n+2) // 3:
            residual += u_inv[i]**2 * Gamma_x[i]
    
    return sqrt(residual / total)
    
#def F2(n, t, dt, beta):
#    X_x, Wx, Gamma_x = Init(n, beta)
#    y = f(X_x, t)
#    y = discrete_beta(y, X_x, Wx, n, Gamma_x, beta)
#    result = torch.tensor(y, dtype=torch.float64)
#    return result

u = [target_fun(X_x[i], t) for i in range(n)]
Weight_mat = [[0 for i in range(n)] for j in range(n)]
for i in range(n):
    Weight_mat[i][i] = sqrt(Wx[i])

Weight_mat = torch.tensor(Weight_mat)
    #Y_real = discrete(u, X_x, Wx, n, Gamma_x)
u = torch.tensor([u for i in range(interm)])
freq_ind = None
q = 0.98
nu = 1 / q
beta_under = 0.2
beta_over = 5
error = []
ttime = time()
ck1 = 0 
ck = int(t_target / dt)
while ck1 < ck:
    ck1 += 1
    if freq_ind is None:
        Y_real_inv = discrete_beta([target_fun(X_x[i], t) for i in range(n)], X_x, Wx, n, Gamma_x, beta)
        freq_ind = frequency_indicator(Y_real_inv, n, Gamma_x)
        freq_thres = freq_ind 
    else:
        u = []
        lst = []
        coeffs = [float(Y_pred[interm-1, i2]) for i2 in range(n)]
        for i2 in range(n):
            lst.append(inverse(X_x[i2], coeffs, beta))
        
        for i1 in range(interm):
            u.append(copy.deepcopy(lst))
            
        u = torch.tensor(u)
        
        Y_real_inv = [float(Y_pred[interm-1, i2]) for i2 in range(n)]
        freq_ind = frequency_indicator(Y_real_inv, n, Gamma_x)
        if freq_ind > nu *freq_thres:
            beta_hat = beta *q
            X_x1, Wx1, Gamma_x1 = Init(n, beta_hat)
            lst = [inverse(X_x1[i2], Y_real_inv, beta) for i2 in range(n)]
            u_inv_new = discrete_beta(lst, X_x1, Wx1, n, Gamma_x1, beta_hat)
            fred_ind_new = frequency_indicator(u_inv_new, n, Gamma_x1)
            while fred_ind_new < freq_ind and beta_hat > beta_under:
                if q == 1:
                    break
                
                freq_thres = fred_ind_new
                freq_ind = fred_ind_new
                U_new = [inverse(X_x1[i2], u_inv_new, beta_hat) for i2 in range(n)]
                u = torch.tensor([U_new for i in range(interm)])
                beta = beta_hat
                X_x = X_x1
                Wx = Wx1
                Gamma_x = Gamma_x1
                beta_hat *= q
                X_x1, Wx1, Gamma_x1 = Init(n, beta_hat)
                lst = [inverse(X_x1[i2], u_inv_new, beta) for i2 in range(n)]
                u_inv_new = discrete_beta(lst, X_x1, Wx1, n, Gamma_x1, beta_hat)
                fred_ind_new = frequency_indicator(u_inv_new, n, Gamma_x1)
        elif freq_ind < freq_thres:
            beta_hat = beta /q
            X_x1, Wx1, Gamma_x1 = Init(n, beta_hat)
            lst = [inverse(X_x1[i2], Y_real_inv, beta) for i2 in range(n)]
            u_inv_new = discrete_beta(lst, X_x1, Wx1, n, Gamma_x1, beta_hat)
            fred_ind_new = frequency_indicator(u_inv_new, n, Gamma_x1)
            while fred_ind_new < freq_ind and beta_hat < beta_over:
                if q == 1:
                    break
                
                freq_thres = fred_ind_new
                freq_ind = fred_ind_new
                U_new = [inverse(X_x1[i2], u_inv_new, beta_hat) for i2 in range(n)]
                u = torch.tensor([U_new for i in range(interm)])
                beta = beta_hat
                X_x = X_x1
                Wx = Wx1
                Gamma_x = Gamma_x1
                beta_hat /= q
                X_x1, Wx1, Gamma_x1 = Init(n, beta_hat)
                lst = [inverse(X_x1[i2], u_inv_new, beta) for i2 in range(n)]
                u_inv_new = discrete_beta(lst, X_x1, Wx1, n, Gamma_x1, beta_hat)
                fred_ind_new = frequency_indicator(u_inv_new, n, Gamma_x1)
            
                
                
                
        
        
    X_x, Wx, Gamma_x = Init(n, beta)
    Matrix1 = [[Hermite(X_x[j], i, beta) for j in range(n)] for i in range(n)]
    Matrix1 = torch.tensor(Matrix1, dtype=torch.float64)
    Matrix1 = Matrix1.transpose(0, 1)
        
    if t == 0:
        u = [target_fun(X_x[i], t) for i in range(n)]
        Y_real = discrete_beta(u, X_x, Wx, n, Gamma_x, beta)
        u = torch.tensor([u for i in range(interm)])
    #else:
    #    u = [float(Y_pred[interm-1, i]) for i in range(n)]
        #u = Y_pred[interm - 1]
    #    Y_real = discrete_beta(u, X_x, Wx, n, Gamma_x, beta)
    #    u = torch.tensor([u for i in range(interm)])
        
    #Y_real_inv0 = [target_fun(X_x[i], t) for i in range(n)]
    #u = torch.tensor([Y_real_inv0 for i in range(interm)])
    
    Y_real_inv = discrete_beta([target_fun(X_x[i], t+dt) for i in range(n)], X_x, Wx, n, Gamma_x, beta)
    Y_real_inv1 = copy.deepcopy(Y_real_inv)
    Y_real_inv2 = torch.tensor([Y_real_inv], dtype=torch.float64)
    #if t > 0:
    #    Y_real_inv = discrete([target_fun(X_x[i], t) for i in range(n)], X_x, Wx, n, Gamma_x)
    #    Y_real_inv1 = copy.deepcopy(Y_real_inv)
    #    u_real_inv = discrete(lst, X_x, Wx, n, Gamma_x)
    #    print(sqrt(sum((torch.tensor(u_real_inv) - torch.tensor(Y_real_inv1))**2)/n))
    #    pdb.set_trace()
    #if t > 0:
    #    Y_real_inv = discrete([target_fun(X_x[i], t) for i in range(n)], X_x, Wx, n, Gamma_x)
    #    Y_real_inv1 = copy.deepcopy(Y_real_inv)
    #    Y_real_inv2 = torch.tensor([Y_real_inv], dtype=torch.float64)
    #    print(sqrt(sum((torch.matmul(Matrix1, Y_real_inv2.transpose(0, 1)).transpose(0, 1)[0] - torch.matmul(Matrix1, u.transpose(0, 1)).transpose(0, 1)[interm-1])**2)/n))
    #    pdb.set_trace()
    
    Dif_mat = [[0 for i in range(n)] for j in range(n)]


    for i in range(n):
        if i - 1 > 0:
            Dif_mat[i][i-2] = -beta**2 *sqrt((i) * (i-1)) / 2
    
        Dif_mat[i][i] = (i+1/2) * beta**2
        if i + 2 < n:
            Dif_mat[i][i+2] = -beta**2 *sqrt((i+1) * (i+2)) / 2
    
            
        
    Dif_mat = torch.tensor(Dif_mat, dtype=torch.float64)
    
    times = [(t + dt * C[i]) for i in range(interm-1)]
    times.append(t + dt)
    F_inter = F(n, t, dt, times, beta)
    model = TwoLayerNet20(1, 200, n)
    Y_pred = model(X)
    #print(X)
    #pdb.set_trace()
    criterion = torch.nn.MSELoss(reduction='sum')
    optimizer = torch.optim.SGD(model.parameters(), lr=0.0005)
    #optimizer = torch.optim.SGD(model.parameters(), lr=0.0005, momentum=0.9, weight_decay=0.005)
    
    #optimizer = torch.optim.Adam(model.parameters(), lr=0.002)
    optimizer.zero_grad()
    loss = criterion(zeros,torch.matmul(torch.matmul(Matrix1, Y_pred.transpose(0, 1) + dt * torch.matmul(torch.matmul(Dif_mat, Y_pred.transpose(0, 1)) - F_inter, B_matrix)).transpose(0, 1) - u, Weight_mat))
    loss.backward()
    #print(loss.item())
    optimizer.step()
    optimizer.zero_grad()
    while loss.item() > 1e-16 and k < 100000:
        k += 1
        if k % 10000 == 0 and k > 1:
            print(loss.item())
    #print(loss.item())
        Y_pred = model(X)
        #Z = torch.tensor([[0. for j3 in range(n)] for i3 in range(interm)])
        #for i3 in range(interm):
        #    for j3 in range(n):
        #        Z[i3, j3] += Y_pred[i3, j3]
    #print(B_matrix, shape0, shape1)
    
    ######################
    #shape = B_matrix.shape
    #shape0 = shape[0]
    #shape1 = m
    ##Z2 = torch.zeros(shape0, n)
    ##Y_predd = torch.matmul(Dif_mat, Y_pred.transpose(0, 1)).transpose(0, 1)
    #print(shape, Y_predd.shape, Y_pred.shape)
    #pdb.set_trace()
    #print(Z2.shape)
    
    #pdb.set_trace()
    ###for i in range(shape0):
        # t_i to t_s
    ###    for s in range(shape0):
            #Matrix0 = torch.tensor([[Hermite(X_x[j], ell, beta) for j in range(n)] for ell in range(n)])
            #print(Y_predd[i] - F_inter[i])
            ##inter_va = torch.matmul(Y_predd[i] - F_inter[i], Matrix0)
    ###        inter_va = torch.matmul(Y_predd[i] - F2(n, time[i], dt, beta), Matrix0)
    ###        Z2[s] = Z2[s] + inter_va * B_matrix[i, s]
    
    ###Z2 = Z2 * dt
    ###Z2 = Z2 + torch.matmul(Matrix1, Y_pred.transpose(0, 1)).transpose(0, 1)
    #print(Z2.shape, u.shape)
    ###Z2 = Z2 - u
    
    ###Z2 = torch.matmul(Z2, Weight_mat)
    #print(Y[0] - torch.matmul(Matrix, Y_pred1.transpose(0, 1) - dt * torch.matmul(torch.matmul(Dif_mat, Y_pred1.transpose(0, 1)) - F_inter, B_matrix)).transpose(0, 1)[3])
    #pdb.set_trace()
    ###loss = criterion(u,torch.matmul(Matrix1, Y_pred.transpose(0, 1) + dt * torch.matmul(torch.matmul(Dif_mat, Y_pred.transpose(0, 1)) - F_inter, B_matrix)).transpose(0, 1))
    #########################
        #print(Y_pred.shape, F_inter.shape)
        #print(dt * torch.matmul(Dif_mat, Y_pred.transpose(0, 1)) - F_inter)
        #loss = criterion(zeros,torch.matmul(torch.matmul(Matrix1, Z.transpose(0, 1) + dt * torch.matmul(torch.matmul(Dif_mat, Y_pred.transpose(0, 1)) - F_inter, B_matrix)).transpose(0, 1) - u, Weight_mat))
        loss = criterion(zeros,torch.matmul(torch.matmul(Matrix1, Y_pred.transpose(0, 1) + dt * torch.matmul(torch.matmul(Dif_mat, Y_pred.transpose(0, 1)) - F_inter, B_matrix)).transpose(0, 1) - u, Weight_mat))
    ###loss = criterion(zeros,Z2)
        optimizer.zero_grad()
        loss.backward()
        optimizer.step()
    #result = sqrt(sum((prediction1(Y_pred[0]) - torch.tensor(Y_ori))**2) / m)
        #if (k+1) % 40000 == 0:
        #result = sqrt(sum((prediction1(Y_real) - torch.tensor(Y_ori1))**2) / m)
        #print(torch.matmul(Dif_mat, Y_real1)- F_test)
        #print(torch.matmul(Matrix, Y_pred.transpose(0, 1)).transpose(0, 1)[3])
        #pdb.set_trace()
        #print(Y_real_inv2, Y_pred[3])
        #pdb.set_trace()
        ##print(Y_pred[3], Y_real_inv2)
        ##pdb.set_trace()
        ###result = sqrt(sum(( (Y_real_inv2 - Y_pred[3])**2)))
        ###result = sqrt(sum((Y_real - torch.matmul(Matrix, Y_real_inv2.transpose(0, 1)).transpose(0, 1)[0])**2)/m)
    result = sqrt(sum((torch.matmul(torch.matmul(Matrix1, Y_real_inv2.transpose(0, 1)).transpose(0, 1)[0] - torch.matmul(Matrix1, Y_pred.transpose(0, 1)).transpose(0, 1)[interm-1], Weight_mat))**2))
        #####star result = sqrt(sum(( Y_real_inv2[0] - Y_pred[3])**2)/n)
        #result = sqrt(sum((Y_real - torch.matmul(Matrix, Y_real_inv2_prev - dt * (torch.matmul(Dif_mat, Y_real_inv2_prev) - F_test)))**2) / m)
        #result = sqrt(sum((torch.tensor(Y_ori) - Y_real)**2) / m)
        #print(torch.tensor([[Y_pred[3][i]] for i in range(len(Y_pred[3]))]), Y_real1)
        #result = sqrt(sum((Y_real - torch.matmul(Matrix, torch.tensor([[Y_pred[3][i]] for i in range(len(Y_pred[3]))])))**2) / m)
        #result = loss.item()
            #print(result, k)
        #pdb.set_trace()
    error.append(result)
    print(beta, freq_ind, result, t)
        #error.append(result)
    k = 0
    t += dt
    
    #beta0 = beta
    
    #beta *= 0.95
#torch.save(ode_solve.state_dict(),'net_params_3_50.pkl')
#print(sqrt(sum(( Y_real_inv2[0] - Y_pred[interm-1])**2)))
#print(sqrt(sum(( Y_real_inv2_prev - Y_pred[interm-1])**2)))
#print(Y_real_inv2[0], Y_pred[interm-1], Y_real_inv2_prev)
testing = pd.DataFrame(data = error)
#testing.to_csv('error_spectral_parabolic6_adpative_q10_t005_1_200.csv', header=False, index = False)
print(time() - ttime, beta)