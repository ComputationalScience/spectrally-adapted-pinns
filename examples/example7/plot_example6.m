function plot_example6
subplot('Position', [0.13 0.585 0.34 0.36])

error1 = csvread('lambda_spectral_parabolic6_lambda_100_0new.csv');
error2 = csvread('lambda_spectral_parabolic6_lambda_100_0001new.csv');
error3 = csvread('lambda_spectral_parabolic6_lambda_100_001new.csv');
error1 = abs(error1 - 2);
error2 = abs(error2 - 2);
error3 = abs(error3 - 2);
[x1, ~] = size(error1);
k1 = 0:1:x1-1;
k1 = k1 * 0.1 + 0.05;
%k1 = k1 * 500;
%[x2, ~] = size(error2);
%k2 = 1:1:x2;
%k2 = k2 * 500;
h1 = semilogy(k1, error1, 'color', 'b', 'Linewidth', 1.5);
hold on;
h2 = semilogy(k1, error2, 'color', 'r', 'Linewidth', 1.5);
hold on;
h3 = semilogy(k1, error3, 'color', 'k', 'Linewidth', 1.5);
hold on;
%h4 = plot(k1, error4, 'color', [0, 0.5, 0.5], 'Linewidth', 1.5);
%hold on;
semilogy(k1, error1, '*', 'color', 'b', 'Linewidth', 1.5);
hold on;
semilogy(k1, error2, 's', 'color', 'r', 'Linewidth', 1.5);
hold on;
semilogy(k1, error3, '+', 'color', 'k', 'Linewidth', 1.5);
hold on;
%plot(k1, error4, '<', 'color', [0, 0.5, 0.5], 'Linewidth', 1.5);
%hold on;
set(gca, 'Fontsize', 15)
[H, h] = legend([h1, h2, h3], ' ~$\sigma=0$', '~$\sigma=10^{-3}$', '~$\sigma=10^{-2}$', 'interp', 'latex');
H.Box = 'off';
set(h(5), 'marker', '*')
set(h(7), 'marker', 's')
set(h(9), 'marker', '+')
%set(h(12), 'marker', '<')
set(h(1:3), 'fontsize', 18, 'interp', 'latex')
set(gca, 'xtick', [0 0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9 1])
set(gca, 'xticklabel', {0 ,'', 0.2,'', 0.4,'',  0.6,'',  0.8,'', 1})
set(gca, 'ytick', [1e-6, 1e-5, 1e-4, 1e-3, 1e-2, 1e-1, 1])
set(gca, 'yticklabel', {'10^{-6}', '', '10^{-4}', '', '10^{-2}', '', '1'})
set(gca, 'yminortick', 'off')
axis([0, 1, 1e-6, 1])
set(H, 'Position', [0.27, 0.59, 0.2, 0.15])
xlabel('time', 'interp', 'latex')
ylabel('$|\hat{\lambda}-2|$', 'interp', 'latex')
title('$(a)~\textrm{error of}~ \hat{\lambda}$', 'interp', 'latex')

subplot('Position', [0.615 0.585 0.34 0.36])

error1 = csvread('error_spectral_parabolic6_lambda_100_0new.csv');
error2 = csvread('error_spectral_parabolic6_lambda_100_0001new.csv');
error3 = csvread('error_spectral_parabolic6_lambda_100_001new.csv');
[x1, ~] = size(error1);

k1 = 0:1:x1-1;
k1 = k1 * 0.1 + 0.05;
%k1 = k1 * 500;
%[x2, ~] = size(error2);
%k2 = 1:1:x2;
%k2 = k2 * 500;
h1 = semilogy(k1, error1, 'color', 'b', 'Linewidth', 1.5);
hold on;
h2 = semilogy(k1, error2, 'color', 'r', 'Linewidth', 1.5);
hold on;
h3 = semilogy(k1, error3, 'color', 'k', 'Linewidth', 1.5);
hold on;
semilogy(k1, error1, '*', 'color', 'b', 'Linewidth', 1.5);
hold on;
semilogy(k1, error2, 's', 'color', 'r', 'Linewidth', 1.5);
hold on;
semilogy(k1, error3, '+', 'color', 'k', 'Linewidth', 1.5);
hold on;
set(gca, 'Fontsize', 15)
[H, h] = legend([h1, h2, h3], '~$\sigma=0$', '~$\sigma=10^{-3}$', '~$\sigma=10^{-2}$', 'interp', 'latex');
H.Box = 'off';
set(h(5), 'marker', '*')
set(h(7), 'marker', 's')
set(h(9), 'marker', '+')
set(h(1:3), 'fontsize', 18, 'interp', 'latex')
%set(gca, 'xtick', [0 10000 20000 30000 40000 50000])
%set(gca, 'xticklabel', {0 ,'10^4' ,'2*10^4','3*10^4','4*10^4', '5*10^4'})
set(gca, 'xtick', [0 0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9 1])
set(gca, 'xticklabel', {0 ,'', 0.2,'', 0.4,'',  0.6,'',  0.8,'', 1})
set(gca, 'ytick', [1e-8, 1e-7, 1e-6, 1e-5, 1e-4, 1e-3, 1e-2, 1e-1, 1])
set(gca, 'yticklabel', {'10^{-8}', '', '10^{-6}', '', '10^{-4}', '', '10^{-2}', '', '1'})
set(gca, 'yminortick', 'off')
axis([0, 1, 0, 20])
set(H, 'Position', [0.7, 0.8, 0.2, 0.15])
xlabel('time', 'interp', 'latex')
ylabel('MSE', 'interp', 'latex')
title('(b) MSE', 'interp', 'latex')

subplot('Position', [0.13 0.077 0.34 0.36])

error1 = csvread('beta_spectral_parabolic6_lambda_100_0new.csv');
error2 = csvread('beta_spectral_parabolic6_lambda_100_0001new.csv');
error3 = csvread('beta_spectral_parabolic6_lambda_100_001new.csv');
[x1, ~] = size(error1);
k1 = 0:1:x1-1;
k1 = k1 * 0.1 + 0.05;
%k1 = k1 * 500;
%[x2, ~] = size(error2);
%k2 = 1:1:x2;
%k2 = k2 * 500;
h1 = plot(k1, error1, 'color', 'b', 'Linewidth', 1.5);
hold on;
h2 = plot(k1, error2, 'color', 'r', 'Linewidth', 1.5);
hold on;
h3 = plot(k1, error3, 'color', 'k', 'Linewidth', 1.5);
hold on;
plot(k1, error1, '*', 'color', 'b', 'Linewidth', 1.5);
hold on;
plot(k1, error2, 's', 'color', 'r', 'Linewidth', 1.5);
hold on;
plot(k1, error3, '+', 'color', 'k', 'Linewidth', 1.5);
hold on;
set(gca, 'Fontsize', 15)
[H, h] = legend([h1, h2, h3], ' ~$\sigma=0$', '~$\sigma=10^{-3}$', '~$\sigma=10^{-2}$', 'interp', 'latex');
H.Box = 'off';
set(h(5), 'marker', '*')
set(h(7), 'marker', 's')
set(h(9), 'marker', '+')
set(h(1:3), 'fontsize', 18, 'interp', 'latex')
%set(gca, 'xtick', [0 10000 20000 30000 40000 50000])
%set(gca, 'xticklabel', {0 ,'10^4' ,'2*10^4','3*10^4','4*10^4', '5*10^4'})
set(gca, 'xtick', [0 0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9 1])
set(gca, 'xticklabel', {0 ,'', 0.2,'', 0.4,'',  0.6,'',  0.8,'', 1})
set(gca, 'ytick', [0, 0.2, 0.4, 0.6, 0.8, 1.0])
set(gca, 'yticklabel', {'0', '', '0.4', '', '0.8', ''})
set(gca, 'yminortick', 'off')
axis([0, 1, 0, 1])
set(H, 'Position', [0.18, 0.11, 0.2, 0.15])
xlabel('time', 'interp', 'latex')
ylabel('$\beta$', 'interp', 'latex')
title('(c) scaling factor', 'interp', 'latex')

subplot('Position', [0.615 0.077 0.34 0.36])

error1 = csvread('freq_spectral_parabolic6_lambda_100_0new.csv');
error2 = csvread('freq_spectral_parabolic6_lambda_100_0001new.csv');
error3 = csvread('freq_spectral_parabolic6_lambda_100_001new.csv');
[x1, ~] = size(error1);

k1 = 0:1:x1-1;
k1 = k1 * 0.1 + 0.05;
%k1 = k1 * 500;
%[x2, ~] = size(error2);
%k2 = 1:1:x2;
%k2 = k2 * 500;
h1 = semilogy(k1, error1, 'color', 'b', 'Linewidth', 1.5);
hold on;
h2 = semilogy(k1, error2, 'color', 'r', 'Linewidth', 1.5);
hold on;
h3 = semilogy(k1, error3, 'color', 'k', 'Linewidth', 1.5);
hold on;
semilogy(k1, error1, '*', 'color', 'b', 'Linewidth', 1.5);
hold on;
semilogy(k1, error2, 's', 'color', 'r', 'Linewidth', 1.5);
hold on;
semilogy(k1, error3, '+', 'color', 'k', 'Linewidth', 1.5);
hold on;
set(gca, 'Fontsize', 15)
[H, h] = legend([h1, h2, h3], '~$\sigma=0$', '~$\sigma=10^{-3}$', '~$\sigma=10^{-2}$', 'interp', 'latex');
H.Box = 'off';
set(h(5), 'marker', '*')
set(h(7), 'marker', 's')
set(h(9), 'marker', '+')
set(h(1:3), 'fontsize', 18, 'interp', 'latex')
%set(gca, 'xtick', [0 10000 20000 30000 40000 50000])
%set(gca, 'xticklabel', {0 ,'10^4' ,'2*10^4','3*10^4','4*10^4', '5*10^4'})
set(gca, 'xtick', [0 0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9 1])
set(gca, 'xticklabel', {0 ,'', 0.2,'', 0.4,'',  0.6,'',  0.8,'', 1})
set(gca, 'ytick', [1e-9, 1e-8, 1e-7, 1e-6, 1e-5, 1e-4, 1e-3, 1e-2, 1e-1])
set(gca, 'yticklabel', {'', '10^{-8}', '', '10^{-6}', '', '10^{-4}', '', '10^{-2}', ''})
set(gca, 'yminortick', 'off')
axis([0, 1, 1e-9, 1])
set(H, 'Position', [0.62, 0.295, 0.2, 0.15])
xlabel('time', 'interp', 'latex')
ylabel('$\mathcal{F}(U_N^{\beta})$', 'interp', 'latex')
title('(d) frequency indicator', 'interp', 'latex')
end