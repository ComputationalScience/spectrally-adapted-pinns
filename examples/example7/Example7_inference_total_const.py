# -*- coding: utf-8 -*-
"""
Created on Fri Nov  5 09:15:11 2021

@author: 15000
"""

# -*- coding: utf-8 -*-
"""
Created on Wed Oct  6 15:05:04 2021

@author: 15000
"""
import sys
sys.path.insert(0, '../../utilities/')

from Network_Model82 import TwoLayerNet20
from math import sin, sqrt, acos, cos, pi
from scipy import exp
import random
import numpy as np
import torch
import csv
import time as times
import pandas as pd
import copy
from Hermite import Init, Hermite, discrete_beta, inverse, frequency_indicator

xi = 0.3
k = 1
beta = 0.8
m = 200
n = 17
torch.set_default_tensor_type(torch.DoubleTensor)

# import the Lagendre collocation c_j for time integration, and the matrix of coefficients
# in a high-order RK scheme A = (a)_{ij} as well as the coefficients b_i
C_record = csv.reader(open('C.csv'))
B_record = csv.reader(open('B.csv'))
A_record = csv.reader(open('A.csv'))
C = []
B = []

for i in C_record:
    C.append(float(i[0]))

for i in B_record:
    B.append(float(i[0]))

A = [[] for i in range(len(C)+1)]
interm = len(C) + 1
ss = 0
for i in A_record:
    for j in i:
        A[ss].append(float(j))
    ss += 1

# the analytic solution
def target_fun(x, t=0):
    global xi, k
    y = sin(x)/sqrt(t+1) * exp(-x**2/4/(t+1))
    return y

# contruct the allocation nodes, weights, and norms
X_x, Wx, Gamma_x = Init(n, beta)
Weight_mat = [[0 for i in range(n)] for j in range(n)]
for i in range(n):
    Weight_mat[i][i] = sqrt(Wx[i])

Weight_mat = torch.tensor(Weight_mat)

# the function for the heat source f(x, t)
def f(x, t):
    y = [0 for i in range(n)]
    for i in range(n):
        y[i] = ((x[i] * cos(x[i]) + (t+1) *sin(x[i])) * (t+1)**(-3/2) * exp(-x[i]**2/4/(t+1)))*2 - 1 * (x[i]**2/4/(t+1)**2 - 1/2/(t+1)) * sin(x[i]) / sqrt(t+1) * exp(-x[i]**2/4/(t+1))
    
    return y

def F(n, t, dt, time, beta):
    global X_x, Wx, Gamma_x
    result = []
    for i in range(len(time)):
        y = f(X_x, time[i])
        y = discrete_beta(y, X_x, Wx, n, Gamma_x, beta)
        result.append(y)
    
    result = torch.tensor(result, dtype=torch.float64)
    result = result.transpose(0, 1)
    return result

def F1(n, t, dt):
    global X_x, Wx, Gamma_x
    time = [t]
    result = []
    for i in range(1):
        y = f(X_x, time[i])
        y = discrete_beta(y, X_x, Wx, n, Gamma_x, beta)
        result.append(y)
    
    result = torch.tensor(result, dtype=torch.float64)
    result = result.transpose(0, 1)
    return result

# the MSE contains two terms \|U - u(t_i) - ...\| and \|U - u(t_{i+1}) - ...\|
B_matrix = torch.tensor(A, dtype=torch.float64)
B_matrix = B_matrix.transpose(0, 1)
B_matrix_new = copy.deepcopy(B_matrix)
for i in range(interm - 1):
    for j in range(interm - 1):
        B_matrix_new[i, j] = B_matrix_new[i, j] - B_matrix[i, interm-1]


Dif_mat = [[0 for i in range(n)] for j in range(n)]


for i in range(n):
    if i - 1 > 0:
        Dif_mat[i][i-2] = -beta**2 *sqrt((i) * (i-1)) / 2
    
    Dif_mat[i][i] = (i+1/2) * beta**2
    if i + 2 < n:
        Dif_mat[i][i+2] = -beta**2 *sqrt((i+1) * (i+2)) / 2
    
            
Dif_mat = torch.tensor(Dif_mat, dtype=torch.float64)
X = []
Y = []

t = 0
dt = 0.1
time = [(t + dt * C[i]) for i in range(interm-1)]
F_test = F1(n, t, dt).transpose(0, 1)[0]
X_ori = X
X = torch.tensor([[time[i]] for i in range(interm-1)], dtype=torch.float64)
k = 1

Matrix1 = [[Hermite(X_x[j], i, beta) for j in range(n)] for i in range(n)]
Matrix1 = torch.tensor(Matrix1, dtype=torch.float64)
Matrix1 = Matrix1.transpose(0, 1)

error = []
zeros = [0. for i in range(n)]
zeros = torch.tensor([zeros for i in range((interm-1)*2)], dtype = torch.float64)
t_target = 1

u = [target_fun(X_x[i], t) for i in range(n)]
Weight_mat = [[0 for i in range(n)] for j in range(n)]
for i in range(n):
    Weight_mat[i][i] = sqrt(Wx[i])

Weight_mat = torch.tensor(Weight_mat)
u = torch.tensor([u for i in range(interm)])
# set the parameters in the model, the strength of noise in data is 1e-2
freq_ind = None
q = 0.95
nu = 1 / q
beta_under = 0.2
beta_over = 5
std = 1e-2
error_list = []
lam_list = []
beta_list = []
freq_list = []
data_list = []
while t < t_target:
    # calculate the initial frequency indicator and exterior-error indicator when t = 0
    if freq_ind is None:
        lst = [target_fun(X_x[i], t) + random.normalvariate(0, std) for i in range(n)]
        data_list.append(copy.deepcopy(lst))
        Y_real_inv = discrete_beta(copy.deepcopy(lst), X_x, Wx, n, Gamma_x, beta)
        u = torch.tensor([lst for i8 in range(interm-1)])
        freq_ind = frequency_indicator(Y_real_inv, n, Gamma_x)
        freq_thres = freq_ind 
    else:
        u = []
        lst = copy.deepcopy(u_next_cop)
        data_list.append(copy.deepcopy(lst))      
        for i1 in range(interm-1):
            u.append(copy.deepcopy(lst))
            
        u = torch.tensor(u)
        Y_real_inv = discrete_beta([target_fun(X_x[i], t) for i in range(n)], X_x, Wx, n, Gamma_x, beta)
        freq_ind = frequency_indicator(Y_real_inv, n, Gamma_x)
        # calculate the current frequency indicator
        if freq_ind > nu *freq_thres:
            beta_hat = beta *q
            X_x1, Wx1, Gamma_x1 = Init(n, beta_hat)
            lst = [target_fun(X_x1[i], t) +random.normalvariate(0, std) for i in range(n)]#[inverse(X_x1[i2], Y_real_inv, beta) for i2 in range(n)]
            u_inv_new = discrete_beta(lst, X_x1, Wx1, n, Gamma_x1, beta_hat)
            fred_ind_new = frequency_indicator(u_inv_new, n, Gamma_x1)
            # determine if decreasing the scaling factor is needed 
            # (more diffusive)
            while fred_ind_new < freq_ind and beta_hat > beta_under:
                if q == 1:
                    break
                
                freq_thres = fred_ind_new
                freq_ind = fred_ind_new
                U_new = [inverse(X_x1[i2], u_inv_new, beta_hat) for i2 in range(n)]
                u = torch.tensor([U_new for i in range(interm-1)])
                beta = beta_hat
                X_x = X_x1
                Wx = Wx1
                Gamma_x = Gamma_x1
                beta_hat *= q
                X_x1, Wx1, Gamma_x1 = Init(n, beta_hat)
                lst = [inverse(X_x1[i2], u_inv_new, beta) for i2 in range(n)]
                u_inv_new = discrete_beta(lst, X_x1, Wx1, n, Gamma_x1, beta_hat)
                fred_ind_new = frequency_indicator(u_inv_new, n, Gamma_x1)
        elif freq_ind < freq_thres:
            beta_hat = beta /q
            X_x1, Wx1, Gamma_x1 = Init(n, beta_hat)
            lst = [target_fun(X_x1[i], t) + random.normalvariate(0, std) for i in range(n)]#[inverse(X_x1[i2], Y_real_inv, beta) for i2 in range(n)]
            u_inv_new = discrete_beta(lst, X_x1, Wx1, n, Gamma_x1, beta_hat)
            fred_ind_new = frequency_indicator(u_inv_new, n, Gamma_x1)
            # determine if increasing the scaling factor is needed
            # (less diffusive)
            while fred_ind_new < freq_ind and beta_hat < beta_over:
                if q == 1:
                    break
                
                freq_thres = fred_ind_new
                freq_ind = fred_ind_new
                U_new = [inverse(X_x1[i2], u_inv_new, beta_hat) for i2 in range(n)]
                u = torch.tensor([U_new for i in range(interm-1)])
                beta = beta_hat
                X_x = X_x1
                Wx = Wx1
                Gamma_x = Gamma_x1
                beta_hat /= q
                X_x1, Wx1, Gamma_x1 = Init(n, beta_hat)
                lst = [inverse(X_x1[i2], u_inv_new, beta) for i2 in range(n)]
                u_inv_new = discrete_beta(lst, X_x1, Wx1, n, Gamma_x1, beta_hat)
                fred_ind_new = frequency_indicator(u_inv_new, n, Gamma_x1)
    
    # calculate the new allocation points and weights
    X_x, Wx, Gamma_x = Init(n, beta)
    Matrix1 = [[Hermite(X_x[j], i, beta) for j in range(n)] for i in range(n)]
    Matrix1 = torch.tensor(Matrix1, dtype=torch.float64)
    Matrix1 = Matrix1.transpose(0, 1)
    u_next = [target_fun(X_x[i], t+dt) + random.normalvariate(0, std) for i in range(n)]
    u_next_cop = copy.deepcopy(u_next)
    Y_real_next = discrete_beta(u_next, X_x, Wx, n, Gamma_x, beta)
    u_next = torch.tensor([u_next for i in range(interm - 1)]) 
    # the matrix needed for computing the weak form of the spatial derivative u_xx
    Dif_mat = [[0 for i in range(n)] for j in range(n)]
    for i in range(n):
        if i - 1 > 0:
            Dif_mat[i][i-2] = -beta**2 *sqrt((i) * (i-1)) / 2
    
        Dif_mat[i][i] = (i+1/2) * beta**2
        if i + 2 < n:
            Dif_mat[i][i+2] = -beta**2 *sqrt((i+1) * (i+2)) / 2
    
            
        
    Dif_mat = torch.tensor(Dif_mat, dtype=torch.float64)
    
    time = [(t + dt * C[i]) for i in range(interm-1)]
    F_inter = F(n, t, dt, time, beta)    
    lambda_weight = torch.nn.Parameter(torch.tensor([0.]), requires_grad=True)
    model = TwoLayerNet20(1, 100, n)
    # include the parameter lambda to be inferred from data in the neural network model
    model.register_parameter('lambda_wei', lambda_weight)
    Y_pred = model(X)
    Y_pred0 = Y_pred[:, 0:n]
    criterion = torch.nn.MSELoss(reduction='sum')
    optimizer = torch.optim.SGD(model.parameters(), lr=0.002)
    optimizer.zero_grad()
    Z0 = (torch.matmul(Dif_mat, Y_pred0.transpose(0, 1))) 
    Z1 = torch.tensor([[0. for i in range(interm-1)] for j5 in range(n)])
    for i5 in range(n):
        for j5 in range(interm-1):
            Z1[i5, j5] = Z0[i5, j5] * lambda_weight
    
    # construct the two terms in the MSE
    Z2 = Z1 - F_inter
    B_matrix = B_matrix[0:interm-1, 0:interm-1]
    B_matrix_new = B_matrix_new[0:interm-1, 0:interm-1]
    error0 = torch.matmul(torch.matmul(Matrix1, Y_pred0.transpose(0, 1) + dt * torch.matmul(Z2, B_matrix)).transpose(0, 1) - u, Weight_mat)
    error1 = torch.matmul(torch.matmul(Matrix1, Y_pred0.transpose(0, 1) + dt * torch.matmul(Z2, B_matrix_new)).transpose(0, 1) - u_next, Weight_mat)
    error = torch.tensor([[0. for i4 in range(n)] for j4 in range((interm-1)*2)])
    for i4 in range(n):
        for j4 in range((interm-1)*2):
            if j4 < interm - 1:
                error[j4, i4] = error0[j4, i4]
            else:
                error[j4, i4] = error1[j4-interm+1, i4]

    loss = criterion(zeros, error)
    loss.backward()
    optimizer.step()
    optimizer.zero_grad()
    ttime  = times.time()
    # training
    while loss.item() > 1e-20 and k < 50000:
        k += 1
        if k % 10000 == 0 and k > 0:
            res = model(X)
            print(k, loss.item(), lambda_weight, times.time() - ttime)
            ttime = times.time()
        
        Y_pred = model(X)
        Y_pred0 = Y_pred[:, 0:n]
        Z0 = (torch.matmul(Dif_mat, Y_pred0.transpose(0, 1))) 
        Z1 = torch.tensor([[0. for i in range(interm-1)] for j5 in range(n)])
        for i5 in range(n):
            for j5 in range(interm-1):
                Z1[i5, j5] = Z0[i5, j5] * lambda_weight
    
        Z2 = Z1 - F_inter
        
        error0 = torch.matmul(torch.matmul(Matrix1, Y_pred0.transpose(0, 1) + dt * torch.matmul(Z2, B_matrix)).transpose(0, 1) - u, Weight_mat)
        error1 = torch.matmul(torch.matmul(Matrix1, Y_pred0.transpose(0, 1) + dt * torch.matmul(Z2, B_matrix_new)).transpose(0, 1) - u_next, Weight_mat)
        error = torch.tensor([[0. for i4 in range(n)] for j4 in range((interm-1)*2)])
        for i4 in range(n):
            for j4 in range((interm-1)*2):
                if j4 < interm - 1:
                    error[j4, i4] = error0[j4, i4]
                else:
                    error[j4, i4] = error1[j4-interm+1, i4]

        loss = criterion(zeros, error)
        loss.backward()
        optimizer.step()
        optimizer.zero_grad()

    error_list.append(sqrt(loss.item()))
    lam_list.append(float(lambda_weight))
    beta_list.append(beta)
    freq_list.append(freq_ind)    
    print(beta, freq_ind, loss.item(), t, float(lambda_weight))#para_list)
    k = 0
    t += dt

testing = pd.DataFrame(data = data_list)
testing.to_csv('data_spectral_parabolic6_lambda_100_001new.csv', header=False, index = False)
testing = pd.DataFrame(data = error_list)
testing.to_csv('error_spectral_parabolic6_lambda_100_001new.csv', header=False, index = False)
testing = pd.DataFrame(data = lam_list)
testing.to_csv('lambda_spectral_parabolic6_lambda_100_001new.csv', header=False, index = False)
testing = pd.DataFrame(data = beta_list)
testing.to_csv('beta_spectral_parabolic6_lambda_100_001new.csv', header=False, index = False)
testing = pd.DataFrame(data = freq_list)
testing.to_csv('freq_spectral_parabolic6_lambda_100_001new.csv', header=False, index = False)
print(k)
