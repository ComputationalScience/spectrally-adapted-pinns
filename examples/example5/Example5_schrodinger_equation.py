# -*- coding: utf-8 -*-
"""
Created on Fri Oct 29 06:27:00 2021

@author: 15000
"""

# -*- coding: utf-8 -*-
"""
Created on Mon Oct 11 05:10:09 2021

@author: 15000
"""
import sys
sys.path.insert(0, '../../utilities/')

from Network_Model import TwoLayerNet20
import math
from scipy import exp, sin, sqrt, cos, pi
import random
import numpy as np
import torch
from torch.autograd import Variable
import pdb
import csv
import pandas as pd
import copy
from exterior_indicator_complex import exterior_indicator_complex
from Hermite_complex import Init, Hermite, discrete_beta, inverse, frequency_indicator

xi = 0.3
k = 1
beta = 0.8
m = 200
n = 25
torch.set_default_tensor_type(torch.DoubleTensor)

# import the Lagendre collocation c_j for time integration, and the matrix of coefficients
# in a high-order RK scheme A = (a)_{ij} as well as the coefficients b_i
C_record = csv.reader(open('C.csv'))
B_record = csv.reader(open('B.csv'))
A_record = csv.reader(open('A.csv'))
C = []
B = []

for i in C_record:
    C.append(float(i[0]))

for i in B_record:
    B.append(float(i[0]))

A = [[] for i in range(len(C)+1)]
interm = len(C) + 1
ss = 0
for i in A_record:
    for j in i:
        A[ss].append(float(j))
    ss += 1


    
# real and imaginary parts of the analytic solution
def target_fun(x, t=0):
    xi = 1
    k = 0.5
    y = 1 / np.sqrt(xi + 1j * t) * np.exp(1j*k*(x - k*t) - ((x-2*k*t)**2 / (4*(xi+1j*t))))
    return y.real

def target_fun_complex(x, t=0):
    xi = 1
    k = 0.5
    y = 1 / np.sqrt(xi + 1j * t) * np.exp(1j*k*(x - k*t) - ((x-2*k*t)**2 / (4*(xi+1j*t))))
    return y.imag

# contruct the allocation nodes, weights, and norms 
X_x, Wx, Gamma_x = Init(n, beta)
Weight_mat = [[0 for i in range(n)] for j in range(n)]
for i in range(n):
    Weight_mat[i][i] = sqrt(Wx[i])

Weight_mat = torch.tensor(Weight_mat)

# real and imaginary parts of a possible potential term in the Schrodinger equaiton. 
# In our example it is 0
def f(x, t):
    y = [0 for i in range(n)]
    for i in range(n):
        y[i] = 0
    
    return y


def f_complex(x, t):
    y = [0. for i in range(n)]
    for i in range(n):
        y[i] = 0
    
    return y

def F(n, t, dt, time, beta):
    global X_x, Wx, Gamma_x
    result = []
    for i in range(len(time)):
        y = f(X_x, time[i])
        y1 = f_complex(X_x, time[i])
        y = discrete_beta(y+y1, X_x, Wx, n, Gamma_x, beta) 
        result.append(y)
    
    result = torch.tensor(result, dtype=torch.float64)
    result = result.transpose(0, 1)
    return result

B_matrix = torch.tensor(A, dtype=torch.float64)
B_matrix = B_matrix.transpose(0, 1)
Dif_mat = [[0 for i in range(n)] for j in range(n)]


for i in range(n):
    if i - 1 > 0:
        Dif_mat[i][i-2] = -beta**2 *sqrt((i) * (i-1)) / 2
    
    Dif_mat[i][i] = (i+1/2) * beta**2
    if i + 2 < n:
        Dif_mat[i][i+2] = -beta**2 *sqrt((i+1) * (i+2)) / 2
        
Dif_mat = torch.tensor(Dif_mat, dtype=torch.float64)
X = []
Y = []
t = 0
dt = 0.1
time = [(t + dt * C[i]) for i in range(interm-1)]
time.append(t + dt)
X = torch.tensor([[time[i]] for i in range(interm)], dtype=torch.float64)
k = 1
Matrix1 = [[Hermite(X_x[j], i, beta) for j in range(n)] for i in range(n)]
Matrix1 = torch.tensor(Matrix1, dtype=torch.float64)
Matrix1 = Matrix1.transpose(0, 1)
error = []
zeros = [0 for i in range(n)]
zeros = torch.tensor([zeros for i in range(interm)], dtype = torch.float64)
t_target = 1
    

u = [target_fun(X_x[i], t) for i in range(n)]
Weight_mat = [[0 for i in range(n)] for j in range(n)]
for i in range(n):
    Weight_mat[i][i] = sqrt(Wx[i])

Weight_mat = torch.tensor(Weight_mat)
u = torch.tensor([u for i in range(interm)])
# set the parameters in the model
freq_ind = None
freq_thres_p = None
thres = 2
thres0 = 2
update = 1.4
q = 0.98
gamma_move = 1.001
nu = 1 / q
beta_under = 0.2
beta_over = 5
error = []
move_list = []
beta_list = []
freq_list = []
exter_list = []
n_list = []
while t < t_target:
    # calculate the initial frequency indicator and exterior-error indicator when t = 0
    if freq_ind is None:
        Y_real_inv = discrete_beta([target_fun(X_x[i], t) for i in range(n)] + [target_fun_complex(X_x[i], t) for i in range(n)], X_x, Wx, n, Gamma_x, beta)
        freq_ind = frequency_indicator(Y_real_inv, n, Gamma_x)
        freq_thres_p = freq_ind
        freq_thres = freq_ind 
        X_x1, Wx1, Gamma_x1 = Init(n, beta)       
        exter_ind_thres = exterior_indicator_complex(Y_real_inv, X_x1, Wx1, n, beta, Gamma_x1, 1, key=0)[-1]
        exter_ind = exter_ind_thres
        move = 0
    # when t > 0
    else:
        result = exterior_indicator_complex([float(Y_pred[interm-1, i2]) for i2 in range(2*n)], X_x, Wx, n, beta, Gamma_x, exter_ind_thres * gamma_move, key=1)
        exter_ind = result[-1]
        xl = result[0]
        # if the moving technique is activated, change the displacement variable called move
        if xl != 0:
            exter_ind_thres = exter_ind
            move += xl
            
        u = []
        lst = []
        # we separately calculate the spectral decomposition of the numerical solution for
        # its real and imaginary parts
        coeffs = [float(Y_pred[interm-1, i2]) for i2 in range(n)]
        coeffs1 = [float(Y_pred[interm-1, i2+n]) for i2 in range(n)]
        u_inv = copy.deepcopy(coeffs) + copy.deepcopy(coeffs1)
        if t > 0:
            Y_real_inv = discrete_beta([target_fun(X_x[i], t) for i in range(n)] + [target_fun_complex(X_x[i], t) for i in range(n)], X_x, Wx, n, Gamma_x, beta)
            Y_real_inv1 = copy.deepcopy(Y_real_inv)
            Y_real_inv2 = torch.tensor([Y_real_inv], dtype=torch.float64)
            u_inv_torch = torch.tensor([u_inv], dtype=torch.float64)
        for i2 in range(n):
            lst.append(inverse(X_x[i2]+xl, coeffs, beta))
        
        for i2 in range(n):
            lst.append(inverse(X_x[i2]+xl, coeffs1, beta))
        
        for i1 in range(interm):
            u.append(copy.deepcopy(lst))
            
        u = torch.tensor(u)
        Y_real_inv = copy.deepcopy(u_inv)
        # calculate the current frequency indicator in the x dimension
        freq_ind = frequency_indicator(Y_real_inv, n, Gamma_x)
        if freq_ind > nu *freq_thres:
            beta_hat = beta *q
            X_x1, Wx1, Gamma_x1 = Init(n, beta_hat)
            lst = [inverse(X_x1[i2], Y_real_inv[0:n], beta) for i2 in range(n)] + [inverse(X_x1[i2], Y_real_inv[n:2*n], beta) for i2 in range(n)]
            u_inv_new = discrete_beta(lst, X_x1, Wx1, n, Gamma_x1, beta_hat)
            if t > 0:
                Matrix1 = [[Hermite(X_x1[j], i, beta_hat) for j in range(n)] for i in range(n)]
                Matrix1 = torch.tensor(Matrix1, dtype=torch.float64)
                Matrix1 = Matrix1.transpose(0, 1)
                Matrix2 = torch.tensor([[0. for i in range(2*n)] for j in range(2*n)])
                for i in range(n):
                    for j in range(n):
                        Matrix2[i, j] = Matrix1[i, j]
                        Matrix2[i+n, j+n] = Matrix1[i, j]
    
                Matrix1 = Matrix2
                Y_real_inv00 = discrete_beta([target_fun(X_x1[i], t) for i in range(n)] + [target_fun_complex(X_x1[i], t) for i in range(n)], X_x1, Wx1, n, Gamma_x1, beta_hat)
                Y_real_inv10 = copy.deepcopy(Y_real_inv00)
                Y_real_inv20 = torch.tensor([Y_real_inv10], dtype=torch.float64)
                u_inv_torch = torch.tensor([u_inv_new], dtype=torch.float64)
                
            fred_ind_new = frequency_indicator(u_inv_new, n, Gamma_x1)
            # determine if decreasing the scaling factor is needed 
            # (more diffusive)
            while fred_ind_new < freq_ind and beta_hat > beta_under:
                if q == 1:
                    break
                
                freq_thres = fred_ind_new
                freq_ind = fred_ind_new
                U_new = [inverse(X_x1[i2], u_inv_new[0:n], beta_hat) for i2 in range(n)] + [inverse(X_x1[i2], u_inv_new[n:2*n], beta_hat) for i2 in range(n)]
                u_inv = copy.deepcopy(u_inv_new)
                
                u = torch.tensor([U_new for i in range(interm)])
                beta = beta_hat
                X_x = copy.deepcopy(X_x1)
                Wx = copy.deepcopy(Wx1)
                Gamma_x = copy.deepcopy(Gamma_x1)
                #exter_ind_thres = exterior_indicator_complex(u_inv, X_x, Wx, n, beta, Gamma_x, 1, key=0)[-1]
                beta_hat *= q
                X_x1, Wx1, Gamma_x1 = Init(n, beta_hat)
                lst = [inverse(X_x1[i2], u_inv_new[0:n], beta) for i2 in range(n)] + [inverse(X_x1[i2], u_inv_new[n:2*n], beta) for i2 in range(n)]
                u_inv_new = discrete_beta(lst, X_x1, Wx1, n, Gamma_x1, beta_hat)
                fred_ind_new = frequency_indicator(u_inv_new, n, Gamma_x1)
                
                if t > 0:
                    Matrix1 = [[Hermite(X_x[j], i, beta) for j in range(n)] for i in range(n)]
                    Matrix1 = torch.tensor(Matrix1, dtype=torch.float64)
                    Matrix1 = Matrix1.transpose(0, 1)
                    Matrix2 = torch.tensor([[0. for i in range(2*n)] for j in range(2*n)])
                    for i in range(n):
                        for j in range(n):
                            Matrix2[i, j] = Matrix1[i, j]
                            Matrix2[i+n, j+n] = Matrix1[i, j]
    
                    Matrix1 = Matrix2
                    
                    Y_real_inv00 = discrete_beta([target_fun(X_x[i], t) for i in range(n)] + [target_fun_complex(X_x[i], t) for i in range(n)], X_x, Wx, n, Gamma_x, beta)
                    Y_real_inv10 = copy.deepcopy(Y_real_inv00)
                    Y_real_inv20 = torch.tensor([Y_real_inv10], dtype=torch.float64)
                    u_inv_torch = torch.tensor([u_inv], dtype=torch.float64)
        
        if t > 0:
            Matrix1 = [[Hermite(X_x[j], i, beta) for j in range(n)] for i in range(n)]
            Matrix1 = torch.tensor(Matrix1, dtype=torch.float64)
            Matrix1 = Matrix1.transpose(0, 1)
            Matrix2 = torch.tensor([[0. for i in range(2*n)] for j in range(2*n)])
            for i in range(n):
                for j in range(n):
                    Matrix2[i, j] = Matrix1[i, j]
                    Matrix2[i+n, j+n] = Matrix1[i, j]
    
            Matrix1 = Matrix2
            Y_real_inv = discrete_beta([target_fun(X_x[i], t) for i in range(n)] + [target_fun_complex(X_x[i], t) for i in range(n)], X_x, Wx, n, Gamma_x, beta)
            Y_real_inv1 = copy.deepcopy(Y_real_inv)
            Y_real_inv2 = torch.tensor([Y_real_inv], dtype=torch.float64)
            u_inv_torch = torch.tensor([u_inv], dtype=torch.float64)
            
        elif freq_ind < freq_thres:
            beta_hat = beta /q
            X_x1, Wx1, Gamma_x1 = Init(n, beta_hat)
            lst = [inverse(X_x1[i2], Y_real_inv[0:n], beta) for i2 in range(n)] + [inverse(X_x1[i2], Y_real_inv[n:2*n], beta) for i2 in range(n)]
            u_inv_new = discrete_beta(lst, X_x1, Wx1, n, Gamma_x1, beta_hat)
            fred_ind_new = frequency_indicator(u_inv_new, n, Gamma_x1)
            # determine if increasing the scaling factor is needed 
            # (less diffusive)
            while fred_ind_new < freq_ind and beta_hat < beta_over:
                if q == 1:
                    break
                
                freq_thres = fred_ind_new
                freq_ind = fred_ind_new
                U_new = [inverse(X_x1[i2], u_inv_new[0:n], beta_hat) for i2 in range(n)] + [inverse(X_x1[i2], u_inv_new[n:2*n], beta_hat) for i2 in range(n)]
                u = torch.tensor([U_new for i in range(interm)])
                u_inv = u_inv_new
                beta = beta_hat
                X_x = X_x1
                Wx = Wx1
                Gamma_x = Gamma_x1
                #exter_ind_thres = exterior_indicator_complex(u_inv, X_x, Wx, n, beta, Gamma_x, 1, key=0)[-1]
                beta_hat /= q
                X_x1, Wx1, Gamma_x1 = Init(n, beta_hat)
                lst = [inverse(X_x1[i2], u_inv_new[0:n], beta) for i2 in range(n)] + [inverse(X_x1[i2], u_inv_new[0:2*n], beta) for i2 in range(n)]
                u_inv_new = discrete_beta(lst, X_x1, Wx1, n, Gamma_x1, beta_hat)
                fred_ind_new = frequency_indicator(u_inv_new, n, Gamma_x1)
        

        
    ll = 0
    marker = 0
    decrease = 0
    # if the current frequency indicator is smaller than the threshold, could consider
    # decrease the expansion order if the frequency indicator after coarsening is still
    # smaller than the threshold
    while decrease != 1 and freq_ind < freq_thres_p / thres0 and n > 0 and t > 0:
        if thres0 == 1:
            break
        
        decrease = 1
        n0 = n - 1
        X_node10, W_x10, Gamma_x10 = Init(n0, beta)
        u_inv_next3 = [0 for i in range(2*n0)]
        for i in range(n0):
            u_inv_next3[i] = u_inv[i]
            u_inv_next3[i+n0] = u_inv[i+n]
        
        U2_inv_nexty0 = u_inv_next3
        freq_ind_next = frequency_indicator(U2_inv_nexty0, n0, Gamma_x10)
        if freq_ind_next < freq_ind:
            decrease = 2
            u_inv = U2_inv_nexty0
            freq_ind = freq_ind_next
            n = n0
            X_x, Wx, Gamma_x = Init(n, beta)
            u = []
            for i in range(interm):
                u.append([inverse(X_x[i2], u_inv[0:n], beta) for i2 in range(n)] + [inverse(X_x[i2], u_inv[n:2*n], beta) for i2 in range(n)])
            
            u = torch.tensor(u)
    
    markkk = 0
    # if the current frequency indicator is larger than the threshold, increase 
    # the expansion order
    while decrease == 0 and ll < 3 and freq_ind > freq_thres_p * thres and t > 0:
        if thres0 == 1:
            break
        
        marker = 1
        ll += 1
        n += 1
        if markkk == 0:
            X_x0 = X_x
            Wx0 = Wx
            Gamma_x0 = Gamma_x
            u_inv0 = u_inv
            n0 = n - 1
            markkk = 1
            
        X_x, Wx, Gamma_x = Init(n, beta)
        u_inv_next3 = [0 for i in range(2*n)]
        for i in range(n-1):
            u_inv_next3[i] = u_inv[i]
            u_inv_next3[i+n] = u_inv[i+(n-1)]
        
        u_inv = u_inv_next3
        u = []
        for i in range(interm):
            u.append([inverse(X_x[i2], u_inv[0:n], beta) for i2 in range(n)] + [inverse(X_x[i2], u_inv[n:2*n], beta) for i2 in range(n)])
            
        u = torch.tensor(u)
        freq_ind = frequency_indicator(u_inv, n, Gamma_x)
    
    if marker == 1:
        thres *= update
        freq_thres_p = freq_ind
        exter_ind_thres = exterior_indicator_complex(u_inv, X_x, Wx, n, beta, Gamma_x, 1, key=0)[-1]
    if decrease == 2:
        freq_thres_p = freq_ind
        exter_ind_thres = exterior_indicator_complex(u_inv, X_x, Wx, n, beta, Gamma_x, 1, key=0)[-1]
    
    
    X_x, Wx, Gamma_x = Init(n, beta)
    Matrix1 = [[Hermite(X_x[j], i, beta) for j in range(n)] for i in range(n)]
    Matrix1 = torch.tensor(Matrix1, dtype=torch.float64)
    Matrix1 = Matrix1.transpose(0, 1)
    Matrix2 = torch.tensor([[0. for i in range(2*n)] for j in range(2*n)])
    for i in range(n):
        for j in range(n):
            Matrix2[i, j] = Matrix1[i, j]
            Matrix2[i+n, j+n] = Matrix1[i, j]
    
    Matrix1 = Matrix2
    
    Weight_mat = [[0 for i in range(2*n)] for j in range(2*n)]
    for i in range(n):
        Weight_mat[i][i] = sqrt(Wx[i])
        Weight_mat[i+n][i+n] = sqrt(Wx[i])

    Weight_mat = torch.tensor(Weight_mat)
    if t == 0:
        u = [target_fun(X_x[i], t) for i in range(n)] + [target_fun_complex(X_x[i], t) for i in range(n)]
        Y_real = discrete_beta(u, X_x, Wx, n, Gamma_x, beta)
        u = torch.tensor([u for i in range(interm)])
    
    Y_real_inv = discrete_beta([target_fun(X_x[i], t+dt) for i in range(n)] + [target_fun_complex(X_x[i], t+dt) for i in range(n)], X_x, Wx, n, Gamma_x, beta)
    Y_real_inv1 = copy.deepcopy(Y_real_inv)
    Y_real_inv2 = torch.tensor([Y_real_inv], dtype=torch.float64)
    # the matrix needed for computing the weak form of the spatial derivative 1i * u_xx
    Dif_mat = [[0 for i in range(2*n)] for j in range(2*n)]
    for i in range(n):
        if i - 1 > 0:
            Dif_mat[i][i-2+n] = beta**2 *sqrt((i) * (i-1)) / 2
            Dif_mat[i+n][i-2] = -beta**2 *sqrt((i) * (i-1)) / 2
    
        Dif_mat[i][i+n] = -(i+1/2) * beta**2
        Dif_mat[i+n][i] = (i+1/2) * beta**2
        if i + 2 < n:
            Dif_mat[i][i+2+n] = beta**2 *sqrt((i+1) * (i+2)) / 2
            Dif_mat[i+n][i+2] = -beta**2 *sqrt((i+1) * (i+2)) / 2
    
     
    Dif_mat = torch.tensor(Dif_mat, dtype=torch.float64)    
    time = [(t + dt * C[i]) for i in range(interm-1)]
    time.append(t + dt)
    F_inter = F(n, t, dt, time, beta)
    zeros = [0 for i in range(2*n)]
    zeros = torch.tensor([zeros for i in range(interm)], dtype = torch.float64)
    # set up the neural network model
    model = TwoLayerNet20(1, 100, 2*n)
    Y_pred = model(X)
    criterion = torch.nn.MSELoss(reduction='sum')
    optimizer = torch.optim.SGD(model.parameters(), lr=0.0002)
    optimizer.zero_grad()
    loss = criterion(zeros,torch.matmul(torch.matmul(Matrix1, Y_pred.transpose(0, 1) + dt * torch.matmul(torch.matmul(Dif_mat, Y_pred.transpose(0, 1)) - F_inter, B_matrix)).transpose(0, 1) - u, Weight_mat))
    loss.backward()
    optimizer.step()
    optimizer.zero_grad()
    # training
    while loss.item() > 1e-20 and k < 200000:
        if k % 10000 == 0:
            print(loss.item())
            
        k += 1
        Y_pred = model(X)
        loss = criterion(zeros,torch.matmul(torch.matmul(Matrix1, Y_pred.transpose(0, 1) + dt * torch.matmul(torch.matmul(Dif_mat, Y_pred.transpose(0, 1)) - F_inter, B_matrix)).transpose(0, 1) - u, Weight_mat))
        optimizer.zero_grad()
        loss.backward()
        optimizer.step()
    
    result = math.sqrt(sum((torch.matmul(torch.matmul(Matrix1, Y_real_inv2.transpose(0, 1)).transpose(0, 1)[0] - torch.matmul(Matrix1, Y_pred.transpose(0, 1)).transpose(0, 1)[interm-1], Weight_mat))**2))
    error.append(result)
    move_list.append(move)
    beta_list.append(beta)
    freq_list.append(freq_ind)
    exter_list.append(exter_ind)
    n_list.append(n)
    print(beta, freq_ind, result, t, n, move)
    k = 0
    t += dt
    

testing = pd.DataFrame(data = error)
testing.to_csv('error_spectral_schrodinger_adaptive1.csv', header=False, index = False)
testing = pd.DataFrame(data = move_list)
testing.to_csv('xl_spectral_schrodinger_adaptive1.csv', header=False, index = False)
testing = pd.DataFrame(data = beta_list)
testing.to_csv('beta_spectral_schrodinger_adaptive1.csv', header=False, index = False)
testing = pd.DataFrame(data = freq_list)
testing.to_csv('freq_spectral_schrodinger_adaptive1.csv', header=False, index = False)
testing = pd.DataFrame(data = exter_list)
testing.to_csv('exter_spectral_schrodinger_adaptive1.csv', header=False, index = False)
testing = pd.DataFrame(data = n_list)
testing.to_csv('N_spectral_schrodinger_adaptive1.csv', header=False, index = False)
