# -*- coding: utf-8 -*-
"""
Created on Sun Oct 10 11:36:15 2021

@author: 15000
"""

# -*- coding: utf-8 -*-
"""
Created on Thu Oct  7 01:38:51 2021

@author: 15000
"""
import torch
from math import sqrt, pi
from scipy import exp
import pdb

# the functions for calculating a complex-valued function's exterior indicator
def exterior_indicator_complex(N_inv, X_node, W_x, n, scale, Gamma_x, s, key=1):
    # the norm \|\partial_x U\| in the semi-unbounded domain [x_R, \infty] is calculated
    # by using the Laguerre collocation points and weights
    def Init_Laguerre(N, alpha, b):
        def factorial(n):
            if n == 0:
                return 1
            elif n == 1:
                return 1
            else:
                return n *factorial(n-1)
    
        def Laguerre(x, alpha, n, b):
            x = x * b
            if n == 0:
                y = 1
                y *= exp(-x/2)
            elif n == 1:
                y = -x + alpha + 1
                y *= exp(-x/2)
            else:
                y0 = 1
                y1 = -x + alpha + 1
                for i in range(1, n):
                    y2 = ((2*i + alpha + 1 - x) * y1 - (i + alpha) * y0) / (i+1)
                    y0 = y1
                    y1 = y2
                
                y = y1 * exp(-x/2)
            
            return y
            
        An = [[0. for i in range(N)] for j in range(N)]
        An = torch.tensor(An)
        Wx = [0 for i in range(N)]
        Gamma_x = [0 for i in range(N)]
        X = [0 for i in range(N)]
        for i in range(N):
            if i == 0:
                An[i, i] = 2*(i) + 1 + alpha
                An[i, i+1] = -sqrt((i+1)*(i+alpha))
            elif i == N-1:
                An[i, i] = 2*(i) + 1 + alpha
                An[i, i-1] = -sqrt((i)*((i)+alpha))
            else:
                An[i, i] = 2*(i) + 1 + alpha
                An[i, i-1] = -sqrt((i)*((i)+alpha))
                An[i, i+1] = -sqrt((i+1)*(i+1+alpha))
        
        evals, evecs = torch.eig(An, eigenvectors=True)
        for i in range(N):
            X[i] = float(evals[i][0])
        
        X.sort()
        
        for i in range(N):
            Wx[i] = 1/N**2 * X[i] / (Laguerre(X[i], alpha, N-1, 1))**2
        
        for i in range(N):
            Gamma_x[i] = 1 / (i+1)
    
        X = [1/b * X[i] for i in range(N)]
        Wx = [Wx[i] / b**(alpha+1) for i in range(N)]
        Gamma_x = [Gamma_x[i] / b**(alpha+1) for i in range(N)]
        return X, Wx, Gamma_x

    def inv_trans(N_inv, X_node, alpha, n, b, m):
        N_next = [0 for i in range(2*n)]
        X_val = [[0 for i in range(m)] for j in range(n)]
        for i in range(n):
            for j in range(m):
                X_val[i][j] = Hermite(X_node[i], j, b)
        
        for i in range(n):
            for j in range(m):
                N_next[i] += X_val[i][j] * N_inv[j]
                N_next[i+n] += X_val[i][j] * N_inv[j+m]
        
        return N_next
    
    def Hermite(x, n, beta):
        x = x * beta
        if n == 0:
            y = pi**(-1/4) * exp(-x**2/2)
        elif n == 1:
            y = sqrt(2) * pi**(-1/4) * x * exp(-x**2/2)
        else:
            y0 = pi**(-1/4) * exp(-x**2/2)
            y1 = sqrt(2) * pi**(-1/4) * x * exp(-x**2/2)
            for i in range(n-1):
                y2 = x * sqrt(2/(i+2)) * y1 - sqrt((i+1)/(i+2))*y0
                y0 = y1
                y1 = y2
            
            y = y2
        y = y * sqrt(beta)
        return y
    
    # the function for calculating U_x given the expansion coefficients N_inv
    # for the Laguerre functions
    def inv_transp_value(N_inv, X_node, alpha, m, b, n):
        V = [0 for i in range(2*(n+1))]
        for i in range(n+1):
            if i == 0:
                V[i] = sqrt((i+1)/2) * N_inv[i+1]
                V[i+n+1] = sqrt((i+1)/2) * N_inv[i+1+n]
            elif i == n:
                V[i] = -sqrt(n/2) * N_inv[i-1]
                V[i+n+1] = -sqrt(n/2) * N_inv[i-1+n]
            elif i == n-1:
                V[i] = -sqrt((i)/2) * N_inv[i-1]
                V[i+n+1] = -sqrt((i)/2) * N_inv[i-1+n]
            else:
                V[i] = sqrt((i+1)/2) * N_inv[i+1] - sqrt(i/2) * N_inv[i-1]
                V[i+n+1] = sqrt((i+1)/2) * N_inv[i+1+n] - sqrt(i/2) * N_inv[i-1+n]
        
        V = [V[i]*b for i in range(2*(n+1))]
        N_next = inv_trans(V, X_node, alpha, n, b, n+1)
        return N_next
        
    N_p = inv_transp_value(N_inv, X_node, 0, n, scale, n)
    summ = 0
    for i in range(n):
        summ += (abs(N_p[i])**2 + abs(N_p[i])**2) * W_x[i]
    
    [X_L, Wx_L, Gamma_x_L] = Init_Laguerre(n, 0, 2)
    summ = sqrt(summ)
    y = X_node[(2*n+2) // 3]
    
    # calculate the exterior-error indicator of a complex-valued function
    def Tailer(N_inv, y):
        nonlocal X_L, scale, n
        Tail_U = inv_transp_value(N_inv, [X_L[i]+y for i in range(len(X_L))], 0, n, scale, n)
        Tail = 0
        for j in range(n):
            Tail += (abs(Tail_U[j])**2 + abs(Tail_U[j+n])**2) * Wx_L[j]
        
        return Tail
        
    Tail = sqrt(Tailer(N_inv, y)) / summ
    
    # calculate the displacement d_0
    l = 0
    while Tail > s:
        if key == 0:
            break
        
        y += 0.004
        l += 1
        if l >= 25:
            break
        
        Tail = sqrt(Tailer(N_inv, y)) / summ;
    
    move = y - X_node[(2*n+2) // 3]
    return [move, l, Tail]
    