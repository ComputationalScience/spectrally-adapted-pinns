function multiplot4
subplot('Position', [0.13 0.594 0.34 0.36])
t = 0.1:0.1:1;
E2 = csvread('error_spectral_schrodinger_adaptive1.csv');
E1 = csvread('error_spectral_schrodinger_nonadaptive1.csv');
%E1 = E1(1:5001);
%E2 = E2(1:5001);
%E3 = E3(1:5001);
%E4 = E4(1:5001);
h1 = semilogy(t(1:10), E1(1:10), 'color', 'b', 'Linewidth', 1.5 );
hold on;
h2 = semilogy(t(1:10), E2(1:10), 'color', 'r', 'Linewidth', 1.5 );
hold on;
semilogy(t(1:3:10), E1(1:3:10), '*', 'color', 'k', 'Linewidth', 1.5);
hold on;
semilogy(t(1:3:10), E2(1:3:10), 's', 'color', 'r', 'Linewidth', 1.5 );
hold on;

set(gca, 'fontsize', 15)
xlabel('time','fontsize', 20, 'interp', 'latex');
ylabel('$L^2$ error','fontsize', 20, 'interp', 'latex')
[H, h] = legend([h1, h2], '~non-adaptive', '~adaptive', 'interp', 'latex');
H.Box = 'off';
%set(h(6), 'marker', '*')
set(h(4), 'marker', '*')
set(h(6), 'marker', 's')
set(h(1:2), 'fontsize', 20, 'interp', 'latex')
%set(H, 'Position', [.08, .64, .1, .2])
axis([0 1 1e-10 1])
set(gca, 'xtick', [0 0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9 1])
set(gca, 'xticklabel', {0 ,'', 0.2,'', 0.4,'', 0.6,'', 0.8,'', 1})
set(gca, 'ytick', [1e-12 1e-11 1e-10, 1e-9, 1e-8, 1e-7, 1e-6, 1e-5, 1e-4, 1e-3, 1e-2, 1e-1, 1])
set(gca, 'yticklabel', {'', '', '10^{-10}','', '',  '','', '10^{-5}', '', '', '', '', '1'})
set(gca,'yminortick','off') 
title('(a) error ', 'fontsize', 20, 'interp', 'latex');
set(H, 'Position', [.17, .82, .1, .15])
%disp(E1)

subplot('Position', [0.615 0.594 0.34 0.36])
t = 0.1:0.1:1;
E1 = csvread('beta_spectral_schrodinger_adaptive.csv');
h1 = plot(t(1:10), E1(1:10), 'color', [0, 0.5, 0.5], 'Linewidth', 1.5 );
hold on;
%plot(t(1:3:10), E1(1:3:10), '*', 'color', [0, 0.5, 0.5], 'Linewidth', 1.5);
%hold on;
set(gca, 'fontsize', 15)
xlabel('time','fontsize', 20, 'interp', 'latex');
ylabel('$\beta$','fontsize', 20, 'interp', 'latex')
%[H, h] = legend([h1], '~$\beta$', 'interp', 'latex');
%H.Box = 'off';
%set(h(3), 'marker', '*')
%set(h(1), 'fontsize', 20, 'interp', 'latex')
%set(gca, 'xtick', [ 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0])
%set(gca, 'xticklabel', {'', '',  '','', '0.5', '', '',  '','', '1.0'})
set(gca, 'xtick', [0 0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9 1])
set(gca, 'xticklabel', {0 ,'', 0.2,'', 0.4,'', 0.6,'', 0.8,'', 1})
set(gca, 'ytick', [  0.6, 0.65, 0.7, 0.75, 0.8, 0.85])
set(gca, 'yticklabel', {0.6, '', 0.7, '', 0.8, ''})
set(gca,'yminortick','off')
axis([0 1 0.6, 0.85])
%set(H, 'Position', [.55, .71, .1, .12])
title('(b) scaling factor', 'fontsize', 20, 'interp', 'latex');


subplot('Position', [0.13 0.077 0.34 0.36])
t = 0.1:0.1:1;
E1 = csvread('xl_spectral_schrodinger_adaptive.csv');
h1 = plot(t(1:10), E1(1:10), 'color', [0.5, 0, 0.5], 'Linewidth', 1.5 );
hold on;
%plot(t(1:3:10), E1(1:3:10), '+', 'color', [0.5, 0, 0.5], 'Linewidth', 1.5);
%hold on;
set(gca, 'fontsize', 15)
xlabel('time','fontsize', 20, 'interp', 'latex');
ylabel('$x_L$','fontsize', 20, 'interp', 'latex')
%[H, h] = legend([h1], '~$x_L$', 'interp', 'latex');
%H.Box = 'off';
%set(h(3), 'marker', '+')
%set(h(1), 'fontsize', 20, 'interp', 'latex')
%set(gca, 'xtick', [ 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0])
%set(gca, 'xticklabel', {'', '',  '','', '0.5', '', '',  '','', '1.0'})
set(gca, 'xtick', [0 0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9 1])
set(gca, 'xticklabel', {0 ,'', 0.2,'', 0.4,'', 0.6,'', 0.8,'', 1})
set(gca, 'ytick', [0, 0.1, 0.2, 0.3, 0.4, 0.5])
set(gca, 'yticklabel', {'0','', '0.2',  '','0.4', '','0.6'})
set(gca,'yminortick','off')
axis([0 1 0 0.6])
%set(H, 'Position', [.25, .31, .1, .12])
title('(c) displacement', 'fontsize', 20, 'interp', 'latex');

subplot('Position', [0.615 0.077 0.34 0.36])
t = 0.1:0.1:2;
E1 = csvread('N_spectral_schrodinger_adaptive.csv');
E1 = E1 - 1;
h1 = plot(t(1:10), E1(1:10), 'color', 'b', 'Linewidth', 1.5 );
hold on;
%plot(t(1:3:10), E1(1:3:10), '*', 'color', 'b', 'Linewidth', 1.5);
%hold on;
set(gca, 'fontsize', 15)
xlabel('time','fontsize', 20, 'interp', 'latex');
ylabel('$N$','fontsize', 20, 'interp', 'latex')
%[H, h] = legend([h1], '~$N$', 'interp', 'latex');
%H.Box = 'off';
%set(h(3), 'marker', '*')
%set(h(1), 'fontsize', 20, 'interp', 'latex')
set(gca, 'xtick', [0 0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9 1])
set(gca, 'xticklabel', {0 ,'', 0.2,'', 0.4,'', 0.6,'', 0.8,'', 1})
set(gca, 'ytick', [20, 25, 30, 35, 40, 45])
set(gca, 'yticklabel', {20, '',30, '', 40, ''})
set(gca,'yminortick','off')
axis([0 1 20 45])
%set(H, 'Position', [.75, .1, .1, .12])
title('(d) expansion order', 'fontsize', 20, 'interp', 'latex');
end