# -*- coding: utf-8 -*-
"""
Created on Fri Oct 29 02:15:09 2021

@author: 15000
"""


"""
Created on Sun Sep  5 08:48:05 2021

@author: 15000
"""

# -*- coding: utf-8 -*-
"""
Created on Thu Sep  2 01:26:17 2021

@author: 15000
"""

# -*- coding: utf-8 -*-
"""
Created on Wed Sep  1 13:55:34 2021

@author: 15000
"""

# -*- coding: utf-8 -*-
"""
Created on Wed Sep  1 05:50:31 2021

@author: 15000
"""

from Network_Model import TwoLayerNet20
from math import sin, sqrt, acos, cos, pi
from scipy import exp
import random
import numpy as np
import torch
from torch.autograd import Variable
import pdb
import csv
import pandas as pd
import copy

xi = 0.3
k = 1
beta = 0.4
beta_y = 0.5
m = 200
n = 9
ny = 9
torch.set_default_tensor_type(torch.DoubleTensor)

C_record = csv.reader(open('C.csv'))
B_record = csv.reader(open('B.csv'))
A_record = csv.reader(open('A.csv'))
C = []
B = []

for i in C_record:
    C.append(float(i[0]))

for i in B_record:
    B.append(float(i[0]))

A = [[] for i in range(len(C)+1)]
interm = len(C) + 1
ss = 0
for i in A_record:
    for j in i:
        #print(A[ss], ss)
        #print(j, A[i])
        A[ss].append(float(j))
    ss += 1

#print(A, B, C)
#pdb.set_trace()

def Init(n, beta):
    A = [[0 for i in range(n)] for j in range(n)]
    Wx = [0 for i in range(n)]
    for i in range(n):
        if i == 0:
            A[i][i] = 0
            A[i][i+1] = sqrt((1+i)/2)
        elif i == n-1:
            A[i][i] = 0
            A[i][i-1] = sqrt(i/2)
        else:
            A[i][i] = 0
            A[i][i-1] = sqrt(i/2)
            A[i][i+1] = sqrt((i+1) / 2)
    
    A = torch.tensor(A, dtype=torch.float64)
    #print(A)
    #pdb.set_trace()
    #A = A.to(torch.float64)
    (evals, evecs) = torch.eig(A, eigenvectors=True)
    X = [0 for i in range(n)]
    for i in range(n):
        X[i] = float(evals[i][0])
    
    X.sort()
    for i in range(n):
        Wx[i] = 1 / n / Hermite(X[i], n-1, 1)**2
    
    Gamma_x = [1 for i in range(n)]
    Wx = [Wx[i] / beta for i in range(n)]
    X = [X[i] / beta for i in range(n)]
    
    return X, Wx, Gamma_x


def discrete_beta(u, X, Wx, n, Gamma_x, Y, Wy, ny, Gamma_y, beta, betay):
    U = [0 for i in range(n*ny)]
    X_val = [[0 for i in range(n)] for j in range(n)]
    Y_val = [[0 for i in range(ny)] for j in range(ny)]
    for i in range(n):
        for j in range(n):
            X_val[i][j] = Hermite(X[i], j, beta)
    
    for i in range(ny):
        for j in range(ny):
            Y_val[i][j] = Hermite(Y[i], j, betay)
    
    for i in range(n):
        for j in range(n):
            for i2 in range(ny):
                for j2 in range(ny):        
                    U[i2*n + i] += u[j2*n+j] * Wx[j] * X_val[j][i] / Gamma_x[i] * Wy[j2] * Y_val[j2][i2] / Gamma_y[i2]
    
    return U



def Hermite(x, n, beta):
    x = x * beta
    if n == 0:
        y = pi**(-1/4) * exp(-x**2/2)
    elif n == 1:
        y = sqrt(2) * pi**(-1/4) * x * exp(-x**2/2)
    else:
        y0 = pi**(-1/4) * exp(-x**2/2)
        y1 = sqrt(2) * pi**(-1/4) * x * exp(-x**2/2)
        for i in range(n-1):
            y2 = x * sqrt(2/(i+2)) * y1 - sqrt((i+1)/(i+2))*y0
            y0 = y1
            y1 = y2
        y = y2
    
    y = y * sqrt(beta)
    return y
    
    
def target_fun(x, y, t=0):
    global xi, k
    result = 1 / sqrt(t+3) * 1 / sqrt(t+2) *  exp(-x**2/4/(t+3)) * exp(-y**2/4/(t+2))
    return result


X_x, Wx, Gamma_x = Init(n, beta)
Y_x, Wy, Gamma_y = Init(ny, beta_y)

Weight_mat = [[0 for i in range(n*ny)] for j in range(n*ny)]
for i in range(n):
    for j in range(ny):    
        Weight_mat[i+j*n][i+j*n] = sqrt(Wx[i]) * sqrt(Wy[j])

Weight_mat = torch.tensor(Weight_mat)


def prediction2(X, y_pred):
    samples = []
    x = X
    for j in range(len(x)):
        samples.append(inverse(x[j], y_pred))
    

    return torch.tensor(samples)

def inverse(x, y, n, ny, beta, beta_y, u_inv):
    #n = len(y)
    result = 0
    for i in range(n):
        for j in range(ny):
            result += Hermite(x, i, beta) * Hermite(y, j, beta_y) * u_inv[i+j*n]
    
    return result


def f(x, y, t, nx, ny):
    y = [0 for i in range(nx*ny)]
    for i in range(n):
        for j in range(ny):    
            y[i+j*nx] = 0#(x[i] * cos(x[i]) + (t+1) *sin(x[i])) * (t+1)**(-3/2) * exp(-x[i]**2/4/(t+1))
    
    return y

def F(n, ny, t, dt, time, beta, betay):
    global X_x, Wx, Gamma_x, Y_x, Wy, Gamma_y
    result = []
    for i in range(len(time)):
        y = f(X_x, Y_x, time[i], n, ny)
        y = discrete_beta(y, X_x, Wx, n, Gamma_x, Y_x, Wy, ny, Gamma_y, beta, betay)
        #y = discrete_beta(y, X_x, Wx, n, Gamma_x, beta)
        result.append(y)
    
    result = torch.tensor(result, dtype=torch.float64)
    result = result.transpose(0, 1)
    return result


B_matrix = torch.tensor(A, dtype=torch.float64)
B_matrix = B_matrix.transpose(0, 1)
Dif_mat = [[0 for i in range(n*ny)] for j in range(n*ny)]


for i in range(n):
    for j in range(ny):
        if i - 1 > 0:
            Dif_mat[j*n+i][j*n+i-2] = Dif_mat[j*n+i][j*n+i-2]-beta**2 *sqrt((i) * (i-1)) / 2
    
        Dif_mat[j*n+i][j*n+i] = Dif_mat[j*n+i][j*n+i]+(i+1/2) * beta**2
        if i + 2 < n:
            Dif_mat[j*n+i][j*n+i+2] = Dif_mat[j*n+i][j*n+i+2]-beta**2 *sqrt((i+1) * (i+2)) / 2
    
for i in range(n):
    for j in range(ny):
        if j - 1 > 0:
            Dif_mat[j*n+i][(j-2)*n+i] = Dif_mat[j*n+i][(j-2)*n+i]-beta_y**2 *sqrt((j) * (j-1)) / 2
    
        Dif_mat[j*n+i][j*n+i] = Dif_mat[j*n+i][j*n+i]+(j+1/2) * beta_y**2
        if j + 2 < n:
            Dif_mat[j*n+i][(j+2)*n+i] = Dif_mat[j*n+i][(j+2)*n+i]-beta_y**2 *sqrt((j+1) * (j+2)) / 2
           
        
Dif_mat = torch.tensor(Dif_mat, dtype=torch.float64)
#Matrix = Dif_mat
X_record = csv.reader(open('X.csv'))
Y_record = csv.reader(open('Y.csv'))
X = []
Y = []
for i in X_record:
    X.append(float(i[0]))

for i in Y_record:
    Y.append(float(i[0]))

t = 0
dt = 0.1
time = [(t + dt * C[i]) for i in range(interm-1)]
time.append(t + dt)

#X_ori = X
X = torch.tensor([[time[i]] for i in range(interm)], dtype=torch.float64)
#X = X.transpose(0, 1)

k = 1

Matrix = [[0 for i in range(n*ny)] for j in range(n*ny)]
for i in range(n):
    for j in range(n):
        for i2 in range(ny):
            for j2 in range(ny):
                Matrix[i2*n+i][j2*n+j] = Hermite(X_x[i], j, beta) * Hermite(Y_x[i2], j2, beta_y)

Matrix1 = torch.tensor(Matrix, dtype=torch.float64)

error = []

zeros = [0 for i in range(n*ny)]
zeros = torch.tensor([zeros for i in range(interm)], dtype = torch.float64)
t_target = 2

def frequency_indicator_x(u_inv, n, Gamma_x, ny, Gamma_y):
    total = 0
    residual = 0
    for i in range(n):
        for j in range(ny):
            total += u_inv[i+j*n]**2 * Gamma_x[i]
            if i > (2*n+2) // 3:
                residual += u_inv[i+j*n]**2 * Gamma_x[i]
    
    return sqrt(residual / total)

def frequency_indicator_y(u_inv, n, Gamma_x, ny, Gamma_y):
    total = 0
    residual = 0
    for i in range(n):
        for j in range(ny):
            total += u_inv[i+j*n]**2 * Gamma_x[i]
            if j > (2*ny+2) // 3:
                residual += u_inv[i+j*n]**2 * Gamma_x[i]
    
    return sqrt(residual / total)

u = [0 for i in range(n*ny)]
for i in range(n):
    for j in range(ny):        
        u[j*n+i] = target_fun(X_x[i], Y_x[j], t)

Y_real = u
Weight_mat = [[0 for i in range(n*ny)] for j in range(n*ny)]
for i in range(n):
    for j in range(ny):    
        Weight_mat[i+j*n][i+j*n] = sqrt(Wx[i]) * sqrt(Wy[j])

Weight_mat = torch.tensor(Weight_mat)
u = torch.tensor([u for i in range(interm)])
freq_ind_x = None
freq_ind_y = None
q = 1
nu = 1 / q
beta_under = 0.2
beta_over = 5
error = []
freq_x_list = []
freq_y_list = []
beta_x_list = []
beta_y_list = []
while t < t_target:
    if freq_ind_x is None:
        Y_real_inv = discrete_beta(Y_real, X_x, Wx, n, Gamma_x, Y_x, Wy, ny, Gamma_y, beta, beta_y)
        freq_ind_x = frequency_indicator_x(Y_real_inv, n, Gamma_x, ny, Gamma_y)
        freq_ind_y = frequency_indicator_y(Y_real_inv, n, Gamma_x, ny, Gamma_y)
        lst = [0 for i2 in range(n*ny)]
        for i2 in range(n):
            for j2 in range(ny):    
                lst[i2 + j2*n] = (inverse(X_x[i2], Y_x[j2], n, ny, beta, beta_y, Y_real_inv))
                
        #print(torch.tensor(Y_real))
        #print(Y_real_inv)
        #pdb.set_trace()
        freq_thres_x = freq_ind_x
        freq_thres_y = freq_ind_y
    else:
        u = []
        lst = [0 for i2 in range(n*ny)]
        coeffs = [float(Y_pred[interm-1, i2]) for i2 in range(n*ny)]
        for i2 in range(n):
            for j2 in range(ny):    
                lst[i2 + j2*n] = (inverse(X_x[i2], Y_x[j2], n, ny, beta, beta_y, coeffs))
        
        coeffs_ori = copy.deepcopy(coeffs)
        for i1 in range(interm):
            u.append(copy.deepcopy(lst))
            
        u = torch.tensor(u)
        
        Y_real_inv = [float(Y_pred[interm-1, i2]) for i2 in range(n*ny)]
        u_inv = copy.deepcopy(Y_real_inv)
        u_inv_ori = copy.deepcopy(u_inv)
        X_x_ori = copy.deepcopy(X_x)
        Wx_ori = copy.deepcopy(Wx)
        Gamma_x_ori = copy.deepcopy(Gamma_x)
        beta_ori = beta
        beta_y_ori = beta_y
        freq_ind_x = frequency_indicator_x(Y_real_inv, n, Gamma_x, ny, Gamma_y)
        #print(freq_ind_x)
        if freq_ind_x > nu *freq_thres_x:
            beta_hat = beta *q
            X_x1, Wx1, Gamma_x1 = Init(n, beta_hat)
            lst = [0 for i2 in range(n*ny)]
            for i2 in range(n):
                for j2 in range(ny):    
                    lst[i2 + j2*n] = (inverse(X_x1[i2], Y_x[j2], n, ny, beta, beta_y, copy.deepcopy(coeffs_ori)))
                                                                                                
                    
            #lst = [inverse(X_x1[i2], Y_real_inv, beta) for i2 in range(n)]
            u_inv_new = discrete_beta(lst, X_x1, Wx1, n, Gamma_x1, Y_x, Wy, ny, Gamma_y, beta_hat, beta_y)
            fred_ind_xnew = frequency_indicator_x(u_inv_new, n, Gamma_x1, ny, Gamma_y)
            #print(fred_ind_xnew, freq_ind_x)
            #pdb.set_trace()
            while fred_ind_xnew < freq_ind_x and beta_hat > beta_under:
                if q == 1:
                    break
                
                freq_thres_x = fred_ind_xnew
                freq_ind_ori = freq_ind_x
                freq_ind_x = fred_ind_xnew
              

                U_new = lst#######star [inverse(X_x1[i2], u_inv_new, beta_hat) for i2 in range(n)]
                u = torch.tensor([U_new for i in range(interm)])
                beta = beta_hat
                #if beta < 0.45:
                #    print(freq_ind_ori, fred_ind_xnew)
                #    pdb.set_trace()
                X_x = X_x1
                Wx = Wx1
                Gamma_x = Gamma_x1
                u_inv = u_inv_new
                beta_hat *= q
                X_x1, Wx1, Gamma_x1 = Init(n, beta_hat)
                lst = [0 for i2 in range(n*ny)]
                for i2 in range(n):
                    for j2 in range(ny):    
                        lst[i2 + j2*n] = (inverse(X_x1[i2], Y_x[j2], n, ny, beta, beta_y, u_inv))
                #lst = [inverse(X_x1[i2], u_inv_new, beta) for i2 in range(n)]
                u_inv_new = discrete_beta(Y_real, X_x1, Wx1, n, Gamma_x1, Y_x, Wy, ny, Gamma_y, beta_hat, beta_y)#discrete_beta(lst, X_x1, Wx1, n, Gamma_x1, beta_hat)
                fred_ind_xnew = frequency_indicator_x(u_inv_new, n, Gamma_x1, ny, Gamma_y)#frequency_indicator(u_inv_new, n, Gamma_x1)
        elif freq_ind_x < freq_thres_x:
            beta_hat = beta /q
            X_x1, Wx1, Gamma_x1 = Init(n, beta_hat)
            lst = [0 for i2 in range(n*ny)]
            for i2 in range(n):
                for j2 in range(ny):    
                    lst[i2 + j2*n] = (inverse(X_x1[i2], Y_x[j2], n, ny, beta, beta_y, copy.deepcopy(coeffs_ori)))
                    
            #lst = [inverse(X_x1[i2], Y_real_inv, beta) for i2 in range(n)]
            u_inv_new = discrete_beta(lst, X_x1, Wx1, n, Gamma_x1, Y_x, Wy, ny, Gamma_y, beta_hat, beta_y)
            fred_ind_xnew = frequency_indicator_x(u_inv_new, n, Gamma_x1, ny, Gamma_y)
            while fred_ind_xnew < freq_ind_x and beta_hat > beta_under:
                if q == 1:
                    break
                
                freq_thres_x = fred_ind_xnew
                freq_ind_x = fred_ind_xnew
                U_new = lst#######star [inverse(X_x1[i2], u_inv_new, beta_hat) for i2 in range(n)]
                u = torch.tensor([U_new for i in range(interm)])
                beta = beta_hat
                X_x = X_x1
                Wx = Wx1
                Gamma_x = Gamma_x1
                u_inv = u_inv_new
                beta_hat /= q
                X_x1, Wx1, Gamma_x1 = Init(n, beta_hat)
                lst = [0 for i2 in range(n*ny)]
                for i2 in range(n):
                    for j2 in range(ny):    
                        lst[i2 + j2*n] = (inverse(X_x1[i2], Y_x[j2], n, ny, beta, beta_y, u_inv))
                #lst = [inverse(X_x1[i2], u_inv_new, beta) for i2 in range(n)]
                u_inv_new = discrete_beta(lst, X_x1, Wx1, n, Gamma_x1, Y_x, Wy, ny, Gamma_y, beta_hat, beta_y)#discrete_beta(lst, X_x1, Wx1, n, Gamma_x1, beta_hat)
                fred_ind_xnew = frequency_indicator_x(u_inv_new, n, Gamma_x1, ny, Gamma_y)#frequency_indicator(u_inv_new, n, Gamma_x1)
        
        freq_ind_y = frequency_indicator_y(u_inv_ori, n, Gamma_x_ori, ny, Gamma_y)
        #print(freq_ind_y)
        if freq_ind_y > nu *freq_thres_y:
            beta_y_hat = beta_y *q
            Y_x1, Wy1, Gamma_y1 = Init(ny, beta_y_hat)
            lst = [0 for i2 in range(n*ny)]
            for i2 in range(n):
                for j2 in range(ny):    
                    lst[i2 + j2*n] = (inverse(X_x_ori[i2], Y_x1[j2], n, ny, beta_ori, beta_y, copy.deepcopy(coeffs_ori)))
                    
            #lst = [inverse(X_x1[i2], Y_real_inv, beta) for i2 in range(n)]
            u_inv_new = discrete_beta(lst, X_x_ori, Wx_ori, n, Gamma_x_ori, Y_x1, Wy1, ny, Gamma_y1, beta_ori, beta_y_hat)
            fred_ind_ynew = frequency_indicator_y(u_inv_new, n, Gamma_x_ori, ny, Gamma_y1)
            while fred_ind_ynew < freq_ind_y and beta_y_hat > beta_under:
                if q == 1:
                    break
                
                freq_thres_y = fred_ind_ynew
                freq_ind_y = fred_ind_ynew
                U_new = lst#######star [inverse(X_x1[i2], u_inv_new, beta_hat) for i2 in range(n)]
                u = torch.tensor([U_new for i in range(interm)])
                beta_y = beta_y_hat
                Y_x = Y_x1
                Wy = Wy1
                Gamma_y = Gamma_y1
                u_inv = u_inv_new
                beta_y_hat *= q
                Y_x1, Wy1, Gamma_y1 = Init(ny, beta_y_hat)
                lst = [0 for i2 in range(n*ny)]
                for i2 in range(n):
                    for j2 in range(ny):    
                        lst[i2 + j2*n] = (inverse(X_x_ori[i2], Y_x1[j2], n, ny, beta_ori, beta_y, u_inv))
                #lst = [inverse(X_x1[i2], u_inv_new, beta) for i2 in range(n)]
                u_inv_new = discrete_beta(lst, X_x_ori, Wx_ori, n, Gamma_x_ori, Y_x1, Wy1, ny, Gamma_y1, beta_ori, beta_y_hat)#discrete_beta(lst, X_x1, Wx1, n, Gamma_x1, beta_hat)
                fred_ind_ynew = frequency_indicator_y(u_inv_new, n, Gamma_x_ori, ny, Gamma_y1)#frequency_indicator(u_inv_new, n, Gamma_x1)
        elif freq_ind_y < freq_thres_y:
            beta_y_hat = beta_y /q
            Y_x1, Wy1, Gamma_y1 = Init(ny, beta_y_hat)
            lst = [0 for i2 in range(n*ny)]
            for i2 in range(n):
                for j2 in range(ny):    
                    lst[i2 + j2*n] = (inverse(X_x_ori[i2], Y_x1[j2], n, ny, beta_ori, beta_y, copy.deepcopy(coeffs_ori)))
                    
            #lst = [inverse(X_x1[i2], Y_real_inv, beta) for i2 in range(n)]
            u_inv_new = discrete_beta(lst, X_x_ori, Wx_ori, n, Gamma_x_ori, Y_x1, Wy1, ny, Gamma_y1, beta_ori, beta_y_hat)
            fred_ind_ynew = frequency_indicator_y(u_inv_new, n, Gamma_x_ori, ny, Gamma_y1)
            while fred_ind_ynew < freq_ind_y and beta_y_hat > beta_under:
                if q == 1:
                    break
                
                freq_thres_y = fred_ind_ynew
                freq_ind_y = fred_ind_ynew
                U_new = lst#######star [inverse(X_x1[i2], u_inv_new, beta_hat) for i2 in range(n)]
                u = torch.tensor([U_new for i in range(interm)])
                beta_y = beta_y_hat
                Y_x = Y_x1
                Wy = Wy1
                Gamma_y = Gamma_y1
                u_inv = u_inv_new
                beta_y_hat /= q
                Y_x1, Wy1, Gamma_y1 = Init(ny, beta_y_hat)
                lst = [0 for i2 in range(n*ny)]
                for i2 in range(n):
                    for j2 in range(ny):    
                        lst[i2 + j2*n] = (inverse(X_x[i2], Y_x1[j2], n, ny, beta_ori, beta_y, u_inv))
                #lst = [inverse(X_x1[i2], u_inv_new, beta) for i2 in range(n)]
                u_inv_new = discrete_beta(lst, X_x_ori, Wx_ori, n, Gamma_x_ori, Y_x1, Wy1, ny, Gamma_y1, beta_ori, beta_y_hat)#discrete_beta(lst, X_x1, Wx1, n, Gamma_x1, beta_hat)
                fred_ind_ynew = frequency_indicator_y(u_inv_new, n, Gamma_x_ori, ny, Gamma_y1)#frequency_indicator(u_inv_new, n, Gamma_x1)
                
            
                
                
                
        
    print(beta, beta_y, freq_ind_x)
    X_x, Wx, Gamma_x = Init(n, beta)
    Y_x, Wy, Gamma_y = Init(ny, beta_y)
    if t == 0:
        u = [0 for i in range(n*ny)]
        for i in range(n):
            for j in range(ny):
                u[j*n+i] = target_fun(X_x[i], Y_x[j], t)
        #u = [target_fun(X_x[i], t) for i in range(n)]
        #Y_real = discrete_beta(u, X_x, Wx, n, Gamma_x, beta)
        u = torch.tensor([u for i in range(interm)])
    else:
        
        lst = [0 for i2 in range(n*ny)]
        for i2 in range(n):
            for j2 in range(ny):    
                lst[i2 + j2*n] = (inverse(X_x[i2], Y_x[j2], n, ny, beta_ori, beta_y_ori, coeffs_ori)) 
    
        u_inv = discrete_beta(lst, X_x, Wx, n, Gamma_x, Y_x, Wy, ny, Gamma_y, beta, beta_y)
        u = torch.tensor([lst for i in range(interm)])
   
    Matrix = [[0 for i in range(n*ny)] for j in range(n*ny)]
    for i in range(n):
        for j in range(n):
            for i2 in range(ny):
                for j2 in range(ny):
                    Matrix[i2*n+i][j2*n+j] = Hermite(X_x[i], j, beta) * Hermite(Y_x[i2], j2, beta_y)

    Matrix1 = torch.tensor(Matrix, dtype=torch.float64)
    
    #Matrix1 = [[Hermite(X_x[j], i, beta) for j in range(n)] for i in range(n)]
    #Matrix1 = torch.tensor(Matrix1, dtype=torch.float64)
    #Matrix1 = Matrix1.transpose(0, 1)
    Weight_mat = [[0 for i in range(n*ny)] for j in range(n*ny)]
    for i in range(n):
        for j in range(ny):    
            Weight_mat[i+j*n][i+j*n] = sqrt(Wx[i]) * sqrt(Wy[j])

    Weight_mat = torch.tensor(Weight_mat)
        
    
    
    Y_real = [0 for i in range(n*ny)]
    for i in range(n):
        for j in range(ny):
            Y_real[j*n+i] = target_fun(X_x[i], Y_x[j], t+dt)
            
    Y_real_inv = discrete_beta(Y_real, X_x, Wx, n, Gamma_x, Y_x, Wy, ny, Gamma_y, beta, beta_y)#(Y_real, X_x, Wx, n, Gamma_x, beta)
    Y_real_inv1 = copy.deepcopy(Y_real_inv)
    Y_real_inv2 = torch.tensor([Y_real_inv], dtype=torch.float64)
    #if t > 0:
    #    Y_real = [0 for i in range(n*ny)]
    #    for i in range(n):
    #        for j in range(ny):
    #            Y_real[j*n+i] = target_fun(X_x[i], Y_x[j], t)
    #        
    #    Y_real_inv = discrete_beta(Y_real, X_x, Wx, n, Gamma_x, Y_x, Wy, ny, Gamma_y, beta, beta_y)#(Y_real, X_x, Wx, n, Gamma_x, beta)
    #    Y_real_inv1 = copy.deepcopy(Y_real_inv)
    #    Y_real_inv2 = torch.tensor([Y_real_inv], dtype=torch.float64)
    #    print(sqrt(sum((torch.matmul(torch.matmul(Matrix1, Y_real_inv2.transpose(0, 1)).transpose(0, 1)[0] - torch.matmul(Matrix1, torch.tensor([u_inv]).transpose(0, 1)).transpose(0, 1)[0], Weight_mat))**2)))
    #    pdb.set_trace()
        
    Dif_mat = [[0 for i in range(n*ny)] for j in range(n*ny)]


    for i in range(n):
        for j in range(ny):
            if i - 1 > 0:
                Dif_mat[j*n+i][j*n+i-2] = Dif_mat[j*n+i][j*n+i-2]-beta**2 *sqrt((i) * (i-1)) / 2
    
            Dif_mat[j*n+i][j*n+i] = Dif_mat[j*n+i][j*n+i]+(i+1/2) * beta**2
            if i + 2 < n:
                Dif_mat[j*n+i][j*n+i+2] = Dif_mat[j*n+i][j*n+i+2]-beta**2 *sqrt((i+1) * (i+2)) / 2
    
    for i in range(n):
        for j in range(ny):
            if j - 1 > 0:
                Dif_mat[j*n+i][(j-2)*n+i] = Dif_mat[j*n+i][(j-2)*n+i]-beta_y**2 *sqrt((j) * (j-1)) / 2
    
            Dif_mat[j*n+i][j*n+i] = Dif_mat[j*n+i][j*n+i]+(j+1/2) * beta_y**2
            if j + 2 < ny:
                Dif_mat[j*n+i][(j+2)*n+i] = Dif_mat[j*n+i][(j+2)*n+i]-beta_y**2 *sqrt((j+1) * (j+2)) / 2
           
        
    Dif_mat = torch.tensor(Dif_mat, dtype=torch.float64)
    
    Dif_mat2 = [[0 for i in range(n*ny)] for j in range(n*ny)]


    for i in range(n):
        for j in range(ny):
            if i - 1 >= 0:
                Dif_mat2[j*n+i][j*n+i-1] = Dif_mat2[j*n+i][j*n+i-1]-beta *sqrt((i)  / 2)
    
            if i + 1 < n:
                Dif_mat2[j*n+i][j*n+i+1] = Dif_mat2[j*n+i][j*n+i+1]+beta *sqrt((i+1)  / 2)
    
    for i in range(n):
        for j in range(ny):
            if j - 1 >= 0:
                Dif_mat2[j*n+i][(j-1)*n+i] = Dif_mat2[j*n+i][(j-1)*n+i]-beta_y *sqrt((j) / 2) 
    
        
            if j + 1 < n:
                Dif_mat2[j*n+i][(j+1)*n+i] = Dif_mat2[j*n+i][(j+1)*n+i]+beta_y *sqrt((j+1) / 2)
           
        
    Dif_mat2 = torch.tensor(Dif_mat2, dtype=torch.float64)
    
    time = [(t + dt * C[i]) for i in range(interm-1)]
    time.append(t + dt)
    F_inter = F(n, ny, t, dt, time, beta, beta_y)
    model = TwoLayerNet20(1, 150, n*ny)
    Y_pred = model(X)
    criterion = torch.nn.MSELoss(reduction='sum')
    optimizer = torch.optim.SGD(model.parameters(), lr=0.0005)
    optimizer.zero_grad()
    loss = criterion(zeros,torch.matmul(torch.matmul(Matrix1, Y_pred.transpose(0, 1) + dt * torch.matmul(torch.matmul(Dif_mat, Y_pred.transpose(0, 1)) - F_inter, B_matrix)).transpose(0, 1) - u, Weight_mat))
    loss.backward()
    #print(loss.item())
    optimizer.step()
    optimizer.zero_grad()
    while loss.item() > 1e-20 and k < 400000:
        if k % 10000 == 0:
            print(loss.item())
            
        k += 1
        Y_pred = model(X)
        loss = criterion(zeros,torch.matmul(torch.matmul(Matrix1, Y_pred.transpose(0, 1) + dt * torch.matmul(torch.matmul(Dif_mat, Y_pred.transpose(0, 1)) - F_inter, B_matrix)).transpose(0, 1) - u, Weight_mat))
        optimizer.zero_grad()
        loss.backward()
        optimizer.step()
        
    result = sqrt(sum((torch.matmul(torch.matmul(Matrix1, Y_real_inv2.transpose(0, 1)).transpose(0, 1)[0] - torch.matmul(Matrix1, Y_pred.transpose(0, 1)).transpose(0, 1)[interm-1], Weight_mat))**2))
    error.append(result)
    freq_x_list.append(freq_ind_x)
    freq_y_list.append(freq_ind_y)
    beta_x_list.append(beta)
    beta_y_list.append(beta_y)
    print(beta, beta_y, freq_ind_x, freq_ind_y, result, t)
    k = 0
    t += dt

testing = pd.DataFrame(data = error)
testing.to_csv('error_spectral_parabolic_nonadpative_2D1.csv', header=False, index = False)
testing = pd.DataFrame(data = beta_x_list)
testing.to_csv('betax_spectral_parabolic_nonadpative_2D1.csv', header=False, index = False)
testing = pd.DataFrame(data = beta_y_list)
testing.to_csv('betay_spectral_parabolic_nonadpative_2D1.csv', header=False, index = False)
testing = pd.DataFrame(data = freq_x_list)
testing.to_csv('freq_x_spectral_parabolic_nonadpative_2D1.csv', header=False, index = False)
testing = pd.DataFrame(data = freq_y_list)
testing.to_csv('freq_y_spectral_parabolic_nonadpative_2D1.csv', header=False, index = False)