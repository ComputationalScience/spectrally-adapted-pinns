# -*- coding: utf-8 -*-

# -*- coding: utf-8 -*-
"""
Created on Wed Sep  1 05:50:31 2021

@author: 15000
"""
import sys
sys.path.insert(0, '../../utilities/')

from Network_Model import TwoLayerNet20
from math import sin, sqrt, acos, cos, pi
from scipy import exp
import random
import numpy as np
import torch
import csv
import pandas as pd
import copy
from Hermite_2D import Init, discrete_beta, Hermite, inverse,frequency_indicator_x, frequency_indicator_y

xi = 0.3
k = 1
beta = 0.4
beta_y = 0.5
m = 200
n = 9
ny = 9
torch.set_default_tensor_type(torch.DoubleTensor)

# import the Lagendre collocation c_j for time integration, and the matrix of coefficients
# in a high-order RK scheme A = (a)_{ij} as well as the coefficients b_i
C_record = csv.reader(open('C.csv'))
B_record = csv.reader(open('B.csv'))
A_record = csv.reader(open('A.csv'))
C = []
B = []

for i in C_record:
    C.append(float(i[0]))

for i in B_record:
    B.append(float(i[0]))

A = [[] for i in range(len(C)+1)]
interm = len(C) + 1
ss = 0
for i in A_record:
    for j in i:
        A[ss].append(float(j))
    ss += 1

    
# the analytic solution
def target_fun(x, y, t=0):
    global xi, k
    result = 1 / sqrt(t+3) * 1 / sqrt(t+2) *  exp(-x**2/4/(t+3)) * exp(-y**2/4/(t+2))
    return result

# contruct the allocation nodes, weights, and norms in both dimensions
X_x, Wx, Gamma_x = Init(n, beta)
Y_x, Wy, Gamma_y = Init(ny, beta_y)

Weight_mat = [[0 for i in range(n*ny)] for j in range(n*ny)]
for i in range(n):
    for j in range(ny):    
        Weight_mat[i+j*n][i+j*n] = sqrt(Wx[i]) * sqrt(Wy[j])

Weight_mat = torch.tensor(Weight_mat)
# heat source, for this example it is 0
def f(x, y, t, nx, ny):
    y = [0 for i in range(nx*ny)]
    for i in range(n):
        for j in range(ny):    
            y[i+j*nx] = 0
    
    return y

# heat source, for this example it is 0
def F(n, ny, t, dt, time, beta, betay):
    global X_x, Wx, Gamma_x, Y_x, Wy, Gamma_y
    result = []
    for i in range(len(time)):
        y = f(X_x, Y_x, time[i], n, ny)
        y = discrete_beta(y, X_x, Wx, n, Gamma_x, Y_x, Wy, ny, Gamma_y, beta, betay)
        result.append(y)
    
    result = torch.tensor(result, dtype=torch.float64)
    result = result.transpose(0, 1)
    return result

B_matrix = torch.tensor(A, dtype=torch.float64)
B_matrix = B_matrix.transpose(0, 1)

# the matrix for calculating the spatial derivative u_xx in the weak form (u_x, v_x)
Dif_mat = [[0 for i in range(n*ny)] for j in range(n*ny)]
for i in range(n):
    for j in range(ny):
        if i - 1 > 0:
            Dif_mat[j*n+i][j*n+i-2] = Dif_mat[j*n+i][j*n+i-2]-beta**2 *sqrt((i) * (i-1)) / 2
    
        Dif_mat[j*n+i][j*n+i] = Dif_mat[j*n+i][j*n+i]+(i+1/2) * beta**2
        if i + 2 < n:
            Dif_mat[j*n+i][j*n+i+2] = Dif_mat[j*n+i][j*n+i+2]-beta**2 *sqrt((i+1) * (i+2)) / 2
    
for i in range(n):
    for j in range(ny):
        if j - 1 > 0:
            Dif_mat[j*n+i][(j-2)*n+i] = Dif_mat[j*n+i][(j-2)*n+i]-beta_y**2 *sqrt((j) * (j-1)) / 2
    
        Dif_mat[j*n+i][j*n+i] = Dif_mat[j*n+i][j*n+i]+(j+1/2) * beta_y**2
        if j + 2 < n:
            Dif_mat[j*n+i][(j+2)*n+i] = Dif_mat[j*n+i][(j+2)*n+i]-beta_y**2 *sqrt((j+1) * (j+2)) / 2
           
        
Dif_mat = torch.tensor(Dif_mat, dtype=torch.float64)
t = 0
dt = 0.1
# set up the time points for the R-K scheme
time = [(t + dt * C[i]) for i in range(interm-1)]
time.append(t + dt)
X = torch.tensor([[time[i]] for i in range(interm)], dtype=torch.float64)
k = 1
Matrix = [[0 for i in range(n*ny)] for j in range(n*ny)]
for i in range(n):
    for j in range(n):
        for i2 in range(ny):
            for j2 in range(ny):
                Matrix[i2*n+i][j2*n+j] = Hermite(X_x[i], j, beta) * Hermite(Y_x[i2], j2, beta_y)

Matrix1 = torch.tensor(Matrix, dtype=torch.float64)
error = []

zeros = [0 for i in range(n*ny)]
zeros = torch.tensor([zeros for i in range(interm)], dtype = torch.float64)
t_target = 2
u = [0 for i in range(n*ny)]
for i in range(n):
    for j in range(ny):        
        u[j*n+i] = target_fun(X_x[i], Y_x[j], t)

Y_real = u
Weight_mat = [[0 for i in range(n*ny)] for j in range(n*ny)]
for i in range(n):
    for j in range(ny):    
        Weight_mat[i+j*n][i+j*n] = sqrt(Wx[i]) * sqrt(Wy[j])

Weight_mat = torch.tensor(Weight_mat)
# set the parameters in the model
u = torch.tensor([u for i in range(interm)])
freq_ind_x = None
freq_ind_y = None
q = 0.95#1
nu = 1 / q
beta_under = 0.2
beta_over = 5
error = []
freq_x_list = []
freq_y_list = []
beta_x_list = []
beta_y_list = []
while t < t_target:
    # calculate the initial frequency indicators in both dimensions when t = 0
    if freq_ind_x is None:
        Y_real_inv = discrete_beta(Y_real, X_x, Wx, n, Gamma_x, Y_x, Wy, ny, Gamma_y, beta, beta_y)
        freq_ind_x = frequency_indicator_x(Y_real_inv, n, Gamma_x, ny, Gamma_y)
        freq_ind_y = frequency_indicator_y(Y_real_inv, n, Gamma_x, ny, Gamma_y)
        freq_thres_x = freq_ind_x
        freq_thres_y = freq_ind_y
    else:
        u = []
        lst = [0 for i2 in range(n*ny)]
        coeffs = [float(Y_pred[interm-1, i2]) for i2 in range(n*ny)]
        for i2 in range(n):
            for j2 in range(ny):    
                lst[i2 + j2*n] = (inverse(X_x[i2], Y_x[j2], n, ny, beta, beta_y, coeffs))
        
        coeffs_ori = copy.deepcopy(coeffs)
        for i1 in range(interm):
            u.append(copy.deepcopy(lst))
            
        u = torch.tensor(u)
        
        Y_real_inv = [float(Y_pred[interm-1, i2]) for i2 in range(n*ny)]
        u_inv = copy.deepcopy(Y_real_inv)
        u_inv_ori = copy.deepcopy(u_inv)
        X_x_ori = copy.deepcopy(X_x)
        Wx_ori = copy.deepcopy(Wx)
        Gamma_x_ori = copy.deepcopy(Gamma_x)
        beta_ori = beta
        beta_y_ori = beta_y
        # calculate the current frequency indicator in the x dimension
        freq_ind_x = frequency_indicator_x(Y_real_inv, n, Gamma_x, ny, Gamma_y)
        if freq_ind_x > nu *freq_thres_x:
            beta_hat = beta *q
            X_x1, Wx1, Gamma_x1 = Init(n, beta_hat)
            lst = [0 for i2 in range(n*ny)]
            for i2 in range(n):
                for j2 in range(ny):    
                    lst[i2 + j2*n] = (inverse(X_x1[i2], Y_x[j2], n, ny, beta, beta_y, copy.deepcopy(coeffs_ori)))
                                                                                                
            u_inv_new = discrete_beta(lst, X_x1, Wx1, n, Gamma_x1, Y_x, Wy, ny, Gamma_y, beta_hat, beta_y)
            fred_ind_xnew = frequency_indicator_x(u_inv_new, n, Gamma_x1, ny, Gamma_y)
            # determine if decreasing the scaling factor is needed in the x dimension 
            # (more diffusive)
            while fred_ind_xnew < freq_ind_x and beta_hat > beta_under:
                if q == 1:
                    break
                
                freq_thres_x = fred_ind_xnew
                freq_ind_ori = freq_ind_x
                freq_ind_x = fred_ind_xnew
                U_new = lst
                u = torch.tensor([U_new for i in range(interm)])
                beta = beta_hat
                X_x = X_x1
                Wx = Wx1
                Gamma_x = Gamma_x1
                u_inv = u_inv_new
                beta_hat *= q
                X_x1, Wx1, Gamma_x1 = Init(n, beta_hat)
                lst = [0 for i2 in range(n*ny)]
                for i2 in range(n):
                    for j2 in range(ny):    
                        lst[i2 + j2*n] = (inverse(X_x1[i2], Y_x[j2], n, ny, beta, beta_y, u_inv))
                
                u_inv_new = discrete_beta(Y_real, X_x1, Wx1, n, Gamma_x1, Y_x, Wy, ny, Gamma_y, beta_hat, beta_y)#discrete_beta(lst, X_x1, Wx1, n, Gamma_x1, beta_hat)
                fred_ind_xnew = frequency_indicator_x(u_inv_new, n, Gamma_x1, ny, Gamma_y)#frequency_indicator(u_inv_new, n, Gamma_x1)
        
        elif freq_ind_x < freq_thres_x:
            beta_hat = beta /q
            X_x1, Wx1, Gamma_x1 = Init(n, beta_hat)
            lst = [0 for i2 in range(n*ny)]
            for i2 in range(n):
                for j2 in range(ny):    
                    lst[i2 + j2*n] = (inverse(X_x1[i2], Y_x[j2], n, ny, beta, beta_y, copy.deepcopy(coeffs_ori)))

            u_inv_new = discrete_beta(lst, X_x1, Wx1, n, Gamma_x1, Y_x, Wy, ny, Gamma_y, beta_hat, beta_y)
            fred_ind_xnew = frequency_indicator_x(u_inv_new, n, Gamma_x1, ny, Gamma_y)
            # determine if increasing the scaling factor is needed in the x dimension 
            # (less diffusive)
            while fred_ind_xnew < freq_ind_x and beta_hat > beta_under:
                if q == 1:
                    break
                
                freq_thres_x = fred_ind_xnew
                freq_ind_x = fred_ind_xnew
                U_new = lst
                u = torch.tensor([U_new for i in range(interm)])
                beta = beta_hat
                X_x = X_x1
                Wx = Wx1
                Gamma_x = Gamma_x1
                u_inv = u_inv_new
                beta_hat /= q
                X_x1, Wx1, Gamma_x1 = Init(n, beta_hat)
                lst = [0 for i2 in range(n*ny)]
                for i2 in range(n):
                    for j2 in range(ny):    
                        lst[i2 + j2*n] = (inverse(X_x1[i2], Y_x[j2], n, ny, beta, beta_y, u_inv))
    
                u_inv_new = discrete_beta(lst, X_x1, Wx1, n, Gamma_x1, Y_x, Wy, ny, Gamma_y, beta_hat, beta_y)#discrete_beta(lst, X_x1, Wx1, n, Gamma_x1, beta_hat)
                fred_ind_xnew = frequency_indicator_x(u_inv_new, n, Gamma_x1, ny, Gamma_y)#frequency_indicator(u_inv_new, n, Gamma_x1)
        
        # use the previous scaling factor \beta_x, \beta_y, calculate the scaling factor
        # in the y dimension
        freq_ind_y = frequency_indicator_y(u_inv_ori, n, Gamma_x_ori, ny, Gamma_y)
        if freq_ind_y > nu *freq_thres_y:
            beta_y_hat = beta_y *q
            Y_x1, Wy1, Gamma_y1 = Init(ny, beta_y_hat)
            lst = [0 for i2 in range(n*ny)]
            for i2 in range(n):
                for j2 in range(ny):    
                    lst[i2 + j2*n] = (inverse(X_x_ori[i2], Y_x1[j2], n, ny, beta_ori, beta_y, copy.deepcopy(coeffs_ori)))

            u_inv_new = discrete_beta(lst, X_x_ori, Wx_ori, n, Gamma_x_ori, Y_x1, Wy1, ny, Gamma_y1, beta_ori, beta_y_hat)
            fred_ind_ynew = frequency_indicator_y(u_inv_new, n, Gamma_x_ori, ny, Gamma_y1)
            # determine if decreasing the scaling factor is needed in the y dimension 
            # (more diffusive)
            while fred_ind_ynew < freq_ind_y and beta_y_hat > beta_under:
                if q == 1:
                    break
                
                freq_thres_y = fred_ind_ynew
                freq_ind_y = fred_ind_ynew
                U_new = lst
                u = torch.tensor([U_new for i in range(interm)])
                beta_y = beta_y_hat
                Y_x = Y_x1
                Wy = Wy1
                Gamma_y = Gamma_y1
                u_inv = u_inv_new
                beta_y_hat *= q
                Y_x1, Wy1, Gamma_y1 = Init(ny, beta_y_hat)
                lst = [0 for i2 in range(n*ny)]
                for i2 in range(n):
                    for j2 in range(ny):    
                        lst[i2 + j2*n] = (inverse(X_x_ori[i2], Y_x1[j2], n, ny, beta_ori, beta_y, u_inv))
                
                u_inv_new = discrete_beta(lst, X_x_ori, Wx_ori, n, Gamma_x_ori, Y_x1, Wy1, ny, Gamma_y1, beta_ori, beta_y_hat)#discrete_beta(lst, X_x1, Wx1, n, Gamma_x1, beta_hat)
                fred_ind_ynew = frequency_indicator_y(u_inv_new, n, Gamma_x_ori, ny, Gamma_y1)#frequency_indicator(u_inv_new, n, Gamma_x1)
        elif freq_ind_y < freq_thres_y:
            beta_y_hat = beta_y /q
            Y_x1, Wy1, Gamma_y1 = Init(ny, beta_y_hat)
            lst = [0 for i2 in range(n*ny)]
            for i2 in range(n):
                for j2 in range(ny):    
                    lst[i2 + j2*n] = (inverse(X_x_ori[i2], Y_x1[j2], n, ny, beta_ori, beta_y, copy.deepcopy(coeffs_ori)))

            u_inv_new = discrete_beta(lst, X_x_ori, Wx_ori, n, Gamma_x_ori, Y_x1, Wy1, ny, Gamma_y1, beta_ori, beta_y_hat)
            fred_ind_ynew = frequency_indicator_y(u_inv_new, n, Gamma_x_ori, ny, Gamma_y1)
            # determine if increasing the scaling factor is needed in the y dimension 
            # (less diffusive)
            while fred_ind_ynew < freq_ind_y and beta_y_hat > beta_under:
                if q == 1:
                    break
                
                freq_thres_y = fred_ind_ynew
                freq_ind_y = fred_ind_ynew
                U_new = lst
                u = torch.tensor([U_new for i in range(interm)])
                beta_y = beta_y_hat
                Y_x = Y_x1
                Wy = Wy1
                Gamma_y = Gamma_y1
                u_inv = u_inv_new
                beta_y_hat /= q
                Y_x1, Wy1, Gamma_y1 = Init(ny, beta_y_hat)
                lst = [0 for i2 in range(n*ny)]
                for i2 in range(n):
                    for j2 in range(ny):    
                        lst[i2 + j2*n] = (inverse(X_x[i2], Y_x1[j2], n, ny, beta_ori, beta_y, u_inv))

                u_inv_new = discrete_beta(lst, X_x_ori, Wx_ori, n, Gamma_x_ori, Y_x1, Wy1, ny, Gamma_y1, beta_ori, beta_y_hat)#discrete_beta(lst, X_x1, Wx1, n, Gamma_x1, beta_hat)
                fred_ind_xnew = frequency_indicator_y(u_inv_new, n, Gamma_x_ori, ny, Gamma_y1)#frequency_indicator(u_inv_new, n, Gamma_x1)

    # calculate the new allocation points and weights
    X_x, Wx, Gamma_x = Init(n, beta)
    Y_x, Wy, Gamma_y = Init(ny, beta_y)
    if t == 0:
        u = [0 for i in range(n*ny)]
        for i in range(n):
            for j in range(ny):
                u[j*n+i] = target_fun(X_x[i], Y_x[j], t)
                
        u = torch.tensor([u for i in range(interm)])
    else:
        
        lst = [0 for i2 in range(n*ny)]
        for i2 in range(n):
            for j2 in range(ny):    
                lst[i2 + j2*n] = (inverse(X_x[i2], Y_x[j2], n, ny, beta_ori, beta_y_ori, coeffs_ori)) 
    
        u_inv = discrete_beta(lst, X_x, Wx, n, Gamma_x, Y_x, Wy, ny, Gamma_y, beta, beta_y)
        u = torch.tensor([lst for i in range(interm)])
   
    
    Matrix = [[0 for i in range(n*ny)] for j in range(n*ny)]
    for i in range(n):
        for j in range(n):
            for i2 in range(ny):
                for j2 in range(ny):
                    Matrix[i2*n+i][j2*n+j] = Hermite(X_x[i], j, beta) * Hermite(Y_x[i2], j2, beta_y)

    Matrix1 = torch.tensor(Matrix, dtype=torch.float64)
    Weight_mat = [[0 for i in range(n*ny)] for j in range(n*ny)]
    for i in range(n):
        for j in range(ny):    
            Weight_mat[i+j*n][i+j*n] = sqrt(Wx[i]) * sqrt(Wy[j])

    Weight_mat = torch.tensor(Weight_mat)
    Y_real = [0 for i in range(n*ny)]
    for i in range(n):
        for j in range(ny):
            Y_real[j*n+i] = target_fun(X_x[i], Y_x[j], t+dt)
            
    Y_real_inv = discrete_beta(Y_real, X_x, Wx, n, Gamma_x, Y_x, Wy, ny, Gamma_y, beta, beta_y)#(Y_real, X_x, Wx, n, Gamma_x, beta)
    Y_real_inv1 = copy.deepcopy(Y_real_inv)
    Y_real_inv2 = torch.tensor([Y_real_inv], dtype=torch.float64)   
    
    # the matrix needed for computing the weak form of the spatial derivative u_xx + u_yy
    Dif_mat = [[0 for i in range(n*ny)] for j in range(n*ny)]
    for i in range(n):
        for j in range(ny):
            if i - 1 > 0:
                Dif_mat[j*n+i][j*n+i-2] = Dif_mat[j*n+i][j*n+i-2]-beta**2 *sqrt((i) * (i-1)) / 2
    
            Dif_mat[j*n+i][j*n+i] = Dif_mat[j*n+i][j*n+i]+(i+1/2) * beta**2
            if i + 2 < n:
                Dif_mat[j*n+i][j*n+i+2] = Dif_mat[j*n+i][j*n+i+2]-beta**2 *sqrt((i+1) * (i+2)) / 2
    
    for i in range(n):
        for j in range(ny):
            if j - 1 > 0:
                Dif_mat[j*n+i][(j-2)*n+i] = Dif_mat[j*n+i][(j-2)*n+i]-beta_y**2 *sqrt((j) * (j-1)) / 2
    
            Dif_mat[j*n+i][j*n+i] = Dif_mat[j*n+i][j*n+i]+(j+1/2) * beta_y**2
            if j + 2 < ny:
                Dif_mat[j*n+i][(j+2)*n+i] = Dif_mat[j*n+i][(j+2)*n+i]-beta_y**2 *sqrt((j+1) * (j+2)) / 2
           
        
    Dif_mat = torch.tensor(Dif_mat, dtype=torch.float64)
    Dif_mat2 = [[0 for i in range(n*ny)] for j in range(n*ny)]
    for i in range(n):
        for j in range(ny):
            if i - 1 >= 0:
                Dif_mat2[j*n+i][j*n+i-1] = Dif_mat2[j*n+i][j*n+i-1]-beta *sqrt((i)  / 2)
    
            if i + 1 < n:
                Dif_mat2[j*n+i][j*n+i+1] = Dif_mat2[j*n+i][j*n+i+1]+beta *sqrt((i+1)  / 2)
    
    for i in range(n):
        for j in range(ny):
            if j - 1 >= 0:
                Dif_mat2[j*n+i][(j-1)*n+i] = Dif_mat2[j*n+i][(j-1)*n+i]-beta_y *sqrt((j) / 2) 
    
        
            if j + 1 < n:
                Dif_mat2[j*n+i][(j+1)*n+i] = Dif_mat2[j*n+i][(j+1)*n+i]+beta_y *sqrt((j+1) / 2)
           
        
    Dif_mat2 = torch.tensor(Dif_mat2, dtype=torch.float64)
    time = [(t + dt * C[i]) for i in range(interm-1)]
    time.append(t + dt)
    # heat source (0)
    F_inter = F(n, ny, t, dt, time, beta, beta_y)
    # set up the neural network model
    model = TwoLayerNet20(1, 75, n*ny)
    Y_pred = model(X)
    criterion = torch.nn.MSELoss(reduction='sum')
    optimizer = torch.optim.SGD(model.parameters(), lr=0.0002)
    optimizer.zero_grad()
    loss = criterion(zeros,torch.matmul(torch.matmul(Matrix1, Y_pred.transpose(0, 1) + dt * torch.matmul(torch.matmul(Dif_mat, Y_pred.transpose(0, 1)) - F_inter, B_matrix)).transpose(0, 1) - u, Weight_mat))
    loss.backward()
    optimizer.step()
    optimizer.zero_grad()
    # training
    while loss.item() > 1e-20 and k < 400000:
        if k % 10000 == 0:
            print(loss.item())
            
        k += 1
        Y_pred = model(X)
        loss = criterion(zeros,torch.matmul(torch.matmul(Matrix1, Y_pred.transpose(0, 1) + dt * torch.matmul(torch.matmul(Dif_mat, Y_pred.transpose(0, 1)) - F_inter, B_matrix)).transpose(0, 1) - u, Weight_mat))
        optimizer.zero_grad()
        loss.backward()
        optimizer.step()
        
    result = sqrt(sum((torch.matmul(torch.matmul(Matrix1, Y_real_inv2.transpose(0, 1)).transpose(0, 1)[0] - torch.matmul(Matrix1, Y_pred.transpose(0, 1)).transpose(0, 1)[interm-1], Weight_mat))**2))
    error.append(result)
    freq_x_list.append(freq_ind_x)
    freq_y_list.append(freq_ind_y)
    beta_x_list.append(beta)
    beta_y_list.append(beta_y)
    print(beta, freq_ind_x, freq_ind_y, result, t)
    k = 0
    t += dt

testing = pd.DataFrame(data = error)
testing.to_csv('error_spectral_parabolic_adpative_2D1.csv', header=False, index = False)
testing = pd.DataFrame(data = beta_x_list)
testing.to_csv('betax_spectral_parabolic_adpative_2D1.csv', header=False, index = False)
testing = pd.DataFrame(data = beta_y_list)
testing.to_csv('betay_spectral_parabolic_adpative_2D1.csv', header=False, index = False)
testing = pd.DataFrame(data = freq_x_list)
testing.to_csv('freq_x_spectral_parabolic_adpative_2D1.csv', header=False, index = False)
testing = pd.DataFrame(data = freq_y_list)
testing.to_csv('freq_y_spectral_parabolic_adpative_2D1.csv', header=False, index = False)
