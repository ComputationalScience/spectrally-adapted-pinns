function multiplot4
subplot('Position', [0.13 0.594 0.34 0.36])
t = 0.1:0.1:2;
E1 = csvread('error_spectral_parabolic_nonadpative_2D1.csv');
E2 = csvread('error_spectral_parabolic_adpative_2D1.csv');
%E1 = E1(1:5001);
%E2 = E2(1:5001);
%E3 = E3(1:5001);
%E4 = E4(1:5001);
h1 = semilogy(t(1:20), E1(1:20), 'color', 'k', 'Linewidth', 1.5 );
hold on;
h2 = semilogy(t(1:20), E2(1:20), 'color', 'r', 'Linewidth', 1.5 );
hold on;
semilogy(t(1:4:20), E1(1:4:20), '*', 'color', 'k', 'Linewidth', 1.5);
hold on;
semilogy(t(1:4:20), E2(1:4:20), 's', 'color', 'r', 'Linewidth', 1.5 );
hold on;

set(gca, 'fontsize', 15)
xlabel('time','fontsize', 20, 'interp', 'latex');
ylabel('$L^2$ error','fontsize', 20, 'interp', 'latex')
[H, h] = legend([h1, h2], '~no scaling', '~scaling', 'interp', 'latex');
H.Box = 'off';
%set(h(6), 'marker', '*')
set(h(4), 'marker', '*')
set(h(6), 'marker', 's')
set(h(1:2), 'fontsize', 20, 'interp', 'latex')
%set(H, 'Position', [.08, .64, .1, .2])
axis([0 2 1e-10 1])
set(gca, 'ytick', [1e-12 1e-11 1e-10, 1e-9, 1e-8, 1e-7, 1e-6, 1e-5, 1e-4, 1e-3, 1e-2, 1e-1, 1])
set(gca, 'yticklabel', {'', '', '10^{-10}','', '',  '','', '10^{-5}', '', '', '', '', '1'})
set(gca,'yminortick','off') 
title('(a) error ', 'fontsize', 20, 'interp', 'latex');
set(H, 'Position', [.17, .82, .1, .12])
%disp(E1)

subplot('Position', [0.615 0.594 0.34 0.36])
t = 0.1:0.1:2;
E1 = csvread('betax_spectral_parabolic_adpative_2D1.csv');
E2 = csvread('betay_spectral_parabolic_adpative_2D1.csv');
h1 = plot(t(1:1:20), E1(1:1:20), 'color', 'b', 'Linewidth', 1.5 );
hold on;
h2 = plot(t(1:1:20), E2(1:1:20), 'color', 'r', 'Linewidth', 1.5 );
hold on;
plot(t(1:4:20), E1(1:4:20), '*', 'color', 'b', 'Linewidth', 1.5);
hold on;
plot(t(1:4:20), E2(1:4:20), 's', 'color', 'r', 'Linewidth', 1.5 );
hold on;
set(gca, 'fontsize', 15)
xlabel('time','fontsize', 20, 'interp', 'latex');
ylabel('$\beta$','fontsize', 20, 'interp', 'latex')
[H, h] = legend([h1, h2], '~$\beta_x$', '~$\beta_y$', 'interp', 'latex');
H.Box = 'off';
%set(h(6), 'marker', '*')
set(h(4), 'marker', '*')
set(h(6), 'marker', 's')
set(h(1:2), 'fontsize', 20, 'interp', 'latex')
set(H, 'Position', [.74, .591, .1, .12])
axis([0 2 0.2, 0.55])
set(gca, 'ytick', [0.20, 0.25, 0.30, 0.35, 0.40, 0.45, 0.50, 0.55])
set(gca, 'yticklabel', {0.20, '', 0.30, '', 0.40, '', 0.50, ''})
set(gca,'yminortick','off') 
title('(b) scaling factor ', 'fontsize', 20, 'interp', 'latex');

subplot('Position', [0.13 0.077 0.34 0.36])
t = 0.1:0.1:2;
E1 = csvread('freq_x_spectral_parabolic_nonadpative_2D1.csv');
E4 = csvread('freq_x_spectral_parabolic_adpative_2D1.csv');
h1 = semilogy(t(1:20), E1(1:20), 'color', 'k', 'Linewidth', 1.5 );
hold on;
h4 = semilogy(t(1:20), E4(1:20), 'color', 'r', 'Linewidth', 1.5 );
hold on;
semilogy(t(1:4:20), E1(1:4:20), '*', 'color', 'k', 'Linewidth', 1.5);
hold on;
semilogy(t(1:4:20), E4(1:4:20), 's', 'color', 'r', 'Linewidth', 1.5 );
hold on;
set(gca, 'fontsize', 15)
xlabel('time','fontsize', 20, 'interp', 'latex');
ylabel('$\mathcal{F}_x$','fontsize', 20, 'interp', 'latex')
[H, h] = legend([h1, h4], '~no scaling', '~scaling', 'interp', 'latex');
H.Box = 'off';
set(h(4), 'marker', '*')
set(h(6), 'marker', 's')
set(h(1:2), 'fontsize', 20, 'interp', 'latex')
set(gca, 'ytick', [1e-12 1e-11 1e-10, 1e-9, 1e-8, 1e-7, 1e-6, 1e-5, 1e-4, 1e-3, 1e-2, 1e-1, 1])
set(gca, 'yticklabel', {'', '', '10^{-10}','', '',  '','', '10^{-5}', '', '', '', '', '1'})
set(gca,'yminortick','off')
axis([0 2 1e-10 1])
set(H, 'Position', [.18, .32, .1, .12])
title('(c) frequency indicator in $x$', 'fontsize', 20, 'interp', 'latex');


subplot('Position', [0.615 0.077 0.34 0.36])
t = 0.1:0.1:2;
E1 = csvread('freq_y_spectral_parabolic_nonadpative_2D1.csv');
E4 = csvread('freq_y_spectral_parabolic_adpative_2D1.csv');
h1 = semilogy(t(1:20), E1(1:20), 'color', 'k', 'Linewidth', 1.5 );
hold on;
h4 = semilogy(t(1:20), E4(1:20), 'color', 'r', 'Linewidth', 1.5 );
hold on;
semilogy(t(1:4:20), E1(1:4:20), '*', 'color', 'k', 'Linewidth', 1.5);
hold on;
semilogy(t(1:4:20), E4(1:4:20), 's', 'color', 'r', 'Linewidth', 1.5 );
hold on;
set(gca, 'fontsize', 15)
xlabel('time','fontsize', 20, 'interp', 'latex');
ylabel('$\mathcal{F}_y$','fontsize', 20, 'interp', 'latex')
[H, h] = legend([h1, h4], '~no scaling', '~scaling', 'interp', 'latex');
H.Box = 'off';
set(h(4), 'marker', '*')
set(h(6), 'marker', 's')
set(h(1:2), 'fontsize', 20, 'interp', 'latex')
set(gca, 'ytick', [1e-13, 1e-12 1e-11 1e-10, 1e-9, 1e-8, 1e-7, 1e-6, 1e-5, 1e-4, 1e-3, 1e-2, 1e-1, 1])
set(gca, 'yticklabel', {'', '', '', '10^{-10}','', '',  '','', '10^{-5}', '', '', '', '', '1'})
set(gca,'yminortick','off')
axis([0 2 1e-13 1])
set(H, 'Position', [.75, .1, .1, .12])
title('(d) frequency indicator in $y$', 'fontsize', 20, 'interp', 'latex');

end