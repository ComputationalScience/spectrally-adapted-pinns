# -*- coding: utf-8 -*-


# -*- coding: utf-8 -*-
"""
Created on Wed Sep  1 13:55:34 2021

@author: 15000
"""

# -*- coding: utf-8 -*-
"""
Created on Wed Sep  1 05:50:31 2021

@author: 15000
"""
import sys
sys.path.insert(0, '../../utilities/')

import pandas as pd
from Network_Model import TwoLayerNet2
from math import sin, sqrt, acos, cos, pi
import random
import numpy as np
import torch
from torch.autograd import Variable
import pdb
import csv
import time      
import copy
from Chebyshev import Chebyshev, Init, discrete_beta, inverse, frequency_indicator

xi = 0.3
k = 1
beta = 0.8
n = 9
torch.set_default_tensor_type(torch.DoubleTensor)

# import the Lagendre collocation c_j for time integration, and the matrix of coefficients
# in a high-order RK scheme A = (a)_{ij} as well as the coefficients b_i
C_record = csv.reader(open('C.csv'))
B_record = csv.reader(open('B.csv'))
A_record = csv.reader(open('A.csv'))
C = []
B = []

for i in C_record:
    C.append(float(i[0]))

for i in B_record:
    B.append(float(i[0]))

A = [[] for i in range(len(C)+1)]
interm = len(C) + 1
ss = 0
for i in A_record:
    for j in i:
        A[ss].append(float(j))
        
    ss += 1

B_matrix = torch.tensor(A, dtype=torch.float64)
B_matrix = B_matrix.transpose(0, 1)
    
# the analytic solution
def target_fun(x, t=0):
    global xi, k
    y = cos((t+1) * (x + 2))
    return y

X_x, Wx, Gamma_x = Init(n, beta)
Weight_mat = [[0 for i in range(n)] for j in range(n)]
for i in range(n):
    Weight_mat[i][i] = sqrt(Wx[i])

Weight_mat = torch.tensor(Weight_mat)

# the matrix for calculating the spatial derivative u_x
Dif_mat = [[0 for i in range(n)] for j in range(n)]
for i in range(n):
    if i - 1 > 0:
        Dif_mat[i][i-2] = -beta**2 *sqrt((i) * (i-1)) / 2
    
    Dif_mat[i][i] = (i+1/2) * beta**2
    if i + 2 < n:
        Dif_mat[i][i+2] = -beta**2 *sqrt((i+1) * (i+2)) / 2
        
Dif_mat = torch.tensor(Dif_mat, dtype=torch.float64)

X = []
Y = []
t = 0
dt = 0.01
times = [(t + dt * C[i]) for i in range(interm-1)]
times.append(t + dt)
X_ori = X
X = torch.tensor([[times[i]] for i in range(interm)], dtype=torch.float64)
k = 1

Matrix1 = [[Chebyshev(X_x[j], i, beta) for j in range(n)] for i in range(n)]
Matrix1 = torch.tensor(Matrix1, dtype=torch.float64)
Matrix1 = Matrix1.transpose(0, 1)

error = []
zeros = [0 for i in range(n)]
zeros = torch.tensor([zeros for i in range(interm)], dtype = torch.float64)
t_target = 3
u = [target_fun(X_x[i], t) for i in range(n)]
Weight_mat = [[0 for i in range(n)] for j in range(n)]
for i in range(n):
    Weight_mat[i][i] = sqrt(Wx[i])

Weight_mat = torch.tensor(Weight_mat)

# set the parameters
u = torch.tensor([u for i in range(interm)])
freq_ind = None
thres = 1.5
thres0 = 1.5
update = 1.3
q = 1
nu = 1 / q
beta_under = 0.2
beta_over = 5
error = []
t_time = time.time()
N_list = []
freq_list = []
while t < t_target:
    # calculate the initial frequency inidcator when t = 0
    if freq_ind is None:
        Y_real_inv = discrete_beta([target_fun(X_x[i], t) for i in range(n)], X_x, Wx, n, Gamma_x, beta)
        freq_ind = frequency_indicator(Y_real_inv, n, Gamma_x)
        freq_thres_p = freq_ind
        freq_thres = freq_ind 
    else:
        u = []
        lst = []
        coeffs = [float(Y_pred[interm-1, i2]) for i2 in range(n)]
        u_inv = coeffs
        for i2 in range(n):
            if i2 < n-1:
                lst.append(inverse(X_x[i2], coeffs, beta))
            else:
                lst.append(target_fun(X_x[i2], t))
        
        for i1 in range(interm):
            u.append(copy.deepcopy(lst))
            
        u = torch.tensor(u)
        Y_real_inv = [float(Y_pred[interm-1, i2]) for i2 in range(n)]
        # at each time step, calculate the current frequency indicator
        freq_ind = frequency_indicator(Y_real_inv, n, Gamma_x)
        
        
        
    ll = 0
    marker = 0
    decrease = 0
    # if the current frequency indicator is smaller than the threshold, could consider
    # decrease the expansion order if the frequency indicator after coarsening is still
    # smaller than the threshold
    while decrease != 1 and freq_ind < freq_thres_p / thres0 and n > 0 and t > 0:
        break
        decrease = 1
        n0 = n - 1
        X_node10, W_x10, Gamma_x10 = Init(n0, beta)
        U2_nexty0 = [inverse(X_node10[i2], u_inv, beta) for i2 in range(n0)]
        U2_inv_nexty0 = discrete_beta(lst, X_node10, W_x10, n0, Gamma_x10, beta)
        freq_ind_next = frequency_indicator(U2_inv_nexty0, n0, Gamma_x10)
        if freq_ind_next < freq_ind:
            decrease = 2
            u_inv = U2_inv_nexty0
            freq_ind = freq_ind_next
            n = n0
            X_x, Wx, Gamma_x = Init(n, beta)
            u = torch.tensor([[inverse(X_x[i2], u_inv, beta) for i2 in range(n)] for i in range(interm)])
    
    # if the current frequency indicator is larger than the threshold, increase 
    # the expansion order
    while decrease == 0 and ll < 3 and freq_ind > freq_thres_p * thres and t > 0:
        break
        marker = 1
        ll += 1
        n += 1
        X_x, Wx, Gamma_x = Init(n, beta)
        u2_inv_new = [0 for i in range(n)]
        for i4 in range(n-1):
            u2_inv_new[i4] = u_inv[i4]
        
        u_inv = u2_inv_new
        u = torch.tensor([[inverse(X_x[i2], u_inv, beta) for i2 in range(n)] for i in range(interm)])
        freq_ind = frequency_indicator(u_inv, n, Gamma_x)
    
    # update the parameters and thresholds for adjusting the expansion order
    if marker == 1:
        thres *= update
        freq_thres_p = freq_ind
    if decrease == 2:
        freq_thres_p = freq_ind
        
        
    X_x, Wx, Gamma_x = Init(n, beta)
    Matrix1 = [[Chebyshev(X_x[j], i, beta) for j in range(n)] for i in range(n)]
    Matrix1 = torch.tensor(Matrix1, dtype=torch.float64)
    Matrix1 = Matrix1.transpose(0, 1)
    Weight_mat = [[0 for i in range(n)] for j in range(n)]
    for i in range(n):
        Weight_mat[i][i] = sqrt(Wx[i])

    Weight_mat = torch.tensor(Weight_mat)
    if t == 0:
        u = [target_fun(X_x[i], t) for i in range(n)]
        Y_real = discrete_beta(u, X_x, Wx, n, Gamma_x, beta)
        u = torch.tensor([u for i in range(interm)])

    Y_real_inv = discrete_beta([target_fun(X_x[i], t+dt) for i in range(n)], X_x, Wx, n, Gamma_x, beta)
    Y_real_inv1 = copy.deepcopy(Y_real_inv)
    Y_real_inv2 = torch.tensor([Y_real_inv], dtype=torch.float64)
    
    # the matrix needed for computing the spatial derivative u_x
    Dif_mat = [[0 for i in range(n)] for j in range(n)]
    for i in range(n):
        if i - 1 > 0:
            Dif_mat[i][i-2] = -beta**2 *sqrt((i) * (i-1)) / 2
    
        Dif_mat[i][i] = (i+1/2) * beta**2
        if i + 2 < n:
            Dif_mat[i][i+2] = -beta**2 *sqrt((i+1) * (i+2)) / 2
      
    Dif_mat = torch.tensor(Dif_mat, dtype=torch.float64)
    times = [(t + dt * C[i]) for i in range(interm-1)]
    times.append(t + dt)
    zeros = [0 for i in range(n)]
    zeros = torch.tensor([zeros for i in range(interm)], dtype = torch.float64)
    
    # set up the neural network model and optimizer
    model = TwoLayerNet2(1, 200, n)
    Y_pred = model(X)
    criterion = torch.nn.MSELoss(reduction='sum')
    optimizer = torch.optim.SGD(model.parameters(), lr=0.0005)
    optimizer.zero_grad()
    coef_mat = [[(X_x[i3]+2) / (times[j3] + 1) for i3 in range(n)] for j3 in range(interm)]
    coef_mat = torch.tensor(coef_mat)
    coef_mat1 = [[(X_x[i3]+2) / (t + 1) for i3 in range(n)] for j3 in range(interm)]
    coef_mat1 = torch.tensor(coef_mat)
    
    # given the coefficients (u_0, u_1,...,u_n-1) of u=\sum u_i * T_i(x), calculate its derivative
    # \partial_x u
    def diff_operator(Origin, n, X_x):
        global interm
        A = torch.tensor([[0. for i3 in range(n)] for j3 in range(n)])
        N = n-1
        C0 = [1 for i3 in range(n)]
        C0[0] = 2
        C0[-1] = 2
        for i3 in range(n):
            for j3 in range(n):
                if i3 == 0 and j3 == 0:
                    A[i3, j3] = -(2*N**2+1) / 6
                elif i3 == n-1 and j3 == n-1:
                    A[i3, j3] = (2*N**2+1) / 6
                elif i3 == j3:
                    A[i3, j3] = -X_x[i3] / (2 * (1 - X_x[i3]**2))
                else:
                    A[i3, j3] = C0[i3] / C0[j3] * (-1)**(i3+j3) / (X_x[i3] - X_x[j3])
        
        Origin = torch.matmul(A, Origin)
        Origin = Origin.transpose(0, 1)
        return Origin
        
    
    Diff = u.transpose(0, 1)
    Diff = coef_mat1 * diff_operator(Diff, n, X_x)
    Diff = Diff.transpose(0, 1)
    Diff = torch.matmul(Matrix1, Y_pred.transpose(0, 1))
    Diff = coef_mat * diff_operator(Diff, n, X_x)
    Diff = Diff.transpose(0, 1)
    criterion = torch.nn.MSELoss(reduction='sum')
    optimizer = torch.optim.SGD(model.parameters(), lr=0.001)
    optimizer.zero_grad()
    loss = criterion(zeros,torch.matmul((torch.matmul(Matrix1, Y_pred.transpose(0, 1)) - dt * torch.matmul(Diff, B_matrix)).transpose(0, 1) - u, Weight_mat))
    loss.backward()
    optimizer.step()
    optimizer.zero_grad()
    k = 0
    # training 
    while loss.item() > 1e-20 and k < 100000:
        k += 1
        Y_pred = model(X)
        Diff = torch.matmul(Matrix1, Y_pred.transpose(0, 1))
        Diff = coef_mat * diff_operator(Diff, n, X_x)
        Diff = Diff.transpose(0, 1)
        loss = criterion(zeros,torch.matmul((torch.matmul(Matrix1, Y_pred.transpose(0, 1)) - dt * torch.matmul(Diff, B_matrix)).transpose(0, 1) - u, Weight_mat))
        if k % 10000 == 0 and k > 0:
            print(loss.item(), k)
            
        optimizer.zero_grad()
        loss.backward()
        optimizer.step()
        
    result = sqrt(sum((torch.matmul(torch.matmul(Matrix1, Y_real_inv2.transpose(0, 1)).transpose(0, 1)[0] - torch.matmul(Matrix1, Y_pred.transpose(0, 1)).transpose(0, 1)[interm-1], Weight_mat))**2))
    error.append(result)
    freq_list.append(freq_ind)
    N_list.append(n)
    print(beta, freq_ind, result, t, n)
    k = 0
    t += dt
    
print(time.time() - t_time)
testing = pd.DataFrame(data = error)
testing.to_csv('error_spectral_bounded_nonadaptive1.csv', header=False, index = False)
testing = pd.DataFrame(data = N_list)
testing.to_csv('N_spectral_bounded_nonadaptive1.csv', header=False, index = False)
testing = pd.DataFrame(data = freq_list)
testing.to_csv('freq_spectral_bounded_nonadaptive1.csv', header=False, index = False)
print(k)
