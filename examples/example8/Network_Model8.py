# -*- coding: utf-8 -*-
"""
Created on Mon Nov  1 10:53:30 2021

@author: 15000
"""
import torch
from math import sqrt

class TwoLayerNet20(torch.nn.Module):
    torch.set_default_tensor_type(torch.DoubleTensor)
    def __init__(self, D_in, H, D_out):
        """
        In the constructor we instantiate two nn.Linear modules and assign them as
        member variables.
        """
        super(TwoLayerNet20, self).__init__()
        self.linear1 = torch.nn.Linear(D_in, H)
        self.renorm1 = torch.nn.BatchNorm1d(H)
        self.linear2 = torch.nn.Linear(H, H)
        self.renorm2 = torch.nn.BatchNorm1d(H)
        self.linear3 = torch.nn.Linear(H, H)
        self.renorm3 = torch.nn.BatchNorm1d(H)
        self.linear4 = torch.nn.Linear(H, H)
        self.renorm4 = torch.nn.BatchNorm1d(H)
        self.linear5 = torch.nn.Linear(H, H)
        self.renorm5 = torch.nn.BatchNorm1d(H)
        self.linear6 = torch.nn.Linear(H, H)
        self.renorm6 = torch.nn.BatchNorm1d(H)
        self.linear7 = torch.nn.Linear(H, H)
        self.renorm7 = torch.nn.BatchNorm1d(H)
        self.linear8 = torch.nn.Linear(H, H)
        self.renorm8 = torch.nn.BatchNorm1d(H)
        self.linear9 = torch.nn.Linear(H, D_out)
        
    
    def normalize(self, data):
        m = len(data)
        n = len(data[0])
        sigma2 = [0 for i in range(n)]
        mu2 = [0 for j in range(n)]
        for i in range(n):
            for j in range(m):
                mu2[i] = mu2[i] + data[j, i]
            
            mu2[i] = mu2[i] / m
        
        for i in range(n):
            for j in range(m):
                sigma2[i] += (data[j, i] - mu2[i])**2
            
            sigma2[i] = sqrt(sigma2[i]/m)
        
        for i in range(m):
            for j in range(n):
                data[i, j] = (data[i, j] - mu2[i]) / sigma2[i]
        
        return data
    
    def forward(self, x):
        """
        In the forward function we accept a Tensor of input data and we must return
        a Tensor of output data. We can use Modules defined in the constructor as
        well as arbitrary operators on Tensors.
        """
        h_relu1 = self.linear1(x)
        h_relu1 = self.renorm1(h_relu1)
        h_relu1 = torch.relu(h_relu1)
        h_relu2 = self.linear2(h_relu1)
        h_relu2 = self.renorm2(h_relu2)
        h_relu2 = torch.relu(h_relu2)
        h_relu3 = self.linear3(h_relu2)
        h_relu3 = self.renorm3(h_relu3)
        h_relu3 = torch.relu(h_relu3)
        h_relu4 = self.linear4(h_relu3)
        h_relu4 = self.renorm4(h_relu4)
        h_relu4 = torch.relu(h_relu4)
        h_relu5 = self.linear5(h_relu4)
        h_relu5 = self.renorm5(h_relu5)
        h_relu5 = torch.relu(h_relu5)
        h_relu6 = self.linear6(h_relu5)
        h_relu6 = self.renorm6(h_relu6)
        h_relu6 = torch.relu(h_relu6)
        h_relu7 = self.linear7(h_relu6)
        h_relu7 = self.renorm7(h_relu7)
        h_relu7 = torch.relu(h_relu7)
        h_relu8 = self.linear8(h_relu7)
        h_relu8 = self.renorm8(h_relu8)
        h_relu8 = torch.relu(h_relu8)
        y_pred = self.linear9(h_relu8)
        return y_pred