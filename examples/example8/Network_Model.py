# -*- coding: utf-8 -*-
"""
Created on Tue Dec 22 03:01:57 2020

@author: 15000
"""
import torch
from math import sqrt


class TwoLayerNet(torch.nn.Module):
    def __init__(self, D_in, H, D_out):
        """
        In the constructor we instantiate two nn.Linear modules and assign them as
        member variables.
        """
        torch.set_default_tensor_type(torch.DoubleTensor)
        super(TwoLayerNet, self).__init__()
        self.linear1 = torch.nn.Linear(D_in, H)
        self.renorm1 = torch.nn.BatchNorm1d(H)
        self.linear2 = torch.nn.Linear(H, H)
        self.renorm2 = torch.nn.BatchNorm1d(H)
        self.linear3 = torch.nn.Linear(H, H)
        self.renorm3 = torch.nn.BatchNorm1d(H)
        self.linear4 = torch.nn.Linear(H, H)
        self.renorm4 = torch.nn.BatchNorm1d(H)
        self.linear5 = torch.nn.Linear(H, D_out)
        
    
    def normalize(self, data):
        m = len(data)
        n = len(data[0])
        sigma2 = [0 for i in range(n)]
        mu2 = [0 for j in range(n)]
        for i in range(n):
            for j in range(m):
                mu2[i] = mu2[i] + data[j, i]
            
            mu2[i] = mu2[i] / m
        
        for i in range(n):
            for j in range(m):
                sigma2[i] += (data[j, i] - mu2[i])**2
            
            sigma2[i] = sqrt(sigma2[i]/m)
        
        for i in range(m):
            for j in range(n):
                data[i, j] = (data[i, j] - mu2[i]) / sigma2[i]
        
        return data
    
    def forward(self, x):
        """
        In the forward function we accept a Tensor of input data and we must return
        a Tensor of output data. We can use Modules defined in the constructor as
        well as arbitrary operators on Tensors.
        """
        h_relu1 = self.linear1(x)
        h_relu1 = self.renorm1(h_relu1)
        h_relu1 = torch.relu(h_relu1)
        h_relu2 = self.linear2(h_relu1)
        h_relu2 = self.renorm2(h_relu2)
        h_relu2 = torch.relu(h_relu2)
        h_relu3 = self.linear3(h_relu2)
        h_relu3 = self.renorm3(h_relu3)
        h_relu3 = torch.relu(h_relu3)
        h_relu4 = self.linear4(h_relu3)
        h_relu4 = self.renorm4(h_relu4)
        y_pred = self.linear5(h_relu4)
        return y_pred

class TwoLayerNet1(torch.nn.Module):
    def __init__(self, D_in, H, D_out):
        """
        In the constructor we instantiate two nn.Linear modules and assign them as
        member variables.
        """
        super(TwoLayerNet, self).__init__()
        self.linear1 = torch.nn.Linear(D_in, H)
        self.renorm1 = torch.nn.BatchNorm1d(H)
        self.linear2 = torch.nn.Linear(H, H)
        self.renorm2 = torch.nn.BatchNorm1d(H)
        self.linear3 = torch.nn.Linear(H, H)
        self.renorm3 = torch.nn.BatchNorm1d(H)
        self.linear4 = torch.nn.Linear(H, D_out)
        
    
    def normalize(self, data):
        m = len(data)
        n = len(data[0])
        sigma2 = [0 for i in range(n)]
        mu2 = [0 for j in range(n)]
        for i in range(n):
            for j in range(m):
                mu2[i] = mu2[i] + data[j, i]
            
            mu2[i] = mu2[i] / m
        
        for i in range(n):
            for j in range(m):
                sigma2[i] += (data[j, i] - mu2[i])**2
            
            sigma2[i] = sqrt(sigma2[i]/m)
        
        for i in range(m):
            for j in range(n):
                data[i, j] = (data[i, j] - mu2[i]) / sigma2[i]
        
        return data
    
    def forward(self, x):
        """
        In the forward function we accept a Tensor of input data and we must return
        a Tensor of output data. We can use Modules defined in the constructor as
        well as arbitrary operators on Tensors.
        """
        h_relu1 = self.linear1(x)
        h_relu1 = self.renorm1(h_relu1)
        h_relu1 = torch.relu(h_relu1)
        h_relu2 = self.linear2(h_relu1)
        h_relu2 = self.renorm2(h_relu2)
        h_relu2 = torch.relu(h_relu2)
        h_relu3 = self.linear3(h_relu2)
        h_relu3 = self.renorm3(h_relu3)
        h_relu3 = torch.relu(h_relu3)
        y_pred = self.linear4(h_relu3)
        return y_pred


class TwoLayerNet20(torch.nn.Module):
    torch.set_default_tensor_type(torch.DoubleTensor)
    def __init__(self, D_in, H, D_out):
        """
        In the constructor we instantiate two nn.Linear modules and assign them as
        member variables.
        """
        super(TwoLayerNet20, self).__init__()
        self.linear1 = torch.nn.Linear(D_in, H)
        self.renorm1 = torch.nn.BatchNorm1d(H)
        self.linear2 = torch.nn.Linear(H, H)
        self.renorm2 = torch.nn.BatchNorm1d(H)
        self.linear3 = torch.nn.Linear(H, H)
        self.renorm3 = torch.nn.BatchNorm1d(H)
        self.linear4 = torch.nn.Linear(H, H)
        self.renorm4 = torch.nn.BatchNorm1d(H)
        self.linear5 = torch.nn.Linear(H, H)
        self.renorm5 = torch.nn.BatchNorm1d(H)
        self.linear6 = torch.nn.Linear(H, H)
        self.renorm6 = torch.nn.BatchNorm1d(H)
        self.linear7 = torch.nn.Linear(H, H)
        self.renorm7 = torch.nn.BatchNorm1d(H)
        self.linear8 = torch.nn.Linear(H, H)
        self.renorm8 = torch.nn.BatchNorm1d(H)
        self.linear9 = torch.nn.Linear(H, H)
        self.renorm9 = torch.nn.BatchNorm1d(H)
        self.linear10 = torch.nn.Linear(H, H)
        self.renorm10 = torch.nn.BatchNorm1d(H)
        self.linear11 = torch.nn.Linear(H, H)
        self.renorm11 = torch.nn.BatchNorm1d(H)
        self.linear12 = torch.nn.Linear(H, H)
        self.renorm12 = torch.nn.BatchNorm1d(H)
        self.linear13 = torch.nn.Linear(H, H)
        self.renorm13 = torch.nn.BatchNorm1d(H)
        self.linear14 = torch.nn.Linear(H, D_out)
        
    
    def normalize(self, data):
        m = len(data)
        n = len(data[0])
        sigma2 = [0 for i in range(n)]
        mu2 = [0 for j in range(n)]
        for i in range(n):
            for j in range(m):
                mu2[i] = mu2[i] + data[j, i]
            
            mu2[i] = mu2[i] / m
        
        for i in range(n):
            for j in range(m):
                sigma2[i] += (data[j, i] - mu2[i])**2
            
            sigma2[i] = sqrt(sigma2[i]/m)
        
        for i in range(m):
            for j in range(n):
                data[i, j] = (data[i, j] - mu2[i]) / sigma2[i]
        
        return data
    
    def forward(self, x):
        """
        In the forward function we accept a Tensor of input data and we must return
        a Tensor of output data. We can use Modules defined in the constructor as
        well as arbitrary operators on Tensors.
        """
        h_relu1 = self.linear1(x)
        h_relu1 = self.renorm1(h_relu1)
        h_relu1 = torch.relu(h_relu1)
        h_relu2 = self.linear2(h_relu1)
        h_relu2 = self.renorm2(h_relu2)
        h_relu2 = torch.relu(h_relu2)
        h_relu3 = self.linear3(h_relu2)
        h_relu3 = self.renorm3(h_relu3)
        h_relu3 = torch.relu(h_relu3)
        h_relu4 = self.linear4(h_relu3)
        h_relu4 = self.renorm4(h_relu4)
        h_relu4 = torch.relu(h_relu4)
        h_relu5 = self.linear5(h_relu4)
        h_relu5 = self.renorm5(h_relu5)
        h_relu5 = torch.relu(h_relu5)
        h_relu6 = self.linear6(h_relu5)
        h_relu6 = self.renorm6(h_relu6)
        h_relu6 = torch.relu(h_relu6)
        h_relu7 = self.linear7(h_relu6)
        h_relu7 = self.renorm7(h_relu7)
        h_relu7 = torch.relu(h_relu7)
        h_relu8 = self.linear8(h_relu7)
        h_relu8 = self.renorm8(h_relu8)
        h_relu8 = torch.relu(h_relu8)
        h_relu9 = self.linear9(h_relu8)
        h_relu9 = self.renorm9(h_relu9)
        h_relu9 = torch.relu(h_relu9)
        h_relu10 = self.linear10(h_relu9)
        h_relu10 = self.renorm10(h_relu10)
        h_relu10 = torch.relu(h_relu10)
        h_relu11 = self.linear11(h_relu10)
        h_relu11 = self.renorm11(h_relu11)
        h_relu11 = torch.relu(h_relu11)
        h_relu12 = self.linear12(h_relu11)
        h_relu12 = self.renorm12(h_relu12)
        h_relu12 = torch.relu(h_relu12)
        h_relu13 = self.linear13(h_relu12)
        h_relu13 = self.renorm13(h_relu13)
        #h_relu13 = torch.relu(h_relu13)
        y_pred = self.linear14(h_relu13)
        return y_pred

class TwoLayerNet2(torch.nn.Module):
    torch.set_default_tensor_type(torch.DoubleTensor)
    def __init__(self, D_in, H, D_out):
        """
        In the constructor we instantiate two nn.Linear modules and assign them as
        member variables.
        """
        super(TwoLayerNet2, self).__init__()
        self.linear1 = torch.nn.Linear(D_in, H)
        self.renorm1 = torch.nn.BatchNorm1d(H)
        self.linear2 = torch.nn.Linear(H, H)
        self.renorm2 = torch.nn.BatchNorm1d(H)
        self.linear3 = torch.nn.Linear(H, H)
        self.renorm3 = torch.nn.BatchNorm1d(H)
        self.linear4 = torch.nn.Linear(H, H)
        self.renorm4 = torch.nn.BatchNorm1d(H)
        self.linear5 = torch.nn.Linear(H, H)
        self.renorm5 = torch.nn.BatchNorm1d(H)
        self.linear6 = torch.nn.Linear(H, H)
        self.renorm6 = torch.nn.BatchNorm1d(H)
        self.linear7 = torch.nn.Linear(H, H)
        self.renorm7 = torch.nn.BatchNorm1d(H)
        self.linear8 = torch.nn.Linear(H, H)
        self.renorm8 = torch.nn.BatchNorm1d(H)
        self.linear9 = torch.nn.Linear(H, D_out)
        
    
    def normalize(self, data):
        m = len(data)
        n = len(data[0])
        sigma2 = [0 for i in range(n)]
        mu2 = [0 for j in range(n)]
        for i in range(n):
            for j in range(m):
                mu2[i] = mu2[i] + data[j, i]
            
            mu2[i] = mu2[i] / m
        
        for i in range(n):
            for j in range(m):
                sigma2[i] += (data[j, i] - mu2[i])**2
            
            sigma2[i] = sqrt(sigma2[i]/m)
        
        for i in range(m):
            for j in range(n):
                data[i, j] = (data[i, j] - mu2[i]) / sigma2[i]
        
        return data
    
    def forward(self, x):
        """
        In the forward function we accept a Tensor of input data and we must return
        a Tensor of output data. We can use Modules defined in the constructor as
        well as arbitrary operators on Tensors.
        """
        h_relu1 = self.linear1(x)
        h_relu1 = self.renorm1(h_relu1)
        h_relu1 = torch.relu(h_relu1)
        h_relu2 = self.linear2(h_relu1)
        h_relu2 = self.renorm2(h_relu2)
        h_relu2 = torch.relu(h_relu2)
        h_relu3 = self.linear3(h_relu2)
        h_relu3 = self.renorm3(h_relu3)
        h_relu3 = torch.relu(h_relu3)
        h_relu4 = self.linear4(h_relu3)
        h_relu4 = self.renorm4(h_relu4)
        h_relu5 = self.linear5(h_relu4)
        h_relu5 = self.renorm5(h_relu5)
        h_relu6 = self.linear6(h_relu5)
        h_relu6 = self.renorm6(h_relu6)
        h_relu7 = self.linear7(h_relu6)
        h_relu7 = self.renorm7(h_relu7)
        h_relu8 = self.linear8(h_relu7)
        h_relu8 = self.renorm8(h_relu8)
        y_pred = self.linear9(h_relu8)
        return y_pred

class TwoLayerNet2_new(torch.nn.Module):
    torch.set_default_tensor_type(torch.DoubleTensor)
    def __init__(self, D_in, H, D_out):
        """
        In the constructor we instantiate two nn.Linear modules and assign them as
        member variables.
        """
        super(TwoLayerNet2_new, self).__init__()
        self.linear1 = torch.nn.Linear(D_in, H)
        self.renorm1 = torch.nn.BatchNorm1d(H)
        self.linear2 = torch.nn.Linear(H, H)
        self.renorm2 = torch.nn.BatchNorm1d(H)
        self.linear3 = torch.nn.Linear(H, H)
        self.renorm3 = torch.nn.BatchNorm1d(H)
        self.linear4 = torch.nn.Linear(H, H)
        self.renorm4 = torch.nn.BatchNorm1d(H)
        self.linear5 = torch.nn.Linear(H, D_out)
        
    
    def normalize(self, data):
        m = len(data)
        n = len(data[0])
        sigma2 = [0 for i in range(n)]
        mu2 = [0 for j in range(n)]
        for i in range(n):
            for j in range(m):
                mu2[i] = mu2[i] + data[j, i]
            
            mu2[i] = mu2[i] / m
        
        for i in range(n):
            for j in range(m):
                sigma2[i] += (data[j, i] - mu2[i])**2
            
            sigma2[i] = sqrt(sigma2[i]/m)
        
        for i in range(m):
            for j in range(n):
                data[i, j] = (data[i, j] - mu2[i]) / sigma2[i]
        
        return data
    
    def forward(self, x):
        """
        In the forward function we accept a Tensor of input data and we must return
        a Tensor of output data. We can use Modules defined in the constructor as
        well as arbitrary operators on Tensors.
        """
        h_relu1 = self.linear1(x)
        h_relu1 = torch.relu(h_relu1)
        h_relu2 = self.linear2(h_relu1)
        h_relu2 = torch.relu(h_relu2)
        h_relu3 = self.linear3(h_relu2)
        h_relu3 = torch.relu(h_relu3)
        h_relu4 = self.linear4(h_relu3)
        y_pred = self.linear5(h_relu4)
        return y_pred


class TwoLayerNet2_complex(torch.nn.Module):
    torch.set_default_tensor_type(torch.DoubleTensor)
    def __init__(self, D_in, H, D_out):
        """
        In the constructor we instantiate two nn.Linear modules and assign them as
        member variables.
        """
        super(TwoLayerNet2_complex, self).__init__()
        self.linear1 = torch.nn.Linear(D_in, H)
        self.renorm1 = torch.nn.BatchNorm1d(H)
        self.linear2 = torch.nn.Linear(H, H)
        self.renorm2 = torch.nn.BatchNorm1d(H)
        self.linear3 = torch.nn.Linear(H, H)
        self.renorm3 = torch.nn.BatchNorm1d(H)
        self.linear4 = torch.nn.Linear(H, H)
        self.renorm4 = torch.nn.BatchNorm1d(H)
        self.linear5 = torch.nn.Linear(H, D_out)
        
    
    def normalize(self, data):
        m = len(data)
        n = len(data[0])
        sigma2 = [0 for i in range(n)]
        mu2 = [0 for j in range(n)]
        for i in range(n):
            for j in range(m):
                mu2[i] = mu2[i] + data[j, i]
            
            mu2[i] = mu2[i] / m
        
        for i in range(n):
            for j in range(m):
                sigma2[i] += (data[j, i] - mu2[i])**2
            
            sigma2[i] = sqrt(sigma2[i]/m)
        
        for i in range(m):
            for j in range(n):
                data[i, j] = (data[i, j] - mu2[i]) / sigma2[i]
        
        return data
    
    def forward(self, x):
        """
        In the forward function we accept a Tensor of input data and we must return
        a Tensor of output data. We can use Modules defined in the constructor as
        well as arbitrary operators on Tensors.
        """
        h_relu1 = self.linear1(x)
        h_relu1 = self.renorm1(h_relu1)
        h_relu1 = torch.relu(h_relu1)
        h_relu2 = self.linear2(h_relu1)
        h_relu2 = self.renorm2(h_relu2)
        h_relu2 = torch.relu(h_relu2)
        h_relu3 = self.linear3(h_relu2)
        h_relu3 = self.renorm3(h_relu3)
        h_relu3 = torch.relu(h_relu3)
        h_relu4 = self.linear4(h_relu3)
        h_relu4 = self.renorm4(h_relu4)
        y_pred = self.linear5(h_relu4)
        return y_pred
    
    
class TwoLayerNet3(torch.nn.Module):
    torch.set_default_tensor_type(torch.DoubleTensor)
    def __init__(self, D_in, H, D_out):
        """
        In the constructor we instantiate two nn.Linear modules and assign them as
        member variables.
        """
        super(TwoLayerNet3, self).__init__()
        self.linear1 = torch.nn.Linear(D_in, H)
        self.renorm1 = torch.nn.BatchNorm1d(H)
        self.linear2 = torch.nn.Linear(H, H)
        self.renorm2 = torch.nn.BatchNorm1d(H)
        self.linear3 = torch.nn.Linear(H, H)
        self.renorm3 = torch.nn.BatchNorm1d(H)
        self.linear4 = torch.nn.Linear(H, D_out)
        
    
    def normalize(self, data):
        m = len(data)
        n = len(data[0])
        sigma2 = [0 for i in range(n)]
        mu2 = [0 for j in range(n)]
        for i in range(n):
            for j in range(m):
                mu2[i] = mu2[i] + data[j, i]
            
            mu2[i] = mu2[i] / m
        
        for i in range(n):
            for j in range(m):
                sigma2[i] += (data[j, i] - mu2[i])**2
            
            sigma2[i] = sqrt(sigma2[i]/m)
        
        for i in range(m):
            for j in range(n):
                data[i, j] = (data[i, j] - mu2[i]) / sigma2[i]
        
        return data
    
    def forward(self, x):
        """
        In the forward function we accept a Tensor of input data and we must return
        a Tensor of output data. We can use Modules defined in the constructor as
        well as arbitrary operators on Tensors.
        """
        h_relu1 = self.linear1(x)
        h_relu1 = self.renorm1(h_relu1)
        h_relu1 = torch.relu(h_relu1)
        h_relu2 = self.linear2(h_relu1)
        h_relu2 = self.renorm2(h_relu2)
        h_relu2 = torch.relu(h_relu2)
        h_relu3 = self.linear3(h_relu2)
        h_relu3 = self.renorm3(h_relu3)
        y_pred = self.linear4(h_relu3)
        return y_pred


class TwoLayerNet3(torch.nn.Module):
    torch.set_default_tensor_type(torch.DoubleTensor)
    def __init__(self, D_in, H, D_out):
        """
        In the constructor we instantiate two nn.Linear modules and assign them as
        member variables.
        """
        super(TwoLayerNet3, self).__init__()
        self.linear1 = torch.nn.Linear(D_in, H)
        self.renorm1 = torch.nn.BatchNorm1d(H)
        self.linear2 = torch.nn.Linear(H, H)
        self.renorm2 = torch.nn.BatchNorm1d(H)
        self.linear3 = torch.nn.Linear(H, H)
        self.renorm3 = torch.nn.BatchNorm1d(H)
        self.linear4 = torch.nn.Linear(H, D_out)
        #self.weight = torch.autograd.Variable(torch.tensor([0.]), requires_grad=True)
        
    
    def normalize(self, data):
        m = len(data)
        n = len(data[0])
        sigma2 = [0 for i in range(n)]
        mu2 = [0 for j in range(n)]
        for i in range(n):
            for j in range(m):
                mu2[i] = mu2[i] + data[j, i]
            
            mu2[i] = mu2[i] / m
        
        for i in range(n):
            for j in range(m):
                sigma2[i] += (data[j, i] - mu2[i])**2
            
            sigma2[i] = sqrt(sigma2[i]/m)
        
        for i in range(m):
            for j in range(n):
                data[i, j] = (data[i, j] - mu2[i]) / sigma2[i]
        
        return data
    
    def forward(self, x):
        """
        In the forward function we accept a Tensor of input data and we must return
        a Tensor of output data. We can use Modules defined in the constructor as
        well as arbitrary operators on Tensors.
        """
        h_relu1 = self.linear1(x)
        h_relu1 = self.renorm1(h_relu1)
        h_relu1 = torch.relu(h_relu1)
        h_relu2 = self.linear2(h_relu1)
        h_relu2 = self.renorm2(h_relu2)
        h_relu2 = torch.relu(h_relu2)
        h_relu3 = self.linear3(h_relu2)
        h_relu3 = self.renorm3(h_relu3)
        y_pred = self.linear4(h_relu3)
        return y_pred


class TwoLayerNet4(torch.nn.Module):
    torch.set_default_tensor_type(torch.DoubleTensor)
    def __init__(self, D_in, H, D_out):
        """
        In the constructor we instantiate two nn.Linear modules and assign them as
        member variables.
        """
        super(TwoLayerNet4, self).__init__()
        self.linear1 = torch.nn.Linear(D_in, H)
        self.renorm1 = torch.nn.BatchNorm1d(H)
        self.linear2 = torch.nn.Linear(H, H)
        self.renorm2 = torch.nn.BatchNorm1d(H)
        self.linear3 = torch.nn.Linear(H, H)
        self.renorm3 = torch.nn.BatchNorm1d(H)
        self.linear4 = torch.nn.Linear(H, D_out)
        #self.weight = torch.autograd.Variable(torch.tensor([0.]), requires_grad=True)
        
    
    def normalize(self, data):
        m = len(data)
        n = len(data[0])
        sigma2 = [0 for i in range(n)]
        mu2 = [0 for j in range(n)]
        for i in range(n):
            for j in range(m):
                mu2[i] = mu2[i] + data[j, i]
            
            mu2[i] = mu2[i] / m
        
        for i in range(n):
            for j in range(m):
                sigma2[i] += (data[j, i] - mu2[i])**2
            
            sigma2[i] = sqrt(sigma2[i]/m)
        
        for i in range(m):
            for j in range(n):
                data[i, j] = (data[i, j] - mu2[i]) / sigma2[i]
        
        return data
    
    def forward(self, x):
        """
        In the forward function we accept a Tensor of input data and we must return
        a Tensor of output data. We can use Modules defined in the constructor as
        well as arbitrary operators on Tensors.
        """
        h_relu1 = self.linear1(x)
        h_relu1 = self.renorm1(h_relu1)
        h_relu1 = torch.relu(h_relu1)
        h_relu2 = self.linear2(h_relu1)
        h_relu2 = self.renorm2(h_relu2)
        h_relu2 = torch.relu(h_relu2)
        h_relu3 = self.linear3(h_relu2)
        h_relu3 = self.renorm3(h_relu3)
        y_pred = self.linear4(h_relu3)
        return y_pred


class TwoLayerNet5(torch.nn.Module):
    torch.set_default_tensor_type(torch.DoubleTensor)
    def __init__(self, D_in, H, D_out):
        """
        In the constructor we instantiate two nn.Linear modules and assign them as
        member variables.
        """
        super(TwoLayerNet5, self).__init__()
        self.linear1 = torch.nn.Linear(D_in, H)
        self.linear2 = torch.nn.Linear(H, H)
        self.linear3 = torch.nn.Linear(H, H)
        self.linear4 = torch.nn.Linear(H, D_out)
        #self.weight = torch.autograd.Variable(torch.tensor([0.]), requires_grad=True)
        
    
    def normalize(self, data):
        m = len(data)
        n = len(data[0])
        sigma2 = [0 for i in range(n)]
        mu2 = [0 for j in range(n)]
        for i in range(n):
            for j in range(m):
                mu2[i] = mu2[i] + data[j, i]
            
            mu2[i] = mu2[i] / m
        
        for i in range(n):
            for j in range(m):
                sigma2[i] += (data[j, i] - mu2[i])**2
            
            sigma2[i] = sqrt(sigma2[i]/m)
        
        for i in range(m):
            for j in range(n):
                data[i, j] = (data[i, j] - mu2[i]) / sigma2[i]
        
        return data
    
    def forward(self, x):
        """
        In the forward function we accept a Tensor of input data and we must return
        a Tensor of output data. We can use Modules defined in the constructor as
        well as arbitrary operators on Tensors.
        """
        h_relu1 = self.linear1(x)
        h_relu1 = torch.relu(h_relu1)
        h_relu2 = self.linear2(h_relu1)
        h_relu2 = torch.relu(h_relu2)
        h_relu3 = self.linear3(h_relu2)
        y_pred = self.linear4(h_relu3)
        return y_pred