# -*- coding: utf-8 -*-
"""
Created on Sun Dec 26 08:07:55 2021

@author: 15000
"""

from potential import noise_test
import pandas as pd

Lambda_list = [0]
std_list = [0]
for i in range(7):
    std_list.append(10**(-4+0.5*i))  
    Lambda_list.append(10**(-4+0.5*i))


SSE_list = [[0 for i in range(8)] for j in range(8)]
SSE_loss_list = [[0 for i in range(8)] for j in range(8)]
F_norm_list = [[0 for i in range(8)] for j in range(8)]
F_error_list = [[0 for i in range(8)] for j in range(8)]
for i in range(8):
    for j in range(8):
        SSE, SSE_loss, F_norm, F_error = noise_test(std_list[i], Lambda_list[j])
        SSE_list[i][j] = SSE
        SSE_loss_list[i][j] = SSE_loss
        F_norm_list[i][j] = F_norm
        F_error_list[i][j] = F_error
    
    print(i, j)

testing = pd.DataFrame(data = SSE_list)
testing.to_csv('SSE_list.csv', header=False, index = False)
testing = pd.DataFrame(data = SSE_loss_list)
testing.to_csv('SSE_loss_list.csv', header=False, index = False)
testing = pd.DataFrame(data = F_norm_list)
testing.to_csv('F_norm_list.csv', header=False, index = False)
testing = pd.DataFrame(data = F_error_list)
testing.to_csv('F_list.csv', header=False, index = False)