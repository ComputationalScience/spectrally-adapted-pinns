# -*- coding: utf-8 -*-
"""
Created on Fri Oct  8 06:39:37 2021

@author: 15000
"""
import sys
sys.path.insert(0, '../../utilities/')

from Network_Model8 import TwoLayerNet20
from math import sin, sqrt, acos, cos, pi
from scipy import exp
import random
import numpy as np
import torch
from torch.autograd import Variable
import pdb
import csv
import pandas as pd
import copy
import time
from Hermite import Init, Hermite, discrete_beta, inverse, frequency_indicator

def noise_test(std, penal):
    xi = 0.3
    k = 1
    beta = 0.8
    m = 200
    n = 17
    torch.set_default_tensor_type(torch.DoubleTensor)
    
    # import the Lagendre collocation c_j for time integration, and the matrix of coefficients
    # in a high-order RK scheme A = (a)_{ij} as well as the coefficients b_i
    C_record = csv.reader(open('C.csv'))
    B_record = csv.reader(open('B.csv'))
    A_record = csv.reader(open('A.csv'))
    C = []
    B = []
    
    for i in C_record:
        C.append(float(i[0]))
    
    for i in B_record:
        B.append(float(i[0]))
    
    A = [[] for i in range(len(C)+1)]
    interm = len(C) + 1
    ss = 0
    for i in A_record:
        for j in i:
            A[ss].append(float(j))
        ss += 1
    
        
    # the analytic solution
    def target_fun(x, t=0):
        global xi, k
        y = sin(x)/sqrt(t+1) * exp(-x**2/4/(t+1))
        return y
    
    # contruct the allocation nodes, weights, and norms
    X_x, Wx, Gamma_x = Init(n, beta)
    Weight_mat = [[0 for i in range(n)] for j in range(n)]
    for i in range(n):
        Weight_mat[i][i] = sqrt(Wx[i])
    
    Weight_mat = torch.tensor(Weight_mat)
    
    # the reference solution the heat source f(x, t)
    def f(x, t):
        y = [0 for i in range(n)]
        for i in range(n):
            y[i] = ((x[i] * cos(x[i]) + (t+1) *sin(x[i])) * (t+1)**(-3/2) * exp(-x[i]**2/4/(t+1)))*(1) -  (x[i]**2/4/(t+1)**2 - 1/2/(t+1)) * sin(x[i]) / sqrt(t+1) * exp(-x[i]**2/4/(t+1))
        
        return y
    
    def F(n, t, dt, times, beta):
        nonlocal X_x, Wx, Gamma_x
        result = []
        for i in range(len(times)-1):
            y = f(X_x, times[i])
            result.append(y)
        
        result = torch.tensor(result, dtype=torch.float64)
        result = result.transpose(0, 1)
        return result
    
    def F1(n, t, dt):
        nonlocal X_x, Wx, Gamma_x
        times = [t]
        result = []
        for i in range(1):
            y = f(X_x, times[i])
            y = discrete_beta(y, X_x, Wx, n, Gamma_x, beta)
            result.append(y)
        
        result = torch.tensor(result, dtype=torch.float64)
        result = result.transpose(0, 1)
        return result
    
    
    B_matrix = torch.tensor(A, dtype=torch.float64)
    B_matrix = B_matrix.transpose(0, 1)
    B_matrix_new = copy.deepcopy(B_matrix)
    for i in range(interm - 1):
        for j in range(interm - 1):
            B_matrix_new[i, j] = B_matrix_new[i, j] - B_matrix[i, interm-1]
    
    
    Dif_mat = [[0 for i in range(n)] for j in range(n)]
    
    
    for i in range(n):
        if i - 1 > 0:
            Dif_mat[i][i-2] = -beta**2 *sqrt((i) * (i-1)) / 2
        
        Dif_mat[i][i] = (i+1/2) * beta**2
        if i + 2 < n:
            Dif_mat[i][i+2] = -beta**2 *sqrt((i+1) * (i+2)) / 2
        
                
    Dif_mat = torch.tensor(Dif_mat, dtype=torch.float64)
    X = []
    
    t = 0
    dt = 0.2
    times = [(t + dt * C[i]) for i in range(interm-1)]
    F_test = F1(n, t, dt).transpose(0, 1)[0]
    X = torch.tensor([[times[i]] for i in range(interm-1)], dtype=torch.float64)
    k = 1
    
    Matrix1 = [[Hermite(X_x[j], i, beta) for j in range(n)] for i in range(n)]
    Matrix1 = torch.tensor(Matrix1, dtype=torch.float64)
    Matrix1 = Matrix1.transpose(0, 1)
    error = []
    zeros = [0. for i in range(n)]
    zeros_copy = [0. for i in range(n)]
    zeros = torch.tensor([zeros for i in range((interm-1)*3)], dtype = torch.float64)
    zeros0 = torch.tensor([zeros_copy for i in range((interm-1)*2)], dtype = torch.float64)
    t_target = 0.2
        
    u = [target_fun(X_x[i], t) for i in range(n)]
    Weight_mat = [[0 for i in range(n)] for j in range(n)]
    for i in range(n):
        Weight_mat[i][i] = sqrt(Wx[i])
    
    Weight_mat = torch.tensor(Weight_mat)
    u = torch.tensor([u for i in range(interm)])
    # set the parameters in the model, the strength of noise in data is 1e-3, and the 
    # coefficient for the penalty term is 1e-1
    freq_ind = None
    q = 0.95
    nu = 1 / q
    beta_under = 0.2
    beta_over = 5
    #penal = 1e-1
    #std = 1e-3
    data_storage = csv.reader(open('data_std10_1.csv'))
    data_store = []
    for i in data_storage:
        data_storing = []
        for j in i:
            data_storing.append(float(j))
        
        data_store.append(copy.deepcopy(data_storing))
    
    ttime = time.time()
    error_list = []
    lam_list = []
    resulting = []
    while t < t_target:
        # calculate the initial frequency indicator and exterior-error indicator when t = 0
        if freq_ind is None:
            lst = [target_fun(X_x[i], t) + random.normalvariate(0, std) for i in range(n)]
            Y_real_inv = discrete_beta(lst, X_x, Wx, n, Gamma_x, beta)
            u = []
            for i1 in range(interm-1):
                u.append(copy.deepcopy(lst))
            
            u = torch.tensor(u)
            freq_ind = frequency_indicator(Y_real_inv, n, Gamma_x)
            freq_thres = freq_ind
    
        # calculate the new allocation points and weights
        X_x, Wx, Gamma_x = Init(n, beta)
        Matrix1 = [[Hermite(X_x[j], i, beta) for j in range(n)] for i in range(n)]
        Matrix1 = torch.tensor(Matrix1, dtype=torch.float64)
        Matrix1 = Matrix1.transpose(0, 1)
        u_next = [target_fun(X_x[i], t+dt)+random.normalvariate(0, std) for i in range(n)]
        u_next_cop = copy.deepcopy(u_next)
        u_next = torch.tensor([u_next for i in range(interm-1)])
        # the matrix needed for computing the weak form of the spatial derivative u_xx
        Dif_mat = [[0 for i in range(n)] for j in range(n)]
        for i in range(n):
            if i - 1 > 0:
                Dif_mat[i][i-2] = -beta**2 *sqrt((i) * (i-1)) / 2
        
            Dif_mat[i][i] = (i+1/2) * beta**2
            if i + 2 < n:
                Dif_mat[i][i+2] = -beta**2 *sqrt((i+1) * (i+2)) / 2    
            
        Dif_mat = torch.tensor(Dif_mat, dtype=torch.float64)
        
        times = [(t + dt * C[i]) for i in range(interm-1)]
        
        times.append(t + dt)
        model = TwoLayerNet20(1, 100, n)
        lst_result = []
        for i in range(interm-1):
            lst_result.append(copy.deepcopy(data_store[i+1]))
        
        u = torch.tensor([copy.deepcopy(data_store[0]) for i in range(interm-1)])
        u_next = torch.tensor([copy.deepcopy(data_store[interm]) for i in range(interm-1)])
        Y_pred = torch.tensor(lst_result)
        criterion = torch.nn.MSELoss(reduction='sum')
        optimizer = torch.optim.SGD(model.parameters(), lr=0.005)
        optimizer.zero_grad()
        Z0 = (torch.matmul(Dif_mat, Y_pred.transpose(0, 1))) 
        Z1 = torch.tensor([[0. for i in range(interm-1)] for j5 in range(n)])
        for i5 in range(n):
            for j5 in range(interm-1):
                Z1[i5, j5] = Z0[i5, j5] 
        
        # Here, the solution (with noise) at any time is given, and one wishes to 
        # infer the heat solution as a function of f = F(t, \Theta), which is modeled
        # by the neural network
        F_inter = model(X)
        Z2 = Z1 - F_inter.transpose(0, 1)
        B_matrix = B_matrix[0:interm-1, 0:interm-1]
        B_matrix_new = B_matrix_new[0:interm-1, 0:interm-1]
        # the MSE has two terms plus a penalty term
        error0 = torch.matmul(torch.matmul(Matrix1, Y_pred.transpose(0, 1) + dt * torch.matmul(Z2, B_matrix)).transpose(0, 1) - u, Weight_mat)
        error1 = torch.matmul(torch.matmul(Matrix1, Y_pred.transpose(0, 1) + dt * torch.matmul(Z2, B_matrix_new)).transpose(0, 1) - u_next, Weight_mat)
        error = torch.tensor([[0. for i4 in range(n)] for j4 in range((interm-1)*3)])
        for i4 in range(n):
            for j4 in range((interm-1)*3):
                if j4 < interm - 1:
                    error[j4, i4] = error0[j4, i4]
                elif j4 < 2*(interm-1):
                    error[j4, i4] = error1[j4-interm+1, i4]
                else:
                    error[j4, i4] = penal * abs(F_inter[j4-2*(interm-1), i4])
    
        loss = criterion(zeros, error)
        loss.backward()
        optimizer.step()
        optimizer.zero_grad()
        # training
        while loss.item() > 1e-20 and k < 60000:
            k += 1
            if k % 10000 == 0 and k > 0:
                F_model = model(X)
                F_ref = F(n, t, dt, times, beta)
                print(k, loss.item(), sum(((torch.matmul((torch.matmul(Matrix1, F_model.transpose(0, 1)) - F_ref).transpose(0, 1), Weight_mat))**2).transpose(0, 1)))
            
            Z0 = (torch.matmul(Dif_mat, Y_pred.transpose(0, 1))) 
            Z1 = torch.tensor([[0. for i in range(interm-1)] for j5 in range(n)])
            for i5 in range(n):
                for j5 in range(interm-1):
                    Z1[i5, j5] = Z0[i5, j5] 
            
            F_inter = model(X)
            Z2 = Z1 - F_inter.transpose(0, 1)
            error0 = torch.matmul(torch.matmul(Matrix1, Y_pred.transpose(0, 1) + dt * torch.matmul(Z2, B_matrix)).transpose(0, 1) - u, Weight_mat)
            error1 = torch.matmul(torch.matmul(Matrix1, Y_pred.transpose(0, 1) + dt * torch.matmul(Z2, B_matrix_new)).transpose(0, 1) - u_next, Weight_mat)
            error = torch.tensor([[0. for i4 in range(n)] for j4 in range((interm-1)*3)])
            error000 = torch.tensor([[0. for i4 in range(n)] for j4 in range((interm-1)*2)])
            for i4 in range(n):
                for j4 in range((interm-1)*3):
                    if j4 < interm - 1:
                        error[j4, i4] = error0[j4, i4]
                        error000[j4, i4] = error0[j4, i4]
                    elif j4 < 2*(interm-1):
                        error[j4, i4] = error1[j4-interm+1, i4]
                        error000[j4, i4] = error1[j4-interm+1, i4]
                    else:
                        error[j4, i4] = penal * abs(F_inter[j4-2*(interm-1), i4])
    
    
            loss = criterion(zeros, error)
            loss0 = criterion(zeros0, error000)
            loss.backward()
            optimizer.step()
            optimizer.zero_grad()
    
        F_model = model(X)
        
        for i in range(interm-1):
            resulting.append([float(F_model[i, j]) for j in range(n)])
        
        F_ref = F(n, t, dt, times, beta)
        result = sum(((torch.matmul((torch.matmul(Matrix1, F_model.transpose(0, 1)) - F_ref).transpose(0, 1), Weight_mat))**2).transpose(0, 1))
        result_norm = sum(((torch.matmul((torch.matmul(Matrix1, F_model.transpose(0, 1))).transpose(0, 1), Weight_mat))**2).transpose(0, 1))
        errorss = sum([float(result[i] * B[i]) for i in range(interm-1)])
        errorss0 = sum([float(result_norm[i] * B[i]) for i in range(interm-1)])
        error_list.append(errorss)
        res = model(X)
        k = 0
        t += dt
    
    print(result, '*')
    return sqrt(loss0.item()), sqrt(loss.item()), sqrt(dt * errorss), sqrt(dt * errorss0)

