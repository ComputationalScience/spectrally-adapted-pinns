function multiplot
subplot('Position',[0.1 0.14 0.22 0.66])
t = 0:0.01:15;
E1 = csvread('scaled_error5_1com.csv');
E2 = csvread('unscaled_error5_1com.csv');
E3 = csvread('scaled_error15.csv');
E1 = E1(1:1501);
E2 = E2(1:1501);
E3 = E3(1:1501);

h1 = plot(X_node, U1, 'color', 'b', 'Linewidth', 1.5 );
hold on;
h2 = plot(X_node, U2,   'color', 'r', 'Linewidth', 1.5);
hold on;
h3 = plot(X_node, U3, 'color', '[0 0.5 0.5]', 'Linewidth', 1.5 );
hold on;
h4 = plot(X_node, U4,'color', 'k', 'Linewidth', 1.5);
hold on;

plot(X_node(1:200:4001), U1(1:200:4001), '*', 'color', 'b', 'Linewidth', 1.5);
hold on;

plot(X_node(1:200:4001), U2(1:200:4001), 's', 'color', 'r', 'Linewidth', 1.5 );
hold on;

plot(X_node(1:200:4001), U3(1:200:4001), 'o', 'color', [0 0.5 0.5], 'Linewidth', 1.5);
hold on;

plot(X_node(1:200:4001), U4(1:200:4001), '<', 'color', 'k', 'Linewidth', 1.5);
hold on;

set(gca, 'xtick', [0,1,2,3,4,5,6,7,8])
set(gca, 'xticklabel', { '0', '', '2','', '4',  '','6', '', '8'})
[H, h] = legend([h1, h2, h3, h4], '~$t=0$', '~$t=5$', '~$t=10$', '~$t=15$');%, 'Orientation', 'horizontal');
H.Box = 'off';
set(gca, 'Fontsize', 15)
xlabel('$$x$$','fontsize', 20, 'interp', 'latex');
ylabel('$$u(x, t)$$','fontsize', 20, 'interp', 'latex')
disp(h)
set(h(6), 'marker', '*')
set(h(8), 'marker', 's')
set(h(10), 'marker', 'o')
set(h(12), 'marker', '<')
set(h(1:4), 'fontsize', 20, 'interp', 'latex')
axis([a b 0 6])
set(H, 'Position', [.223, .77, .1, .18])
%set(gca, 'Position',[.5 .1 .2 .2], 'Fontsize', 20);
%hold on;

title('(a) $u(x, t)$ at $t=0,5, 10,15$', 'fontsize', 20, 'interp', 'latex')

subplot('Position',[0.405 0.14 0.22 0.66])
t = 0:0.01:15;
E1 = csvread('scaled_error5_1com.csv');
E2 = csvread('unscaled_error5_1com.csv');
E3 = csvread('scaled_error15.csv');
E1 = E1(1:1501);
E2 = E2(1:1501);
E3 = E3(1:1501);
h1 = semilogy(t, E1, 'color', 'k', 'Linewidth', 1.5 );
hold on;
h2 = semilogy(t, E2, 'color', 'b', 'Linewidth', 1.5 );
hold on;
h3 = semilogy(t, E3, 'color', 'r', 'Linewidth', 1.5);
hold on;

semilogy(t(1:100:1501), E1(1:100:1501), '*', 'color', 'k', 'Linewidth', 1.5);
hold on;

semilogy(t(1:100:1501), E2(1:100:1501), 's', 'color', 'b', 'Linewidth', 1.5 );
hold on;

semilogy(t(1:100:1501), E3(1:100:1501), '<', 'color', 'r', 'Linewidth', 1.5 );
hold on;

set(gca, 'Fontsize', 15)
xlabel('time','fontsize', 20, 'interp', 'latex');
ylabel('error','fontsize', 20, 'interp', 'latex')
[H, h] = legend([h1, h2, h3],  ' ~scaled \& unmoved', ' ~unscaled \& unmoved','~unscaled \& moved');
H.Box = 'off';
set(h(5), 'marker', '*')
set(h(7), 'marker', 's')
set(h(9), 'marker', '<')
set(h(1:3), 'fontsize', 20, 'interp', 'latex')
%set(h, 'fontsize', 20, 'interp', 'latex');
set(H, 'Position', [.435, .85, .1, .1])
%set(gca, 'Position',[.175 .20 .7 .825], 'Fontsize', 20);
set(gca, 'yminortick', 'off')
set(gca, 'ytick', [1e-13, 1e-12, 1e-11,1e-10, 1e-9, 1e-8, 1e-7, 1e-6, 1e-5, 1e-4, 1e-3, 1e-2, 1e-1, 1])
set(gca, 'yticklabel', {'', ''  '', '10^{-10}','', '',  '','', '10^{-5}', '', '', '', '', '1'})
set(gca, 'xtick', [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15])
set(gca, 'xticklabel', { '0', '', '','', '',  '5','', '', '', '', '10', '', '', '','','15'})
axis([0 15 1e-13 10])
title('(b) Error in $\Lambda_{\rm e}$ $\textit{vs}$ time', 'fontsize', 20, 'interp', 'latex')



subplot('Position',[0.72 0.14 0.22 0.66])
t = 11:2:51;
E2 = csvread('Error_diff_N55.csv');
E1 = csvread('Error_undiff_N55.csv');
E1 = E1(1:21);
E2 = E2(1:21);
t = t-1;
h1 = semilogy(t, E1, 'color', 'b', 'Linewidth', 1.5 );
hold on;
h2 = semilogy(t, E2, 'color', 'r', 'Linewidth', 1.5 );
hold on;

semilogy(t(1:4:21), E1(1:4:21), '*', 'color', 'b', 'Linewidth', 1.5);
hold on;

semilogy(t(1:4:21), E2(1:4:21), 's', 'color', 'r', 'Linewidth', 1.5 );
hold on;


set(gca, 'Fontsize', 15)
xlabel('$N$','fontsize', 20, 'interp', 'latex');
ylabel('error','fontsize', 20, 'interp', 'latex')
[H, h] = legend([h1, h2],'~unmoved', '~moved');
H.Box = 'off';
set(h(4), 'marker', '*')
set(h(6), 'marker', 's')
set(h(1:2), 'fontsize', 20, 'interp', 'latex')
%set(h, 'fontsize', 20, 'interp', 'latex');
set(H, 'Position', [.73, .63, .1, .1])
%set(gca, 'Position',[.175 .20 .7 .825], 'Fontsize', 20);
set(gca, 'yminortick', 'off')
set(gca, 'ytick', [1e-13, 1e-12, 1e-11,1e-10, 1e-9, 1e-8, 1e-7, 1e-6, 1e-5, 1e-4, 1e-3, 1e-2, 1e-1, 1, 10])
set(gca, 'yticklabel', { '', '', '', '10^{-10}','', '',  '','', '10^{-5}', '', '', '', '', '1', ''})
axis([10 50 1e-13 1e2])
title('(c) Error in $\Lambda_{\rm e}$ $\textit{vs}$ $N$', 'fontsize', 20, 'interp', 'latex')
end