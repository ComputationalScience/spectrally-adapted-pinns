# -*- coding: utf-8 -*-


# -*- coding: utf-8 -*-
"""
Created on Wed Sep  1 13:55:34 2021

@author: 15000
"""

# -*- coding: utf-8 -*-
"""
Created on Wed Sep  1 05:50:31 2021

@author: 15000
"""
import sys
sys.path.insert(0, '../../utilities/')

import pandas as pd
from Network_Model import TwoLayerNet2
from math import sin, sqrt, acos, cos, pi, exp
import random
import numpy as np
import torch
from torch.autograd import Variable
import pdb
import csv
import time      
import copy
from Laguerre import Init, Laguerre, discrete_beta, inverse, frequency_indicator

xi = 0.3
k = 1
beta = 2
n = 11
torch.set_default_tensor_type(torch.DoubleTensor)

# import the Lagendre collocation c_j for time integration, and the matrix of coefficients
# in a high-order RK scheme A = (a)_{ij} as well as the coefficients b_i
C_record = csv.reader(open('C.csv'))
B_record = csv.reader(open('B.csv'))
A_record = csv.reader(open('A.csv'))
C = []
B = []

for i in C_record:
    C.append(float(i[0]))

for i in B_record:
    B.append(float(i[0]))

A = [[] for i in range(len(C)+1)]
interm = len(C) + 1
ss = 0
for i in A_record:
    for j in i:
        A[ss].append(float(j))
        
    ss += 1

B_matrix = torch.tensor(A, dtype=torch.float64)
B_matrix = B_matrix.transpose(0, 1)


    
# the analytic solution
def target_fun(x, t=0):
    global xi, k
    y = exp(-x / (t+1))
    return y

X_x, Wx, Gamma_x = Init(n, beta)
Weight_mat = [[0 for i in range(n)] for j in range(n)]
for i in range(n):
    Weight_mat[i][i] = sqrt(Wx[i])

Weight_mat = torch.tensor(Weight_mat)

# the matrix for calculating the spatial derivative u_x
Dif_mat = [[0 for i in range(n)] for j in range(n)]
for i in range(n):
    if i - 1 > 0:
        Dif_mat[i][i-2] = -beta**2 *sqrt((i) * (i-1)) / 2
    
    Dif_mat[i][i] = (i+1/2) * beta**2
    if i + 2 < n:
        Dif_mat[i][i+2] = -beta**2 *sqrt((i+1) * (i+2)) / 2
        
Dif_mat = torch.tensor(Dif_mat, dtype=torch.float64)

X = []
Y = []
t = 0
dt = 0.05
times = [(t + dt * C[i]) for i in range(interm-1)]
times.append(t + dt)
X_ori = X
X = torch.tensor([[times[i]] for i in range(interm)], dtype=torch.float64)
k = 1

Matrix1 = [[Laguerre(X_x[j], i, beta) for j in range(n)] for i in range(n)]
Matrix1 = torch.tensor(Matrix1, dtype=torch.float64)
Matrix1 = Matrix1.transpose(0, 1)

error = []
zeros = [0 for i in range(n+1)]
zeros = torch.tensor([zeros for i in range(interm)], dtype = torch.float64)
t_target = 4
u = [target_fun(X_x[i], t) for i in range(n)]
Weight_mat = [[0 for i in range(n)] for j in range(n)]
for i in range(n):
    Weight_mat[i][i] = sqrt(Wx[i])

Weight_mat = torch.tensor(Weight_mat)

# set the parameters
u = torch.tensor([u for i in range(interm)])
freq_ind = None
thres = 1.5
thres0 = 1.5
update = 1.3
q = 1#0.95
nu = 1 / q
beta_under = 0.05
beta_over = 5
error = []
beta_list = []
t_time = time.time()
N_list = []
freq_list = []
while t < t_target:
    # calculate the initial frequency inidcator when t = 0
    if freq_ind is None:
        Y_real_inv = discrete_beta([target_fun(X_x[i], t) for i in range(n)], X_x, Wx, n, Gamma_x, beta)
        freq_ind = frequency_indicator(Y_real_inv, n, Gamma_x)
        freq_thres_p = freq_ind
        freq_thres = freq_ind 
    else:
        u = []
        lst = []
        coeffs = [float(Y_pred[interm-1, i2]) for i2 in range(n)]
        u_inv = coeffs
        for i2 in range(n):
            lst.append(inverse(X_x[i2], coeffs, beta))
        
        for i1 in range(interm):
            u.append(copy.deepcopy(lst))
            
        u = torch.tensor(u)
        
        Y_real_inv = [float(Y_pred[interm-1, i2]) for i2 in range(n)]
        freq_ind = frequency_indicator(Y_real_inv, n, Gamma_x)
        if freq_ind > nu *freq_thres:
            
            beta_hat = beta *q
            X_x1, Wx1, Gamma_x1 = Init(n, beta_hat)
            lst = [inverse(X_x1[i2], Y_real_inv, beta) for i2 in range(n)]
            u_inv_new = discrete_beta(lst, X_x1, Wx1, n, Gamma_x1, beta_hat)
            fred_ind_new = frequency_indicator(u_inv_new, n, Gamma_x1)
            # the scaling step to decrease the scaling factor
            while fred_ind_new < freq_ind and beta_hat > beta_under:
                if q == 1:
                    break
                
                freq_thres = fred_ind_new
                freq_ind = fred_ind_new
                U_new = [inverse(X_x1[i2], u_inv_new, beta_hat) for i2 in range(n)]
                u_inv = u_inv_new
                u = torch.tensor([U_new for i in range(interm)])
                beta = beta_hat
                X_x = X_x1
                Wx = Wx1
                Gamma_x = Gamma_x1
                beta_hat *= q
                X_x1, Wx1, Gamma_x1 = Init(n, beta_hat)
                lst = [inverse(X_x1[i2], u_inv_new, beta) for i2 in range(n)]
                u_inv_new = discrete_beta(lst, X_x1, Wx1, n, Gamma_x1, beta_hat)
                fred_ind_new = frequency_indicator(u_inv_new, n, Gamma_x1)
        elif freq_ind < freq_thres:
            beta_hat = beta /q
            X_x1, Wx1, Gamma_x1 = Init(n, beta_hat)
            lst = [inverse(X_x1[i2], Y_real_inv, beta) for i2 in range(n)]
            u_inv_new = discrete_beta(lst, X_x1, Wx1, n, Gamma_x1, beta_hat)
            fred_ind_new = frequency_indicator(u_inv_new, n, Gamma_x1)
            # the scaling step to increase the scaling factor
            while fred_ind_new < freq_ind and beta_hat < beta_over:
                if q == 1:
                    break
                
                freq_thres = fred_ind_new
                freq_ind = fred_ind_new
                U_new = [inverse(X_x1[i2], u_inv_new, beta_hat) for i2 in range(n)]
                u = torch.tensor([U_new for i in range(interm)])
                u_inv = u_inv_new
                beta = beta_hat
                X_x = X_x1
                Wx = Wx1
                Gamma_x = Gamma_x1
                beta_hat /= q
                X_x1, Wx1, Gamma_x1 = Init(n, beta_hat)
                lst = [inverse(X_x1[i2], u_inv_new, beta) for i2 in range(n)]
                u_inv_new = discrete_beta(lst, X_x1, Wx1, n, Gamma_x1, beta_hat)
                fred_ind_new = frequency_indicator(u_inv_new, n, Gamma_x1)
        
        
        
    X_x, Wx, Gamma_x = Init(n, beta)
    Matrix1 = [[Laguerre(X_x[j], i, beta) for j in range(n)] for i in range(n)]
    Matrix1 = torch.tensor(Matrix1, dtype=torch.float64)
    Matrix1 = Matrix1.transpose(0, 1)
    Weight_mat = [[0 for i in range(n)] for j in range(n)]
    for i in range(n):
        Weight_mat[i][i] = sqrt(Wx[i])

    Weight_mat = torch.tensor(Weight_mat)
    if t == 0:
        u = [target_fun(X_x[i], t) for i in range(n)]
        Y_real = discrete_beta(u, X_x, Wx, n, Gamma_x, beta)
        u = torch.tensor([u for i in range(interm)])

    Y_real_inv = discrete_beta([target_fun(X_x[i], t+dt) for i in range(n)], X_x, Wx, n, Gamma_x, beta)
    Y_real_inv1 = copy.deepcopy(Y_real_inv)
    Y_real_inv2 = torch.tensor([Y_real_inv], dtype=torch.float64)
    
    # the matrix needed for computing the spatial derivative u_x
    Dif_mat = [[0 for i in range(n)] for j in range(n)]
    for i in range(n):
        if i - 1 > 0:
            Dif_mat[i][i-2] = -beta**2 *sqrt((i) * (i-1)) / 2
    
        Dif_mat[i][i] = (i+1/2) * beta**2
        if i + 2 < n:
            Dif_mat[i][i+2] = -beta**2 *sqrt((i+1) * (i+2)) / 2
      
    Dif_mat = torch.tensor(Dif_mat, dtype=torch.float64)
    times = [(t + dt * C[i]) for i in range(interm-1)]
    times.append(t + dt)
    zeros = [0 for i in range(n+1)]
    zeros = torch.tensor([zeros for i in range(interm)], dtype = torch.float64)
    
    # set up the neural network model and optimizer
    model = TwoLayerNet2(1, 100, n)
    Y_pred = model(X)
    criterion = torch.nn.MSELoss(reduction='sum')
    optimizer = torch.optim.SGD(model.parameters(), lr=0.001)
    optimizer.zero_grad()
    coef_mat = [[-(X_x[i3]) / (times[j3] + 1) for i3 in range(n)] for j3 in range(interm)]
    coef_mat = torch.tensor(coef_mat)
    coef_mat1 = [[-(X_x[i3]) / (t + 1) for i3 in range(n)] for j3 in range(interm)]
    coef_mat1 = torch.tensor(coef_mat)
    
    # given the coefficients (u_0, u_1,...,u_n-1) of u=\sum u_i * T_i(x), calculate its derivative
    # \partial_x u

    def diff_operator(Origin, n, X_x, beta, Matrix1):
        
        A = torch.tensor([[0. for i3 in range(n)] for j3 in range(n)])
        for i3 in range(n):
            for j3 in range(n):
                if i3 == j3:
                    A[i3, j3] = -1/2 * beta
                elif i3 < j3 :
                    A[i3, j3] = -1 *beta
        
        Origin = torch.matmul(Matrix1, torch.matmul(A, Origin.transpose(0, 1)))
        Origin = Origin.transpose(0, 1)
        return Origin
        
    
    Diff = coef_mat * diff_operator(Y_pred, n, X_x, beta, Matrix1)
    Diff = Diff.transpose(0, 1)
    criterion = torch.nn.MSELoss(reduction='sum')
    optimizer = torch.optim.SGD(model.parameters(), lr=0.001)
    
    Matrix_new = [[Laguerre(0, jj, beta) for jj in range(n)]]
    Matrix_new = torch.tensor(Matrix_new, dtype=torch.float64)
    Z3 = torch.matmul(Matrix_new, Y_pred.transpose(0, 1)) 
    Z1 = torch.matmul((torch.matmul(Matrix1, Y_pred.transpose(0, 1)) - dt * torch.matmul(Diff, B_matrix)).transpose(0, 1) - u, Weight_mat)
    Z2 = torch.tensor([[0 for ii in range(n+1)] for jj in range(interm)], dtype=torch.float64)
    for ii in range(n+1):
        for jj in range(interm):
            if ii < n:
                Z2[jj, ii] = Z1[jj, ii]
            else:
                # impose the boundary constraint when x=0 is not included in the collocation points
                Z2[jj, ii] = Z3[0, jj] - target_fun(0, t + X[jj])
                
    optimizer.zero_grad()
    loss = criterion(zeros,Z2)
    loss.backward()
    optimizer.step()
    optimizer.zero_grad()
    k = 0
    if t == 0:
        thress = 1e-20
    else:
        thress = max(1e-20, 1e-2*result**2)
    # training 
    while loss.item() > thress and k < 100000:
        k += 1
        Y_pred = model(X)
        Diff = coef_mat * diff_operator(Y_pred, n, X_x, beta, Matrix1)
        Diff = Diff.transpose(0, 1)
        
        Z3 = torch.matmul(Matrix_new, Y_pred.transpose(0, 1))
    
        Z1 = torch.matmul((torch.matmul(Matrix1, Y_pred.transpose(0, 1)) - dt * torch.matmul(Diff, B_matrix)).transpose(0, 1) - u, Weight_mat)
        Z2 = torch.tensor([[0 for ii in range(n+1)] for jj in range(interm)], dtype=torch.float64)
        for ii in range(n+1):
            for jj in range(interm):
                if ii < n:
                    Z2[jj, ii] = Z1[jj, ii]
                else:
                    Z2[jj, ii] = Z3[0, jj] - target_fun(0, t + X[jj])
        
        loss = criterion(zeros, Z2)
        if k % 10000 == 0 and k > 0:
            print(loss.item(), k)
            
        optimizer.zero_grad()
        loss.backward()
        optimizer.step()
        
    result = sqrt(sum((torch.matmul(torch.matmul(Matrix1, Y_real_inv2.transpose(0, 1)).transpose(0, 1)[0] - torch.matmul(Matrix1, Y_pred.transpose(0, 1)).transpose(0, 1)[interm-1], Weight_mat))**2))
    error.append(result)
    freq_list.append(freq_ind)
    beta_list.append(beta)
    N_list.append(n)
    print(beta, freq_ind, result, t, n)
    k = 0
    t += dt
    
print(time.time() - t_time)
testing = pd.DataFrame(data = error)
testing.to_csv('error_spectral_nonadaptive1.csv', header=False, index = False)
testing = pd.DataFrame(data = beta_list  )
testing.to_csv('beta_spectraL_nonadaptive1.csv', header=False, index = False)
testing = pd.DataFrame(data = freq_list)
testing.to_csv('freq_spectral_nonadaptive1.csv', header=False, index = False)
print(k)
