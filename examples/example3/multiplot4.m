function multiplot4
subplot('Position', [0.06 0.14 0.25 0.76])
t = 0.05:0.05:4;
E2 = csvread('error_spectral_bounded_adaptive1.csv');
E1 = csvread('error_spectral_bounded_nonadaptive1.csv');

%E1 = E1(1:5001);
%E2 = E2(1:5001);
%E3 = E3(1:5001);
%E4 = E4(1:5001);
h1 = semilogy(t(1:1:80), E1(1:1:80), 'color', 'b', 'Linewidth', 1.5 );
hold on;
h2 = semilogy(t(1:1:80), E2(1:1:80), 'color', 'r', 'Linewidth', 1.5 );
hold on;
semilogy(t(1:10:80), E1(1:10:80), '+', 'color', 'b', 'Linewidth', 1.5);
hold on;
semilogy(t(1:10:80), E2(1:10:80), 's', 'color', 'r', 'Linewidth', 1.5 );
hold on;

set(gca, 'fontsize', 15)
xlabel('time','fontsize', 20, 'interp', 'latex');
ylabel('$L^2$ error','fontsize', 20, 'interp', 'latex')
[H, h] = legend([h1, h2], '~no scaling', '~scaling', 'interp', 'latex');
H.Box = 'off';
%set(h(6), 'marker', '*')
set(h(4), 'marker', '+')
set(h(6), 'marker', 's')
set(h(1:2), 'fontsize', 20, 'interp', 'latex')
%set(H, 'Position', [.08, .64, .1, .2])
axis([0 4 1e-9 1])
set(gca, 'xtick', [ 0 ,1,2,3, 4])
set(gca, 'xticklabel', {0 ,1,2,3, 4})
set(gca, 'ytick', [1e-9, 1e-8, 1e-7, 1e-6, 1e-5, 1e-4, 1e-3, 1e-2, 1e-1, 1])
set(gca, 'yticklabel', {'', '10^{-8}',  '','10^{-6}', '', '10^{-4}', '', '10^{-2}', '', '1'})
set(gca,'yminortick','off') 
title('(a) error ', 'fontsize', 20, 'interp', 'latex');
set(H, 'Position', [.1, .71, .1, .2])
%disp(E1)

subplot('Position', [0.4 0.14 0.25 0.76])
t = 0.05:0.05:4;
E1 = csvread('freq_spectral_bounded_nonadaptive1.csv');
E2 = csvread('freq_spectral_bounded_adaptive1.csv');
h1 = semilogy(t(1:1:80), E1(1:1:80), 'color', 'b', 'Linewidth', 1.5 );
hold on;
h2 = semilogy(t(1:1:80), E2(1:1:80), 'color', 'r', 'Linewidth', 1.5 );
hold on;
semilogy(t(1:10:80), E1(1:10:80), '+', 'color', 'b', 'Linewidth', 1.5);
hold on;
semilogy(t(1:10:80), E2(1:10:80), 's', 'color', 'r', 'Linewidth', 1.5);
hold on;
set(gca, 'fontsize', 15)
xlabel('time','fontsize', 20, 'interp', 'latex');
ylabel('$\mathcal{F}(U_N)$','fontsize', 20, 'interp', 'latex')
[H, h] = legend([h1, h2], '~no scaling', '~scaling', 'interp', 'latex');
H.Box = 'off';
set(h(4), 'marker', '+')
set(h(6), 'marker', 's')
set(h(1:2), 'fontsize', 20, 'interp', 'latex')
set(gca, 'xtick', [ 0 ,1,2,3, 4])
set(gca, 'xticklabel', {0 ,1,2,3, 4})
set(gca, 'ytick', [1e-16,1e-14,  1e-12,  1e-10, 1e-8,  1e-6,  1e-4,  1e-2,  1])
set(gca, 'yticklabel', { '10^{-16}','','10^{-12}', '', '10^{-8}',  '', '10^{-4}', '', '1'})
set(gca,'yminortick','off') 
axis([0 4 1e-16 1])
set(H, 'Position', [.52, .23, .1, .2])
title('(b) frequency indicator', 'fontsize', 20, 'interp', 'latex');


subplot('Position', [0.73 0.14 0.25 0.76])
t = 0.05:0.05:4;
E1 = csvread('beta_spectral_bounded_adaptive1.csv');
h1 = plot(t(1:1:80), E1(1:1:80), 'color', 'r', 'Linewidth', 1.5 );
hold on;
%plot(t(1:30:300), E1(1:30:300), 's', 'color', 'r', 'Linewidth', 1.5);
%hold on;
set(gca, 'fontsize', 15)
xlabel('time','fontsize', 20, 'interp', 'latex');
ylabel('$\beta$','fontsize', 20, 'interp', 'latex')
%[H, h] = legend([h1], '~$x_L$', 'interp', 'latex');
%H.Box = 'off';
%set(h(3), 'marker', '+')
%set(h(1), 'fontsize', 20, 'interp', 'latex')
%set(gca, 'xtick', [ 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0])
%set(gca, 'xticklabel', {'', '',  '','', '0.5', '', '',  '','', '1.0'})
set(gca, 'xtick', [ 0 ,1,2,3, 4])
set(gca, 'xticklabel', {0 ,1,2,3, 4})
set(gca, 'ytick', [0, 0.2,  0.4,  0.6,  0.8,  1,  1.2, 1.4, 1.6, 1.8, 2])
set(gca, 'yticklabel', {0,  '',  0.4,  '',  0.8,  '', 1.2, '', 1.6, '', 2})
set(gca,'yminortick','off')
axis([0 4 0 2])
%set(H, 'Position', [.25, .31, .1, .12])
title('(c) scaling factor', 'fontsize', 20, 'interp', 'latex');

end